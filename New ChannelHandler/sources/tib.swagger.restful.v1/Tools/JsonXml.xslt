<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:pfx6 = "http://psg.tibco.com/specification/configuration/v2"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="pfx6:ServiceProperties">
            <xsl:element name="{concat(@ComponentName,@Operation)}">
                <xsl:for-each select="pfx6:Item">
            <xsl:element name="{@Key}">
                <xsl:for-each select="pfx6:Property">
                    <xsl:element name="{@Name}">
                        <xsl:value-of select="@Value"/>
                    </xsl:element>
                </xsl:for-each>
                <xsl:for-each select="pfx6:Item">
                    <xsl:element name="{@Key}">
                        <xsl:for-each select="pfx6:Property">
                            <xsl:element name="{@Name}">
                                <xsl:value-of select="@Value"/>
                            </xsl:element>
                        </xsl:for-each>
                        <xsl:for-each select="pfx6:Item">
                            <xsl:element name="{@Key}">
                                <xsl:for-each select="pfx6:Property">
                                    <xsl:element name="{@Name}">
                                        <xsl:value-of select="@Value"/>
                                    </xsl:element>
                                </xsl:for-each>
                                <xsl:for-each select="pfx6:Item">
                                    <xsl:element name="{@Key}">
                                        <xsl:for-each select="pfx6:Property">
                                            <xsl:element name="{@Name}">
                                                <xsl:value-of select="@Value"/>
                                            </xsl:element>
                                        </xsl:for-each>
                                        <xsl:for-each select="pfx6:Item">
                                            <xsl:element name="{@Key}">
                                                <xsl:for-each select="pfx6:Property">
                                                    <xsl:element name="{@Name}">
                                                        <xsl:value-of select="@Value"/>
                                                    </xsl:element>
                                                </xsl:for-each>
                                                <xsl:for-each select="pfx6:Item">
                                                    <xsl:element name="{@Key}">
                                                        <xsl:for-each select="pfx6:Property">
                                                            <xsl:element name="{@Name}">
                                                                <xsl:value-of select="@Value"/>
                                                            </xsl:element>
                                                        </xsl:for-each>
                                                        <xsl:for-each select="pfx6:Item">
                                                            <xsl:element name="{@Key}">
                                                                <xsl:for-each select="pfx6:Property">
                                                                    <xsl:element name="{@Name}">
                                                                        <xsl:value-of select="@Value"/>
                                                                    </xsl:element>
                                                                </xsl:for-each>
                                                                <xsl:for-each select="pfx6:Item">
                                                                    <xsl:element name="{@Key}">
                                                                        <xsl:for-each select="pfx6:Property">
                                                                            <xsl:element name="{@Name}">
                                                                                <xsl:value-of select="@Value"/>
                                                                            </xsl:element>
                                                                        </xsl:for-each>
                                                                        
                                                                    </xsl:element>
                                                                </xsl:for-each>
                                                            </xsl:element>
                                                        </xsl:for-each>        
                                                    </xsl:element>
                                                </xsl:for-each>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:for-each>
                            </xsl:element>
                        </xsl:for-each>        
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
        </xsl:for-each>
                <xsl:for-each select="pfx6:Property">
                    <xsl:element name="{@Name}">
                        <xsl:value-of select="@Value"/>
                    </xsl:element>
                </xsl:for-each>
                
            </xsl:element>
    </xsl:template>
</xsl:stylesheet>