SET SERVEROUTPUT ON SIZE 400000;
Accept pid Prompt 'Enter ID : '
DECLARE
   CURSOR SERVICES_CAMPAIGN_CURSOR IS
    select *
    from SERVICES_DATA
    where ID in (SELECT regexp_substr(&pid,'[^,]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(&pid,'[^,]+', 1, level) is not null);
    V_SERVICES_CAMPAIGN SERVICES_CAMPAIGN_CURSOR%ROWTYPE;
    IS_EXIST INTEGER := 0;
    OUTPUT_SUCCESS VARCHAR2(1000);
    OUTPUT_FAIL VARCHAR2(1000);
    VChargeAmount NUMBER;
BEGIN
    OPEN SERVICES_CAMPAIGN_CURSOR;
    FETCH SERVICES_CAMPAIGN_CURSOR INTO V_SERVICES_CAMPAIGN;
    WHILE SERVICES_CAMPAIGN_CURSOR%FOUND LOOP
        DBMS_OUTPUT.PUT_LINE ('---------------------');
        DBMS_OUTPUT.PUT_LINE ('Checking validation @ID:' || V_SERVICES_CAMPAIGN.ID);
        IF (V_SERVICES_CAMPAIGN. "StartDate" < V_SERVICES_CAMPAIGN."EndDate") THEN
            DBMS_OUTPUT.PUT_LINE ('startdate < enddate : OK');
        ELSE
            DBMS_OUTPUT.PUT_LINE ('startdate < enddate : NOT VALID');
        END IF;
        
        /*
        IF (V_SERVICES_CAMPAIGN."EndDate" > V_SERVICES_CAMPAIGN. "StartDate") THEN
            DBMS_OUTPUT.PUT_LINE ('enddate > startdate : OK');
        ELSE
            DBMS_OUTPUT.PUT_LINE ('enddate > startdate : NOT VALID');
        END IF;
        */
        
        --ML4
        FOR C IN (SELECT regexp_substr(V_SERVICES_CAMPAIGN."ch_business_product",'[^|]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(V_SERVICES_CAMPAIGN."ch_business_product", '[^|]+', 1, level) is not null)
        LOOP
            IS_EXIST := 0;        
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM CONTEXT
                WHERE ID = C.LOOP_VALUE
                AND CONTEXT."StartDate" < CONTEXT."EndDate";
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE (C.LOOP_VALUE || ' not exist or date not valid');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE (C.LOOP_VALUE || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE (C.LOOP_VALUE ||' > 1, actual : ' || IS_EXIST);
            END IF;
        
        --ML3        
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM CONTEXT
                WHERE ID LIKE 'ML3_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,3) || '%'
                AND PCO LIKE '%' || C.LOOP_VALUE || '%'
                AND CONTEXT."StartDate" < CONTEXT."EndDate";
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('ML3_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,3) || ' not exist or date not valid');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('ML3_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,3) || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('ML3_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,3) ||' > 1, actual : ' || IS_EXIST);
            END IF;
        
            --ML2 
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM CONTEXT
                WHERE ID LIKE 'ML2_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,2) || '%'
                AND PCO LIKE '%' || 'ML3_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,3) || '%'
                AND CONTEXT."StartDate" < CONTEXT."EndDate";
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('ML2_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,2) || ' not exist or date not valid');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('ML2_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,2) || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('ML2_BP_' || SUBSTR(SUBSTR(C.LOOP_VALUE || '000',1,12),-5,2) ||' > 1, actual : ' || IS_EXIST);
            END IF;
    END LOOP;
        
        --SUB_Brand 
        IF LENGTH(V_SERVICES_CAMPAIGN."S_SUB_Brand") > 0 THEN
            FOR C IN (SELECT regexp_substr(V_SERVICES_CAMPAIGN."S_SUB_Brand",'[^|]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(V_SERVICES_CAMPAIGN."S_SUB_Brand", '[^|]+', 1, level) is not null)
            LOOP
                IS_EXIST := 0;
                BEGIN
                    SELECT COUNT(1)
                    INTO IS_EXIST
                    FROM SEGMENTS
                    WHERE "Name" = 'SUB_Brand'
                    AND TO_CHAR("Values") LIKE '%'|| C.LOOP_VALUE ||'%';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    IS_EXIST := 0;
                END;
                
                IF IS_EXIST = 0 THEN
                    DBMS_OUTPUT.PUT_LINE ('SUB_Brand ' || C.LOOP_VALUE || ' not exist');
                ELSIF IS_EXIST = 1 THEN
                    DBMS_OUTPUT.PUT_LINE ('SUB_Brand ' || C.LOOP_VALUE || ' exist/OK');
                ELSE
                    DBMS_OUTPUT.PUT_LINE ('SUB_Brand ' || C.LOOP_VALUE ||' > 1, actual : ' || IS_EXIST);
                END IF;
            END LOOP;
        END IF;
        
        --SYS_Channel
        IF LENGTH(V_SERVICES_CAMPAIGN."S_SYS_Channel") > 0 THEN
            FOR C IN (SELECT regexp_substr(V_SERVICES_CAMPAIGN."S_SYS_Channel",'[^|]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(V_SERVICES_CAMPAIGN."S_SYS_Channel", '[^|]+', 1, level) is not null)
            LOOP
                IS_EXIST := 0;
                BEGIN
                    SELECT COUNT(1)
                    INTO IS_EXIST
                    FROM SEGMENTS
                    WHERE "Name" = 'SYS_Channel'
                    AND TO_CHAR("Values") LIKE '%'|| C.LOOP_VALUE ||'%';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    IS_EXIST := 0;
                END;
                
                IF IS_EXIST = 0 THEN
                    DBMS_OUTPUT.PUT_LINE ('SYS_Channel ' || C.LOOP_VALUE || ' not exist');
                ELSIF IS_EXIST = 1 THEN
                    DBMS_OUTPUT.PUT_LINE ('SYS_Channel ' || C.LOOP_VALUE || ' exist/OK');
                ELSE
                    DBMS_OUTPUT.PUT_LINE ('SYS_Channel ' || C.LOOP_VALUE ||' > 1, actual : ' || IS_EXIST);
                END IF;
            END LOOP;
        END IF;
        
        --SUB_Location_G 
        IF LENGTH(TO_CHAR(V_SERVICES_CAMPAIGN."S_SUB_Location_G")) > 0 THEN            
            OUTPUT_SUCCESS := 'S_SUB_Location_G ';
            OUTPUT_FAIL := 'S_SUB_Location_G ';
            FOR F IN (SELECT regexp_substr(TO_CHAR(V_SERVICES_CAMPAIGN."S_SUB_Location_G"),'[^|]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(TO_CHAR(V_SERVICES_CAMPAIGN."S_SUB_Location_G"), '[^|]+', 1, level) is not null)
            LOOP
                IS_EXIST := 0;
                BEGIN
                    SELECT COUNT(1)
                    INTO IS_EXIST
                    FROM SEGMENTS
                    WHERE "Name" = 'SUB_Location_G'
                    AND INSTR("Values",F.LOOP_VALUE) > 0;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    IS_EXIST := 0;
                END;
                
                --DBMS_OUTPUT.PUT_LINE('PCO :' || IS_EXIST);
                                
                IF IS_EXIST = 0 THEN
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE || ' not exist');
                    OUTPUT_FAIL := OUTPUT_FAIL || F.LOOP_VALUE || ' Not exist | ';
                ELSIF IS_EXIST = 1 THEN
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE || ' exist/OK');
                    OUTPUT_SUCCESS := OUTPUT_SUCCESS || F.LOOP_VALUE || ' | ';
                ELSE
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE ||' > 1, actual : ' || IS_EXIST);
                    OUTPUT_FAIL := OUTPUT_FAIL || F.LOOP_VALUE || ' > 1, actual : ' || IS_EXIST || ' | ';                    
                END IF;
            END LOOP;
            
            IF LENGTH(nvl(OUTPUT_SUCCESS,' ')) > 12 THEN
                DBMS_OUTPUT.PUT_LINE(OUTPUT_SUCCESS || 'exist/OK');
            END IF;
            IF LENGTH(nvl(OUTPUT_FAIL,' ')) > 12 THEN
                DBMS_OUTPUT.PUT_LINE('#' || OUTPUT_FAIL);
            END IF;
            
        END IF;   
        /*
        IF LENGTH(V_SERVICES_CAMPAIGN."S_SUB_Location_G") > 0 THEN
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM SEGMENTS
                WHERE "Name" = 'SUB_Location_G'
                AND INSTR("Values",V_SERVICES_CAMPAIGN."S_SUB_Location_G") > 0;
                --AND TO_CHAR(substr("Values",1,4000)) LIKE '%'|| V_SERVICES_CAMPAIGN."S_SUB_Location_G" ||'%';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_Location_G ' || V_SERVICES_CAMPAIGN."S_SUB_Location_G" || ' not exist');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_Location_G ' || V_SERVICES_CAMPAIGN."S_SUB_Location_G" || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('SUB_Location_G ' || V_SERVICES_CAMPAIGN."S_SUB_Location_G" ||' > 1, actual : ' || IS_EXIST);
            END IF;
        END IF;
        */
        --SUB_IMEI_G          
        IF LENGTH(V_SERVICES_CAMPAIGN.S_SUB_IMEI_G) > 0 THEN
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM SEGMENTS
                WHERE "Name" = 'SUB_IMEI_G'
                AND TO_CHAR("Values") LIKE '%'|| V_SERVICES_CAMPAIGN.S_SUB_IMEI_G ||'%';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_IMEI_G ' || V_SERVICES_CAMPAIGN.S_SUB_IMEI_G || ' not exist');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_IMEI_G ' || V_SERVICES_CAMPAIGN.S_SUB_IMEI_G || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('SUB_IMEI_G ' || V_SERVICES_CAMPAIGN.S_SUB_IMEI_G ||' > 1, actual : ' || IS_EXIST);
            END IF;
        END IF;
        
        --SUB_MSISDN_G 
        IF LENGTH(TO_CHAR(V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G)) > 0 THEN            
            OUTPUT_SUCCESS := 'SUB_MSISDN_G ';
            OUTPUT_FAIL := 'SUB_MSISDN_G ';
            FOR F IN (SELECT regexp_substr(TO_CHAR(V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G),'[^|]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(TO_CHAR(V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G), '[^|]+', 1, level) is not null)
            LOOP
                IS_EXIST := 0;
                BEGIN
                    SELECT COUNT(1)
                    INTO IS_EXIST
                    FROM SEGMENTS
                    WHERE "Name" = 'SUB_MSISDN_G'
                    AND TO_CHAR("Values") LIKE '%'|| F.LOOP_VALUE ||'%';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    IS_EXIST := 0;
                END;
                
                --DBMS_OUTPUT.PUT_LINE('PCO :' || IS_EXIST);
                                
                IF IS_EXIST = 0 THEN
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE || ' not exist');
                    OUTPUT_FAIL := OUTPUT_FAIL || F.LOOP_VALUE || ' Not exist | ';
                ELSIF IS_EXIST = 1 THEN
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE || ' exist/OK');
                    OUTPUT_SUCCESS := OUTPUT_SUCCESS || F.LOOP_VALUE || ' | ';
                ELSE
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE ||' > 1, actual : ' || IS_EXIST);
                    OUTPUT_FAIL := OUTPUT_FAIL || F.LOOP_VALUE || ' > 1, actual : ' || IS_EXIST || ' | ';                    
                END IF;
            END LOOP;
            
            IF LENGTH(nvl(OUTPUT_SUCCESS,' ')) > 12 THEN
                DBMS_OUTPUT.PUT_LINE(OUTPUT_SUCCESS || 'exist/OK');
            END IF;
            IF LENGTH(nvl(OUTPUT_FAIL,' ')) > 12 THEN
                DBMS_OUTPUT.PUT_LINE('#' || OUTPUT_FAIL);
            END IF;
            
        END IF;   
        /*        
        IF LENGTH(V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G) > 0 THEN
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM SEGMENTS
                WHERE "Name" = 'SUB_MSISDN_G'
                AND TO_CHAR("Values") LIKE '%'|| V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G ||'%';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_MSISDN_G ' || V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G || ' not exist');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_MSISDN_G ' || V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('SUB_MSISDN_G ' || V_SERVICES_CAMPAIGN.S_SUB_MSISDN_G ||' > 1, actual : ' || IS_EXIST);
            END IF;
        END IF;
        */
        
        --SUB_Priceplan 
        IF LENGTH(V_SERVICES_CAMPAIGN."S_SUB_Priceplan") > 0 THEN
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM SEGMENTS
                WHERE "Name" = 'SUB_Priceplan'
                AND TO_CHAR("Values") LIKE '%'|| V_SERVICES_CAMPAIGN."S_SUB_Priceplan" ||'%';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_Priceplan ' || V_SERVICES_CAMPAIGN."S_SUB_Priceplan" || ' not exist');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('SUB_Priceplan ' || V_SERVICES_CAMPAIGN."S_SUB_Priceplan" || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('SUB_Priceplan ' || V_SERVICES_CAMPAIGN."S_SUB_Priceplan" ||' > 1, actual : ' || IS_EXIST);
            END IF;
        END IF;
        
        --SYS Day 
        IF LENGTH(V_SERVICES_CAMPAIGN."S_SYS_Day") > 0 THEN
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM SEGMENTS
                WHERE "Name" = 'SYS_Day'
                AND TO_CHAR("Values") LIKE '%'|| V_SERVICES_CAMPAIGN."S_SYS_Day" ||'%';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('SYS_Day ' || V_SERVICES_CAMPAIGN."S_SYS_Day" || ' not exist');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('SYS_Day ' || V_SERVICES_CAMPAIGN."S_SYS_Day" || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('SYS_Day ' || V_SERVICES_CAMPAIGN."S_SYS_Day" ||' > 1, actual : ' || IS_EXIST);
            END IF;
        END IF;
        
        -- Price  
        IF LENGTH(V_SERVICES_CAMPAIGN."PriceID") > 0 THEN
            IS_EXIST := 0;
            BEGIN
                SELECT COUNT(1)
                INTO IS_EXIST
                FROM PRICE
                WHERE ID = V_SERVICES_CAMPAIGN."PriceID";
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IS_EXIST := 0;
            END;
            
            IF IS_EXIST = 0 THEN
                DBMS_OUTPUT.PUT_LINE ('Price ID ' || V_SERVICES_CAMPAIGN."PriceID" || ' not exist');
            ELSIF IS_EXIST = 1 THEN
                DBMS_OUTPUT.PUT_LINE ('Price ID ' || V_SERVICES_CAMPAIGN."PriceID" || ' exist/OK');
            ELSE
                DBMS_OUTPUT.PUT_LINE ('Price ID ' || V_SERVICES_CAMPAIGN."PriceID" ||' > 1, actual : ' || IS_EXIST);
            END IF;
        END IF;        
        
        --PCO 
        IF LENGTH(TO_CHAR(V_SERVICES_CAMPAIGN.PCO)) > 0 THEN            
            OUTPUT_SUCCESS := 'PCO ';
            OUTPUT_FAIL := 'PCO ';
            FOR F IN (SELECT regexp_substr(TO_CHAR(V_SERVICES_CAMPAIGN.PCO),'[^|]+', 1, level) AS LOOP_VALUE FROM DUAL
                            connect by regexp_substr(TO_CHAR(V_SERVICES_CAMPAIGN.PCO), '[^|]+', 1, level) is not null)
            LOOP
                IS_EXIST := 0;
                BEGIN
                    SELECT COUNT(1)
                    INTO IS_EXIST
                    FROM BONUSES
                    WHERE ID = TO_CHAR(F.LOOP_VALUE);
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    IS_EXIST := 0;
                END;
                
                --DBMS_OUTPUT.PUT_LINE('PCO :' || IS_EXIST);
                                
                IF IS_EXIST = 0 THEN
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE || ' not exist');
                    OUTPUT_FAIL := OUTPUT_FAIL || F.LOOP_VALUE || ' Not exist | ';
                ELSIF IS_EXIST = 1 THEN
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE || ' exist/OK');
                    OUTPUT_SUCCESS := OUTPUT_SUCCESS || F.LOOP_VALUE || ' | ';
                ELSE
                    --DBMS_OUTPUT.PUT_LINE ('PCO ' || C.LOOP_VALUE ||' > 1, actual : ' || IS_EXIST);
                    OUTPUT_FAIL := OUTPUT_FAIL || F.LOOP_VALUE || ' > 1, actual : ' || IS_EXIST || ' | ';                    
                END IF;
            END LOOP;
            
            IF LENGTH(nvl(OUTPUT_SUCCESS,' ')) > 5 THEN
                DBMS_OUTPUT.PUT_LINE(OUTPUT_SUCCESS || 'exist/OK');
            END IF;
            IF LENGTH(nvl(OUTPUT_FAIL,' ')) > 5 THEN
                DBMS_OUTPUT.PUT_LINE(OUTPUT_FAIL);
            END IF;
            
        END IF;        
        
        FETCH SERVICES_CAMPAIGN_CURSOR INTO V_SERVICES_CAMPAIGN;
    END LOOP;
    CLOSE SERVICES_CAMPAIGN_CURSOR;
END;