
			export JRE_HOME=`sed '/^JRE_HOME=/!d;s/.*=//' winstone.properties`
			export prefix=`sed '/^prefix=/!d;s/.*=//' winstone.properties`
			export controlPort=`sed '/^controlPort=/!d;s/.*=//' winstone.properties`
			export path=$JRE_HOME/bin:path
			java -cp .:lib/winstone-0.9.10.jar winstone.tools.WinstoneControl --operation=reload:$prefix --controlPort=$controlPort
		