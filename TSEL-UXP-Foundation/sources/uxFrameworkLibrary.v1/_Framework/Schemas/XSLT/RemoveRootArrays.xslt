<?xml version="1.0"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xi="http://www.w3.org/2001/XInclude"
  version="1.0">

  <!-- TEMPLATE #1 -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- TEMPLATE #2 -->
 <xsl:template match="*[@xsi:type = 'soapenc:Array']">
      <xsl:apply-templates/>
  </xsl:template>

<!--<xsl:template match="@xsi:type[not(starts-with(.,'xsd'))]"/>-->


</xsl:stylesheet>