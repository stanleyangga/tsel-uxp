<?xml-stylesheet type="text/xsl"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:s="http://www.w3.org/2001/XMLSchema" >


<xsl:template match='text()' />

<xsl:template match="//s:schema">
    <xsl:copy-of select='.'/>
</xsl:template>    


</xsl:stylesheet>