package tibco.util.xmldiff;

import java.io.Serializable;



public class MyXMLDiffOptions implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean ignoreDiffBetweenTextAndCDATA;
    private boolean ignoreComments;
    private boolean ignoreAttributeOrder;
    private boolean ignoreWhitespace;
    private boolean normalizeWhitespace;
    
	public MyXMLDiffOptions() {
		super();
	}
	public MyXMLDiffOptions(boolean ignoreDiffBetweenTextAndCDATA,
			boolean ignoreComments, boolean ignoreAttributeOrder,
			boolean ignoreWhitespace, boolean normalizeWhitespace) {
		super();
		this.ignoreDiffBetweenTextAndCDATA = ignoreDiffBetweenTextAndCDATA;
		this.ignoreComments = ignoreComments;
		this.ignoreAttributeOrder = ignoreAttributeOrder;
		this.ignoreWhitespace = ignoreWhitespace;
		this.normalizeWhitespace = normalizeWhitespace;
	}
	public boolean isIgnoreDiffBetweenTextAndCDATA() {
		return ignoreDiffBetweenTextAndCDATA;
	}
	public void setIgnoreDiffBetweenTextAndCDATA(
			boolean ignoreDiffBetweenTextAndCDATA) {
		this.ignoreDiffBetweenTextAndCDATA = ignoreDiffBetweenTextAndCDATA;
	}
	public boolean isIgnoreComments() {
		return ignoreComments;
	}
	public void setIgnoreComments(boolean ignoreComments) {
		this.ignoreComments = ignoreComments;
	}
	public boolean isIgnoreAttributeOrder() {
		return ignoreAttributeOrder;
	}
	public void setIgnoreAttributeOrder(boolean ignoreAttributeOrder) {
		this.ignoreAttributeOrder = ignoreAttributeOrder;
	}
	public boolean isIgnoreWhitespace() {
		return ignoreWhitespace;
	}
	public void setIgnoreWhitespace(boolean ignoreWhitespace) {
		this.ignoreWhitespace = ignoreWhitespace;
	}
	public boolean isNormalizeWhitespace() {
		return normalizeWhitespace;
	}
	public void setNormalizeWhitespace(boolean normalizeWhitespace) {
		this.normalizeWhitespace = normalizeWhitespace;
	}
}
