package tibco.util.xmldiff;

import java.io.Serializable;


public class MyXMLDiffResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String testDoc;
	private String controlDoc;
	



	private MyXMLDiffMessage[] messages;

	
	public MyXMLDiffResult() {		
		super();		
	}

	public String getTestDoc() {
		return testDoc;
	}


	public void setTestDoc(String testDoc) {
		this.testDoc = testDoc;
	}


	public String getControlDoc() {
		return controlDoc;
	}


	public void setControlDoc(String controlDoc) {
		this.controlDoc = controlDoc;
	}


	public MyXMLDiffMessage[] getMessages() {
		return messages;
	}


	public void setMessages(MyXMLDiffMessage[] messages) {
		this.messages = messages;
	}
	
}

