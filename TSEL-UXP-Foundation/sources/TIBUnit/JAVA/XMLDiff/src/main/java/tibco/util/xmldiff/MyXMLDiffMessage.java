/**
 * 
 */
package tibco.util.xmldiff;

import java.io.Serializable;

public class MyXMLDiffMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private String id;
	public String getMessage() {
		return message;
	}
	
	public MyXMLDiffMessage() {
		super();
	}

	public MyXMLDiffMessage(String message, String id) {
		super();
		this.message = message;
		this.id = id;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
}