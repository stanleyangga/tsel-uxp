package tibco.util.xmldiff;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceConstants;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.NodeDetail;
import org.custommonkey.xmlunit.XMLUnit;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class MyXMLDiff implements Serializable {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	public MyXMLDiff() {
		super();
		System.setErr(new PrintStream(new LoggingOutputStream(
		        Logger.getLogger("outLog"), Level.ERROR)));	
		System.setOut(new PrintStream(new LoggingOutputStream(
		        Logger.getLogger("outLog"), Level.INFO)));
	}	
	

	public boolean isEqual(String controlXML, String testXML)
	throws SAXException, IOException {
		return this.isEqual(controlXML, testXML, getDefaultOptions());
	}
	
	public boolean isEqual(String controlXML, String testXML,
			MyXMLDiffOptions opt)
			throws SAXException, IOException {

		setOptions(opt);
		System.out.println("Check point before DIFF");
		Diff d = new Diff(
				controlXML.length() > 0 ? controlXML : "<empty/>",
				testXML.length()    > 0 ? testXML    : "<empty/>");
		System.out.println("Check point after DIFF");
		d.overrideDifferenceListener(
			new DifferenceListener() {
				
				 public int differenceFound(Difference difference){ 
				         if (difference.getId() 
				             == DifferenceConstants.NAMESPACE_PREFIX_ID) 
				     {
				         return RETURN_IGNORE_DIFFERENCE_NODES_IDENTICAL;  
				     } 
				     return RETURN_ACCEPT_DIFFERENCE;
				 }				 
				 public void skippedComparison(Node control, Node test){						 
				 }
				 
			}
		); 
		
		return d.identical();

	}

	public MyXMLDiffResult getDiff(String controlXML, String testXML)
			throws SAXException, IOException {
		return this.getDiff(controlXML, testXML, getDefaultOptions());
	}

	public MyXMLDiffResult getDiff(String controlXML, String testXML,
			MyXMLDiffOptions opt) throws SAXException, IOException {

		MyXMLDiffResult res = new MyXMLDiffResult();

		res.setTestDoc(testXML);
		res.setControlDoc(controlXML);

		setOptions(opt);

		List<Difference> l = getDiffList(controlXML, testXML);

		if (l.size() > 0) {

			List<MyXMLDiffMessage> msgs = new ArrayList<MyXMLDiffMessage>();

			// found some difference
			// get the documents
			Document testDoc;
			Document controlDoc;

			// try to get the parsed documents
			try {
				testDoc = getDocumentFromList(l, Difference.class
						.getMethod("getTestNodeDetail"));
				controlDoc = getDocumentFromList(l, Difference.class
						.getMethod("getControlNodeDetail"));
			} catch (Exception e) {
				return null;
			}

			// if there is some modification in the document
			// add the diff namespace
			if (testDoc != null)
				addDiffNamespace(testDoc);
			if (controlDoc != null)
				addDiffNamespace(controlDoc);

			// add the difference found in the documents
			processDiffList(l, msgs);

			// output the document in an xml form
			// if the document has difference
			if (testDoc != null)
				res.setTestDoc(outputXML(testDoc));
			if (controlDoc != null)
				res.setControlDoc(outputXML(controlDoc));
			res.setMessages(msgs.toArray(new MyXMLDiffMessage[msgs.size()]));
		}

		/* workaround bug tibco */
		res.setControlDoc("<workaround_dont_validate>" + res.getControlDoc() + "</workaround_dont_validate>");
		res.setTestDoc("<workaround_dont_validate>" + res.getTestDoc() + "</workaround_dont_validate>");
		return res;
	}
	
	private void setOptions(MyXMLDiffOptions opt) {		
		if (opt != null) {
			XMLUnit.setIgnoreComments(opt.isIgnoreComments());
			XMLUnit.setIgnoreAttributeOrder(opt.isIgnoreAttributeOrder());
			XMLUnit.setIgnoreDiffBetweenTextAndCDATA(opt
					.isIgnoreDiffBetweenTextAndCDATA());
			XMLUnit.setIgnoreWhitespace(opt.isIgnoreWhitespace());
			XMLUnit.setNormalizeWhitespace(opt.isNormalizeWhitespace());
		}
	}
	
	private MyXMLDiffOptions getDefaultOptions() {		
		return new MyXMLDiffOptions(
				false, true, true, false, true);
	}
	

	private Document getDocumentFromList(List<Difference> l, Method fct) {

		if (l.size() == 0)
			return null;

		Iterator<Difference> ite = l.iterator();
		Document doc = null;

		while (ite.hasNext() && (doc == null)) {
			Difference diff = ite.next();
			try {
				Node node = null;
				if ((node = ((NodeDetail) fct.invoke(diff)).getNode()) != null)
					doc = node.getOwnerDocument();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return doc;
	}

	private void addDiffNamespace(Document doc) {
		doc.getDocumentElement().setAttribute("xmlns:diff", "http://www.tibco.com/BWUnit/Schemas/Diff/MyXMLDiffResult/1");
	}

	private void processDiffList(List<Difference> l, List<MyXMLDiffMessage> res)
			throws DOMException {

		long count = 0;

		Iterator<Difference> ite = l.iterator();
		while (ite.hasNext()) {
			Difference diff = ite.next();

			// System.out.println(diff.toString());
			// System.out.println("Diff Id: " + diff.getId());

			Node nodeTestDoc = diff.getTestNodeDetail().getNode();
			Node nodeControlDoc = diff.getControlNodeDetail().getNode();

			switch (diff.getId()) {
			case DifferenceConstants.CHILD_NODELIST_LENGTH_ID:
			case DifferenceConstants.CHILD_NODELIST_SEQUENCE_ID:
			case DifferenceConstants.ELEMENT_NUM_ATTRIBUTES_ID:
			case DifferenceConstants.ATTR_SEQUENCE_ID:
				break;

			case DifferenceConstants.ELEMENT_TAG_NAME_ID:
			case DifferenceConstants.CHILD_NODE_NOT_FOUND_ID:
				// modifyDocument(nodeTestDoc, "deleted");
				// modifyDocument(nodeControlDoc, "inserted");
				// break;
			case DifferenceConstants.HAS_CHILD_NODES_ID:
			default:
				String diffId = Long.toString(count++);
				modifyDocument(nodeTestDoc, "changed", diffId);
				modifyDocument(nodeControlDoc, "changed", diffId);
				res.add(new MyXMLDiffMessage(diff.toString(), diffId));
				break;
			}
		}
	}

	private String outputXML(Document testDoc)
			throws TransformerFactoryConfigurationError,
			IllegalArgumentException {

		// ///////////////
		// Output the XML

		// set up a transformer
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans;
		try {
			trans = transfac.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return "";
		}
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "no");

		// create string from xml tree
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(testDoc);
		try {
			trans.transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		return sw.toString();

	}

	@SuppressWarnings("unchecked")
	private List<Difference> getDiffList(String myControlXML, String myTestXML)
			throws SAXException, IOException {
		Diff d;		 
			
		d = new Diff(
					myControlXML.length() > 0 ? myControlXML : "<empty/>",
					myTestXML.length()    > 0 ? myTestXML    : "<empty/>");
		DetailedDiff dd = new DetailedDiff(d);
		dd.overrideElementQualifier(null);

		List<Difference> l = dd.getAllDifferences();

		return l;
	}

	private void modifyDocument(Node node, String operation, String diffId) {

		if (node == null)
			return;

		Document doc = node.getOwnerDocument();

		// System.out.println("NodeType: " + node.getNodeType());
		// System.out.println("Node: " + node.toString());

		switch (node.getNodeType()) {
		case Node.CDATA_SECTION_NODE:
		case Node.TEXT_NODE: {
			// surround the text with an element tag
			Node parentNode;
			Element e;

			e = doc.createElement("diff:" + operation);
			e.setAttribute("diff_id", diffId);
			parentNode = node.getParentNode();
			parentNode.replaceChild(e, node);
			e.appendChild(node);
			break;
		}
		case Node.ATTRIBUTE_NODE:
			node = ((Attr) node).getOwnerElement();
			// apply the modification to the owner element itself
		case Node.ELEMENT_NODE: {
			Element e = (Element) node;
			e.setAttribute("diff:" + operation, diffId);
			break;
		}
		default:
			return;
		}
	}

}
