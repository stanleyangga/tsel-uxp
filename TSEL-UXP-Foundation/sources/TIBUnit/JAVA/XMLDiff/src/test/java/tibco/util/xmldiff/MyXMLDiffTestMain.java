package tibco.util.xmldiff;

import java.io.IOException;

import org.xml.sax.SAXException;

import tibco.util.xmldiff.MyXMLDiff;
import tibco.util.xmldiff.MyXMLDiffMessage;
import tibco.util.xmldiff.MyXMLDiffOptions;
import tibco.util.xmldiff.MyXMLDiffResult;

public class MyXMLDiffTestMain {

	/**
	 * @param args
	 * @throws IOException
	 * @throws SAXException
	 */

	private static void testDiff(String controlXML, String testXML) {

		MyXMLDiffResult res;

		MyXMLDiff d = new MyXMLDiff();
		try {
			res = d.getDiff(controlXML, testXML, new MyXMLDiffOptions(false,
					true, true, false, true));
			String stdout = 
					"Here's the control xml:\n" + res.getControlDoc() + "\n" +
					"Here's the test xml:\n" + res.getTestDoc() + "\n";

			MyXMLDiffMessage msgs[] = res.getMessages();
			if (msgs == null) {
				stdout += " o no difference found" + "\n";
			}
			else {
				for (int i = 0; i <  msgs.length; i++) {
					MyXMLDiffMessage dm = msgs[i];
					stdout += " o id: " + dm.getId() + " msg: " + dm.getMessage()  + "\n";
				}
			}
			stdout += "-----------------------------------\n\n";
			
			System.out.println(stdout);
			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void t(String txt) {
		
		System.out.println("\n**** " + txt + " ****\n");
	}

	public static void main(String[] args) {


		t("Attr value changed");
		testDiff(
				Messages.getString("MyXMLDiffTestMain.AttrValueChangedControl"), //$NON-NLS-1$
				Messages.getString("MyXMLDiffTestMain.AttrValueChangedTest")); //$NON-NLS-1$

		t("Element in a list deleted");
		testDiff(
				Messages.getString("MyXMLDiffTestMain.ElementListDeletedControl"), //$NON-NLS-1$
				Messages.getString("MyXMLDiffTestMain.ElementListDeletedTest")); //$NON-NLS-1$

		t("Text deleted");
		testDiff(
				Messages.getString("MyXMLDiffTestMain.TextDeletedControl"), //$NON-NLS-1$
				Messages.getString("MyXMLDiffTestMain.TextDeletedTest")); //$NON-NLS-1$ 
		
		t("Identical");
		testDiff(
				Messages.getString("MyXMLDiffTestMain.IdenticalControl"), //$NON-NLS-1$
				Messages.getString("MyXMLDiffTestMain.IdenticalTest")); //$NON-NLS-1$

		t("Element in a sequence deleted");
		testDiff(
				Messages.getString("MyXMLDiffTestMain.ElementSeqDeletedControl"), //$NON-NLS-1$
				Messages.getString("MyXMLDiffTestMain.ElementSeqDeletedTest")); //$NON-NLS-1$

		t("Element in a sequence deleted");
		testDiff(
				Messages.getString("MyXMLDiffTestMain.ElementSequenceDeletedControl"), //$NON-NLS-1$
				Messages.getString("MyXMLDiffTestMain.ElementSequenceDeletedTest")); //$NON-NLS-1$
		
		t("Big test");
		testDiff(
				 Messages.getString("MyXMLDiffTestMain.BigTestControl") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.BigTestTest")); //$NON-NLS-1$

		t("Element moved");
		testDiff(
				 Messages.getString("MyXMLDiffTestMain.ElementMovedControl") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.ElementMovedTest")); //$NON-NLS-1$
	
		t("Empty doc");
		testDiff(
				 "" //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.ElementMovedTest")); //$NON-NLS-1$

		t("Normalized text node");
		testDiff(
				 Messages.getString("MyXMLDiffTestMain.NormalizedControl") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.NormalizedTest")); //$NON-NLS-1$
	
		t("Normalized text node 2");
		testDiff(
				 Messages.getString("MyXMLDiffTestMain.Normalized2Control") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.Normalized2Test")); //$NON-NLS-1$

		
	}

}
