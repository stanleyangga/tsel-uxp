package tibco.util.xmldiff;

import java.io.IOException;

import org.xml.sax.SAXException;

import junit.framework.TestCase;

public class MyXMLDiffTest extends TestCase {

	public void testIsEqual() throws SAXException, IOException {
		//Normalized text node"
		MyXMLDiff d = new MyXMLDiff();		
		
		assertEquals(true,d.isEqual( 
				 Messages.getString("MyXMLDiffTestMain.NormalizedControl") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.NormalizedTest"))); //$NON-NLS-1$
		
		assertEquals(true,d.isEqual( 
				 Messages.getString("MyXMLDiffTestMain.Normalized2Control") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.Normalized2Test"))); //$NON-NLS-1$
	}

	private long getDiffCount(String controlXML, String testXML) throws SAXException, IOException {

		MyXMLDiffResult res;

		MyXMLDiff d = new MyXMLDiff();

		res = d.getDiff(controlXML, testXML);
//		String stdout = 
//				"Here's the control xml:\n" + res.getControlDoc() + "\n" +
//				"Here's the test xml:\n" + res.getTestDoc() + "\n";

		MyXMLDiffMessage msgs[] = res.getMessages();
//		if (msgs == null) {
//			stdout += " o no difference found" + "\n";
//		}
//		else {
//			for (int i = 0; i <  msgs.length; i++) {
//				MyXMLDiffMessage dm = msgs[i];
//				stdout += " o id: " + dm.getId() + " msg: " + dm.getMessage()  + "\n";
//			}
//		}
//		stdout += "-----------------------------------\n\n";
		
		return msgs == null ? 0 : msgs.length;
		
		//System.out.println(stdout);
			

	}
	public void testGetDiffStringString() throws SAXException, IOException {
		
		//Normalized text node"
		assertEquals(0, getDiffCount(
				 Messages.getString("MyXMLDiffTestMain.NormalizedControl") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.NormalizedTest"))); //$NON-NLS-1$
	
		//Normalized text node 2
		assertEquals(0, getDiffCount(
				 Messages.getString("MyXMLDiffTestMain.Normalized2Control") //$NON-NLS-1$
				 ,
				 Messages.getString("MyXMLDiffTestMain.Normalized2Test"))); //$NON-NLS-1$
	}

}
