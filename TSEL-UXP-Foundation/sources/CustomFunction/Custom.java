package com.tibco.psg;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import javax.script.*;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Custom
{
	
		protected int mInternalValue;
 
      /**
      * The following method will not be available because it is 
      * a constructor.
      */
    public Custom(int value)
      {
         mInternalValue = value;
      }
      
    public static final String[][] HELP_STRINGS = {
        {
        	"GetPartition", 
        	"Returns partitionid from id	 -> GetPartition(id, PartitionSize)", 
        	"GetPartition(62811880683, 5000000)",
        	"1880683"
        },
        {
        	"RegexMatch", 
        	"Returns true if str matches the regex matchpattern	 -> RegexMatch(str, matchpattern)", 
        	"RegexMatch('a=b','.*=.*')",
        	"TRUE"
        },
        {	
        	"RegexReplaceAll", 
        	"Returns replacement string of str with 	 -> RegexMatch(str, matchpattern)", 
        	"RegexMatch('a=b','(.*)=(.*)', '%1==%2')",
        	"'a==b'"
        },
        {
        	"GetFilterExpression", 
        	"Returns filter expression in Java-Script from the Business Expression GetFilterExpression(name=value,name=value)", 
        	"GetFilterExpression('POC=JKT')",
        	"POC==JKT",
        	"GetFilterExpression('POC=JKT;ONNET=1,0,2')",
        	"POC=='JKT' && ( ONNET=='1' || ONNET=='0' || ONNET=='2' )",
        	"GetFilterExpression('POC=JKT;ONNET!=1')",
        	"POC=='JKT' && ONNET!='1'"
        },
        {
        	"StringExpression", 
        	"Returns result of string concatenation from the java-script Expression", 
        	"StringExpression(Expression, InputList)",
        	"Expression is java-script String Expression, InputList is the field definition in form as Variable=Value[;Variable=Value[;...]]",
        	"StringExpression('A + ' ' + B', 'A=Hello;B=World')",
        	"Hello World"
        },
        {
        	"Boolean", 
        	"Returns boolean from the java-script Expression", 
        	"BooleanExpression(Expression, InputList)",
        	"Expression is java-script Expression, InputList is the field definition in form as Variable=Value[;Variable=Value[;...]]",
        	"BooleanExpression('A > 10 && B=='Test1'', 'A=11;B=Test1')",
        	"TRUE"
        },
        { 
					"Compress", 
					"Takes the Input string and gives out a compressed String Output", 
					"Compress(\"TEST\")", 
					"??", 
					"Compress(\"TEST\") -- ERROR while Compression", 
					"-999"
				},
				{ 
					"Uncompress", 
					"Takes the Input string in compressed form and gives out a uncompressed String Output", 
					"Uncompress(\"??\")", 
					"TEST", 
					"Uncompress(\"??\") -- ERROR while Uncompression", 
					"-999"
				},
				{ 
					"EpochToDate", 
					"Return XML DateTime format from Epoch Timestamp", 
					"EpochToDate(1428776888627)", 
					"2015-04-12T01:28:08.626"
				},
				{ 
					"DateToEpoch", 
					"Return Epoch Timestamp from XML DateTime format", 
					"DateToEpoch('2015-04-12T01:28:08.626')", 
					"1428776888627"
				},
				{ 
					"DiffTime", 
					"Return time difference in milliseconds from two different time", 
					"DiffTime('2015-04-12T01:28:08.626','2015-04-12T01:28:08.826')", 
					"200"
				},
				{ 
					"ElapsedTime", 
					"Return time difference from given time compare to now", 
					"Elapsed('2015-04-12T01:28:08.826')", 
					"200"
				},
				{ 
					"EvalFilterExpression", 
					"Evaluate the filter expression based on given fields. Return TRUE  or FALSE", 
					"EvalFilterExpression('POC=JKT,ONNET!=1', 'POC=BD, ONNET=0')", 
					"FALSE"
				},				
				{ 
					"CleanUpXML", 
					"Remove namespace and xml prefix", 
					"CleanUpXML('xmlString',true)", 
					"Remove namespace and xml prefix from xmlString and add \\<?xml version='1.0'?>"
				},				
				{ 
					"RandomUUID", 
					"UUIDs is to enable distributed systems to uniquely identify information without significant central coordination. In this context the word unique should be taken to mean practically unique rather than guaranteed unique. Since the identifiers have a finite size, it is possible for two differing items to share the same identifier. This is a form of hash collision. The identifier size and generation process need to be selected so as to make this sufficiently improbable in practice. Anyone can create a UUID and use it to identify something with reasonable confidence that the same identifier will never be unintentionally created by anyone to identify something else. Information labeled with UUIDs can therefore be later combined into a single database without needing to resolve identifier (ID) conflicts.", 
					"RandomUUID()", 
					"Return human-readable display, de305d54-75b4-431b-adb2-eb6b9e546014"
				},				
				{ 
					"URLDecode", 
					"URL decode with given encoding and control to trim", 
					"URLDecode('Text','UTF-8', true)", 
					"Decode URL characters to UTF-8"
				},				
				{ 
					"URLEncode", 
					"URL encode with given encoding and control to trim", 
					"URLEncode('Text','UTF-8', true)", 
					"Encode URL characters to UTF-8"
				}				
    };
    
     public static String WrapString(String str, int wrapLength, String newLineStr, boolean wrapLongWords) {
         int inputLineLength = str.length();
         int offset = 0;
         StringBuffer wrappedLine = new StringBuffer(inputLineLength + 32);
         
         while ((inputLineLength - offset) > wrapLength) {
             if (str.charAt(offset) == ' ') {
                 offset++;
                 continue;
             }
             int spaceToWrapAt = str.lastIndexOf(' ', wrapLength + offset);

             if (spaceToWrapAt >= offset) {
                 // normal case
 								 wrappedLine.append(str.substring(offset, spaceToWrapAt));
                 wrappedLine.append(newLineStr);
                 offset = spaceToWrapAt + 1;
                 
             } else {
                 // really long word or URL
 						if (wrapLongWords) {
                     // wrap really long word one line at a time
 										wrappedLine.append(str.substring(offset, wrapLength + offset));
                     wrappedLine.append(newLineStr);
                     offset += wrapLength;
                 } else {
                     // do not wrap really long word, just extend beyond limit
 										spaceToWrapAt = str.indexOf(' ', wrapLength + offset);
                     if (spaceToWrapAt >= 0) {
                         wrappedLine.append(str.substring(offset, spaceToWrapAt));
                         wrappedLine.append(newLineStr);
                         offset = spaceToWrapAt + 1;
                     } else {
                         wrappedLine.append(str.substring(offset));
                         offset = inputLineLength;
                     }
                 }
             }
         }

         // Whatever is left in line is short enough to just pass through
 										wrappedLine.append(str.substring(offset));

         return wrappedLine.toString();
     }
     
     public static String CenterString(String text, int len){
    		String out = String.format("%"+len+"s%s%"+len+"s", "",text,"");
    		float mid = (out.length()/2);
    		float start = mid - (len/2);
    		float end = start + len; 
    		return out.substring((int)start, (int)end);
		}

		
	  public static long GetPartition(long id, long PartitionSize){
	  	
	  	return id % PartitionSize;
	  		  	
    }
	
		public static boolean RegexMatch (String str, String regex) {
	
 		return str.matches (regex);
		}
		
		public static String RegexReplaceAll (String str, String regex, String Replacement) {

  	Pattern re = Pattern.compile(regex);
  	Matcher m = re.matcher(str);
  
 		return m.replaceAll(Replacement);
		}
		
	  public static String GetFilterExpression(String Conditions) {
		
		String [] Condition = Conditions.split(";");
		String Expression="", OperatorAND, OperatorOR, ExpressionIN;
		
		int i = 0, j=0;
		
		for (String var : Condition) {
				Pattern p = Pattern.compile("(.*)(=|<>| in )(.*)");
				Matcher m = p.matcher(var);
				
				OperatorAND = ((i == 0) ? "" : " && ");

				if (m.find()) {
					switch (m.group(2)) {
						case "=": Expression +=  OperatorAND + m.group(1) + "=='" + m.group(3) + "'"; break;
						case "<>": Expression +=  OperatorAND + m.group(1) + "!='" + m.group(3) + "'"; break;
						case " in ":
						
									String [] Values = m.group(3).split(",");
									
									j = 0; ExpressionIN = "";
									
									for (String value : Values) { 
										OperatorOR = ((j == 0) ? "" : " || ");
										
										ExpressionIN += OperatorOR + m.group(1) + "=='" + value + "'";
										j++;
									}  
									
									Expression +=  OperatorAND + "( " + ExpressionIN + " )";
									break;
						default: Expression +=  OperatorAND + var; break;
					}
				}
				
				i++;
				
		} 
		
		
		return Expression;
	  }
	  
   public static String Compress(String str){
        if (str == null || str.length() == 0) {
            return str;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = null;
        String outStr="";
        
		try {
			gzip = new GZIPOutputStream(out);
			gzip.write(str.getBytes("UTF-8"));
			gzip.close();
	        outStr = out.toString("ISO-8859-1");			
		} catch (Exception e) {		
			return ("-999");
		} finally {
			if(gzip != null)
				try {
					gzip.close();
				} catch (IOException e) {}
		}

        return outStr;
     }
    
    public static String Uncompress(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        
        String outStr="";
        BufferedReader bf = null;
        
		try {
	        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str.getBytes("ISO-8859-1")));
	        bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
	        String line;
	        while ((line=bf.readLine())!=null) {
	          outStr += line;
	        }
		} catch (Exception e) {		
			return ("-999");
		} finally {
			if(bf != null)
				try {
					bf.close();
				} catch (IOException e) { }
		}
		
		outStr = outStr.replaceAll("[\\p{C}]\\p{Zl}\\p{Zp}]"," ");
        return outStr;
    }
    
    public static String EpochToDate(long epoch)
    {
    	Date date = new Date(epoch);
    	
    	try {
    		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    			return df.format(date);
    	} catch (Exception e) {
    			return "0";
    	}
    }

    public static long DateToEpoch(String date)
    {
    	
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    	try {
				Date pdate = df.parse(date);
				return (long) pdate.getTime();
			} catch (Exception e) {
				return (-999);
			}
    }

    public static long DiffTime(String starttime, String endtime)
    {
    	
    	try {
				return DateToEpoch(endtime) - DateToEpoch(starttime);
			} catch (Exception e) {
				return (-999);
			}
    }

    public static long ElapsedTime(String starttime)
    {
    	
    	try {
				return System.currentTimeMillis() - DateToEpoch(starttime);
			} catch (Exception e) {
				return (-999);
			}
    }
    
    public static String EvalFilterExpression(String expression, String fields)
		{
			
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			int i = 0;

			String name, value;

			String [] field = fields.split(";");
			
			
			for (String var : field) {
				if(var.contains("=")) {
					engine.put(var.split("=")[0], (String)var.split("=")[1]);
					i++;
				}
			}
			

			try {
				return ((Boolean)engine.eval(GetFilterExpression(expression))) ? "true" : "false"; 
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		
		public static String EvaluateCondition(String expression, String fields)
		{
			
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			int i = 0;

			String name, value;

			String [] field = fields.split(";");
			
			
			for (String var : field) {
				if(var.contains("=")) {
					engine.put(var.split("=")[0].replaceAll("\\.", "_"), (String)var.split("=")[1]);
					i++;
				}
			}
			

			try {
				return ((Boolean)engine.eval(expression.replaceAll("\\.", "_"))) ? "true" : "false"; 
			} catch (Exception e) {
				return e.getMessage();
			}
		}

    public static String EvalThresholdExpression(String expression, String fields)
		{
			
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			int i = 0;

			String name, value;

			String [] field = fields.split(";");
			
			
			for (String var : field) {
				if(var.contains("=")) {
					engine.put(var.split("=")[0], (Double) Double.parseDouble(var.split("=")[1]));
					i++;
				}
			}
			

			try {
				return ((Boolean)engine.eval(GetFilterExpression(expression))) ? "true" : "false"; 
			} catch (Exception e) {
				return e.getMessage();
			}
		}

    public static String CalculateExpression(String expression, String fields)
		{
			
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			int i = 0;

			String varExpression, varFields, name, value;

			varExpression = expression.replaceAll("(\\[|\\]\\.|\\.)", "_");
      varFields = fields.replaceAll("(\\[|\\]\\.|\\.)", "_");
			String [] field = varFields.split(";");
						
			for (String var : field) {
				if(var.contains("=")) {
				    name = var.split("=")[0]; 

				    if (var.split("=").length == 2) { value = var.split("=")[1]; } else { value = ""; }
				    
					engine.put(name, (Double) Double.parseDouble(value));
					i++;
				}
			}
			

			try {
				return Double.toString((Double)engine.eval(expression));
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		
		public static String StringExpression(String expression, String fields)
		{
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			int i = 0;

			String varExpression, varFields, name, value;

			varExpression = expression.replaceAll("(\\[|\\]\\.|\\]|\\.)(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", "_");
			varFields = fields;
			String [] field = varFields.split(";");
						
			for (String var : field) {
				if(var.contains("=")) {
				    name = var.split("=")[0]; 
				    
				    if (var.split("=").length == 2) { value = var.split("=")[1]; } else { value = "''"; }
					
				    engine.put(name.replaceAll("(\\[|\\]\\.|\\]|\\.)", "_"), (String)value);
					
					i++;
				}
			}
			
			try {
				return ((String)engine.eval(varExpression));
			} catch (Exception e) {
				return "";
			}	
		}
		
		public static Boolean IN (String property, String fields)
		{

			String[] field = fields.split(",");
			
			return Arrays.binarySearch(field, property) >= 0;
		}
		
		public static String BooleanExpression(String expression, String fields)
		{				
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			int i = 0;

			String varExpression, varFields, name, value;

			// varExpression = expression.replaceAll("(\\[|\\]\\.|\\.)", "_");
			// varFields = fields.replaceAll("(\\[|\\]\\.|\\.)", "_");
			
			varExpression = expression.replaceAll("(\\[|\\]\\.|\\]|\\.)(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", "_");
			varFields = fields.replaceAll("(\\[|\\]\\.|\\]|\\.)(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", "_");

			String [] field = varFields.split(";");
						
			for (String var : field) {
				if(var.contains("=")) {
					name = var.split("=")[0]; 
					
					if (var.split("=").length == 2) { value = var.split("=")[1]; } else { value = "''"; }
					
					if (value.matches("true|false")) {
						engine.put(name, (Boolean) Boolean.parseBoolean(value));
					}
					else if (value.matches("[-+]?(?:\\b[0-9]+(?:\\.[0-9]*)?|\\.[0-9]+\\b)(?:[eE][-+]?[0-9]+\\b)?")) { //If number
						engine.put(name, (Double) Double.parseDouble(value));
					}
					else if(value.matches("^(19|2[0-9])\\d\\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])(.)*")){ //If Date yyyy-MM-dd
						//create JS Date Object
						try {
							engine.eval("var " + name +"="+value.replaceAll("(\\d{4})\\-(\\d{2})\\-(\\d{2})(.)*", "new Date($1,($2-1),$3)")+";");
						} catch (ScriptException e) {
							return e.getMessage();
						}
					}
					else { 
						  engine.put(name, (String)value); 
					}
					i++;
				}
			}
			
			try {
				if(varExpression.matches("(.)*(19|2[0-9])\\d\\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])(.)*")){//If expression contains Date
					//replace date pattern with JS Date Object
					varExpression=varExpression.replaceAll("\\s","").replaceAll("(\\d{4})\\-(\\d{2})\\-(\\d{2})", "new Date($1,($2-1),$3)");
				}
				if(varExpression.matches(".*\\sin\\s\\(.*\\)")) {
					varExpression=varExpression.replaceAll("\\(","\\[").replaceAll("\\)","\\]").replaceAll("(.*)\\sin\\s(\\[.*\\])", "$2\\.indexOf\\($1\\)>-1");
				}
				
				return ((Boolean)engine.eval(varExpression)) ? "true" : "false"; 
			} catch (Exception e) {
				return e.getMessage();
			}
		}		
		
		public static String RoundToMinute(String origDate, long minRound)
		{
			
			Date pdate;
						    	
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			
			try {
				pdate = df.parse(origDate);
			} catch (Exception e) {
				return (e.getMessage());
			}
			
			Date date = new Date(minRound * 60 * 1000 * ((pdate.getTime()) / (minRound * 60 * 1000)));

    	return df.format(date);
			
		}

		public static String RoundToDay(String origDate)
		{
			Date pdate, date;
			Calendar cal = Calendar.getInstance();
						    	
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			
			try {
				pdate = df.parse(origDate);
			} catch (Exception e) {
				return (e.getMessage());
			}

			cal.setTime(pdate);    	
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			date = cal.getTime();
						
    	return df.format(date);
		}
		
		public static String GetMatchPattern(String inputString, String pattern, String option)
		{
				Pattern p = Pattern.compile("(?"+ option +")(" + pattern + ")");
				Matcher m = p.matcher(inputString);
				String resultString = "";
				
				if (m.find()) {
					resultString = m.group(1);
				}
				return resultString;
		}
		
		public static String CleanUpXML(String xmlstring, Boolean AddHeader)
		{
			String xmlResult;
			
			xmlResult = xmlstring.replaceAll("\\sxmlns[^\"]+\"[^\"]+\"", ""); 
			xmlResult = xmlResult.replaceAll("(<\\/?)[a-z0-9]+:", "$1"); 
			
			return (AddHeader == true) ? "<?xml version=\"1.0\"?>\r\n" + xmlResult : xmlResult;
		}

		public static String RandomUUID()
		{
			
			return java.util.UUID.randomUUID().toString();
		}
	    
	    public static String URLDecode(String text, String encoding, boolean istrimmed) {
	    	try {
	    		if(istrimmed == true) {
	    			return URLDecoder.decode(text, encoding).replaceAll("[\\p{C}]\\p{Zl}\\p{Zp}]"," ").trim();
	    		} else {
	    			return URLDecoder.decode(text, encoding);
	    		}
	    	} catch (Exception e) {
	    		return "";
	    	}
	    }
	    
	    public static String URLEncode(String text, String encoding, boolean istrimmed) {
	    	try {
	    		if(istrimmed == true) {
	    			return URLEncoder.encode(text, encoding).replaceAll("[\\p{C}]\\p{Zl}\\p{Zp}]"," ").trim();
	    		} else {
	    			return URLEncoder.encode(text, encoding);
	    		}
	    	} catch (Exception e) {
	    		return "";
	    	}
	    }
}
