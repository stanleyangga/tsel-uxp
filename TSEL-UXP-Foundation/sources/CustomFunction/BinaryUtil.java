package com.tibco.psg;

import java.util.Random;
import java.util.UUID;
import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class BinaryUtil {
	final static Random random = new Random();
	static int sequenceNumber = 0;
	
    final static String encryptionKey = "psg2015@tibco.id";
    final static String encryptionAlgorithm = "AES";
	final static Key aesKey = new SecretKeySpec(encryptionKey.getBytes(), encryptionAlgorithm);
	
	public static int hexToInt(String hex) {
		if (hex != null) {
			try {
		        String digits = "0123456789ABCDEF";
		        hex = hex.toUpperCase();
		        int val = 0;
		        for (int i = 0; i < hex.length(); i++) {
		            char c = hex.charAt(i);
		            int d = digits.indexOf(c);
		            val = 16*val + d;
		        }
		        return val;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return 0;
	}

	public static String intToHex(int in) {
		try {
			return Integer.toHexString(in).toUpperCase();
		} catch (Exception e) {
		}

		return null;
	}

	public static String longToHex(String longString) {
		try {
			long l = Long.parseLong(longString);
			return Long.toHexString(l).toUpperCase();
		} catch (Exception e) {
		}

		return null;
	}

	public static int getBytesLength(String base64) {
		if (base64 != null) {
			try {
				byte[] bytes = decodeBase64(base64);
				return bytes.length;
			} catch (Exception e) {
			}
		}

		return 0;
	}
	
	public static String readByteArrayToString(String base64, int pos, int length) {
		String result = null;
		byte[] bytes = base64 == null ? null : decodeBase64(base64);
		int len = bytes == null ? 0 : bytes.length;
		if (len >= length) {
			byte[] temp = new byte[length];
			System.arraycopy(bytes, pos, temp, 0, length);
			result = new String(temp).trim();
		}
		return result;
	}
	
	public static int readByteArrayToInt(String base64, int pos, int length) {
		int result = 0;
		if(base64 != null) {
			byte[] bytes = decodeBase64(base64);
			int lefts = bytes.length-pos;
			byte[] temp = new byte[lefts];
			System.arraycopy(bytes, pos, temp, 0, lefts);
			int len = temp.length;
	        if (len >= 4) {
	            result |= temp[0]&0xff;
	            result <<= 8;
	            result |= temp[1]&0xff;
	            result <<= 8;
	            result |= temp[2]&0xff;
	            result <<= 8;
	            result |= temp[3]&0xff;
	            return result;
	        }
		}
		return result;
	}
	
	public static int generateSequenceNum() {
		return ++sequenceNumber;
	}
	
	public static int generateRandomInt() {
		return random.nextInt();
	}
	
	public static short generateRandomShort() {
		return (short) random.nextInt(Short.MAX_VALUE + 1);
	}
	
	public static int binaryStringToInt(String binaryText) {
		try {
			return Integer.parseInt(binaryText, 2);
		} catch (Exception e) {
		}

		return 0;
	}

	public static String intToBinaryString(int in) {
		try {
			return Integer.toBinaryString(in);
		} catch (Exception e) {
		}

		return null;
	}

	public static String byteToChar(byte in) {
		try {
			return new String(new byte[]{in});
		} catch (Exception e) {
		}

		return null;
	}

	public static byte charToByte(String in) {
		try {
			return in.getBytes()[0];
		} catch (Exception e) {
		}

		return 0;
	}
	
	public static String encrypt(String in) {
		try {
			Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
	        byte[] encrypted = cipher.doFinal(in.getBytes());
	        return new String(encodeBase64(encrypted, 0, encrypted.length));
		} catch (Exception e) {
		}

		return "";
	}
	
	public static String decrypt(String in) {
		try {
			byte[] base64 = decodeBase64(in);
			Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			return (new String(cipher.doFinal(base64))).trim();
		} catch (Exception e) {
		}

		return "";
	}
	
	public static String getsha256(String in) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] hash = md.digest(in.getBytes("UTF-8"));
	        return new String(encodeBase64(hash, 0, hash.length));
		} catch (Exception e) {
		}

		return "";
	}
	
	public static String shortUUID(String in, boolean trim) {
		try {
			UUID uuid = (in != null ? UUID.fromString(in) : UUID.randomUUID());
			
	    	byte[] uuidArr = new byte[16];

	    	for (int i = 0; i < 8; i++) {
	    		uuidArr[i] = (byte) (uuid.getMostSignificantBits() >>> 8 * (7 - i));
	    	}
	    	for (int i = 8; i < 16; i++) {
	    		uuidArr[i] = (byte) (uuid.getLeastSignificantBits() >>> 8 * (7 - i));
	    	}

	    	String base64 = new String(encodeBase64(uuidArr, 0, 16));
	    	
			if(trim)
				return base64.split("=")[0];
	    	
			return base64;
		} catch (Exception e) {
		}

		return "";
	}
	
	public static final String[][] HELP_STRINGS = {
		{ "hexToInt", "Convert Hex string into Integer", "hexToInt(00FA)", "250" },
		{ "intToHex", "Convert Integer into Hex string", "intToHex(250)", "FA" },
		{ "getBytesLength", "Get bytes length of a string", "getBytesLength(0200F238050140A18000)", "20" },
		{ "readByteArrayToInt", "Read base64 as integer", "readByteArrayToInt(AAAAPAAAAAQAAAAAAAAABQACCDU1NQABATU1NTU1NTU1NQAAAAAAAAAAAAAPSGVsbG8gd2lraXBlZGlh, 0, 4)", "60" },
		{ "readByteArrayToString", "Read byte array as string", "readByteArrayToString(AAAAPAAAAAQAAAAAAAAABQACCDU1NQABATU1NTU1NTU1NQAAAAAAAAAAAAAPSGVsbG8gd2lraXBlZGlh, 19, 4)", "555" },
		{ "generateSequenceNum", "Generate sequence number int", "generateSequenceNum()", "1" },
		{ "generateRandomInt", "Generate random int", "generateRandomInt()", "0" },
		{ "generateRandomShort", "Generate random short", "generateRandomShort()", "0" },
		{ "binaryStringToInt", "Read binary string as integer", "binaryStringToInt(1100)", "13" },
		{ "intToBinaryString", "Read integer as binary string", "intToBinaryString(12)", "1100" },
		{ "byteToChar", "Read byte as char", "byteToChar(78)", "N" },
		{ "charToByte", "Read char as byte", "charToByte(Y)", "89" },
		{ "encrypt", "encrypt text in base64 result", "encrypt(Hello)", "GYY/YeAssq+BLSaczWOopQ==" },
		{ "decrypt", "decrypt text of base64", "decrypt(GYY/YeAssq+BLSaczWOopQ==)", "Hello" },
		{ "shortUUID", "shorten UUID to 16 bytes length in trimmed base64", "shortUUID(c8d9e45f-1af7-4018-a3cb-8c4cec68f49d, true)", "yNnkXxr3QBijy4xM7Gj0nQ" },
		{ "getsha256", "get sha256 in base64", "getsha256(12345678902017-04-12T07:12:00ZUXP)", "Tw6pOKcwk91VBxALL34nhpNHCrUIj+51Aq5viqH7xAM=" }
	};
	
	public static void main(String[] args) {
		String test1 = "AAAAEIAAAAQAAABlAAAABA==";
		System.out.println(getBytesLength(test1));
		System.out.println(readByteArrayToInt(test1, 0, 4));
		System.out.println(intToHex(readByteArrayToInt(test1, 4, 4)));
		System.out.println(readByteArrayToInt(test1, 8, 4));
		System.out.println(readByteArrayToInt(test1, 12, 4));
	}

	private static final char[] map1 = new char[64];
	static {
		int i = 0;
		for (char c = 'A'; c <= 'Z'; c++)
			map1[i++] = c;
		for (char c = 'a'; c <= 'z'; c++)
			map1[i++] = c;
		for (char c = '0'; c <= '9'; c++)
			map1[i++] = c;
		map1[i++] = '+';
		map1[i++] = '/';
	}
	
	private static final byte[] map2 = new byte[128];
	static {
		for (int i = 0; i < map2.length; i++)
			map2[i] = -1;
		for (int i = 0; i < 64; i++)
			map2[map1[i]] = (byte) i;
	}

	public byte[] removeBytes(String base64, int pos, int length) {
		byte[] bytes = base64 == null ? null : decodeBase64(base64);
		int len = bytes == null ? 0 : bytes.length;
		int lefts = len - pos;
		if (lefts > 0) {
			byte[] newBuf = new byte[lefts];
			System.arraycopy(bytes, length, newBuf, 0, lefts);
			bytes = newBuf;
		} else {
			bytes = null;
		}
		return bytes;
	}

	private static char[] encodeBase64(byte[] in, int iOff, int iLen) {
		int oDataLen = (iLen * 4 + 2) / 3; // output length without padding
		int oLen = ((iLen + 2) / 3) * 4; // output length including padding
		char[] out = new char[oLen];
		int ip = iOff;
		int iEnd = iOff + iLen;
		int op = 0;
		while (ip < iEnd) {
			int i0 = in[ip++] & 0xff;
			int i1 = ip < iEnd ? in[ip++] & 0xff : 0;
			int i2 = ip < iEnd ? in[ip++] & 0xff : 0;
			int o0 = i0 >>> 2;
			int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
			int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
			int o3 = i2 & 0x3F;
			out[op++] = map1[o0];
			out[op++] = map1[o1];
			out[op] = op < oDataLen ? map1[o2] : '=';
			op++;
			out[op] = op < oDataLen ? map1[o3] : '=';
			op++;
		}
		return out;
	}
	
	private static byte[] decodeBase64(String base64) {
		char[] in = base64.toCharArray();
		int iOff = 0;
		int iLen = in.length;
		
		if (iLen % 4 != 0)
			throw new IllegalArgumentException(
					"Length of Base64 encoded input string is not a multiple of 4.");
		while (iLen > 0 && in[iOff + iLen - 1] == '=')
			iLen--;
		int oLen = (iLen * 3) / 4;
		byte[] out = new byte[oLen];
		int ip = iOff;
		int iEnd = iOff + iLen;
		int op = 0;
		while (ip < iEnd) {
			int i0 = in[ip++];
			int i1 = in[ip++];
			int i2 = ip < iEnd ? in[ip++] : 'A';
			int i3 = ip < iEnd ? in[ip++] : 'A';
			if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127)
				throw new IllegalArgumentException(
						"Illegal character in Base64 encoded data.");
			int b0 = map2[i0];
			int b1 = map2[i1];
			int b2 = map2[i2];
			int b3 = map2[i3];
			if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0)
				throw new IllegalArgumentException(
						"Illegal character in Base64 encoded data.");
			int o0 = (b0 << 2) | (b1 >>> 4);
			int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
			int o2 = ((b2 & 3) << 6) | b3;
			out[op++] = (byte) o0;
			if (op < oLen)
				out[op++] = (byte) o1;
			if (op < oLen)
				out[op++] = (byte) o2;
		}
		return out;
	}

	private static String byteArrayToHex(byte[] bytes) {
		String dump = "";
		try {
			int dataLen = bytes == null ? 0 : bytes.length;
			for (int i = 0; i < dataLen; i++) {
				dump += Character.forDigit((bytes[i] >> 4) & 0x0f, 16);
				dump += Character.forDigit(bytes[i] & 0x0f, 16);
			}
		} catch (Throwable t) {
			dump = "Throwable caught when dumping = " + t;
		}
		return dump;
	}
	
	private static byte[] hexToByteArray(String hexString) {
	    int len = hexString.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
	                             + Character.digit(hexString.charAt(i+1), 16));
	    }
	    return data;
	}
}
