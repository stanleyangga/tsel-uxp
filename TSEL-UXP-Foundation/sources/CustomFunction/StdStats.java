package com.tibco.psg;

/*************************************************************************
 *  Compilation:  javac StdStats.java
 *  Execution:    java StdStats < input.txt
 *
 *  Library of statistical functions.
 *
 *  The test client reads an array of real numbers from standard
 *  input, and computes the minimum, mean, maximum, and
 *  standard deviation.
 *
 *  The functions all throw a NullPointerException if the array
 *  passed in is null.
 *
 *  The floating-point functions all return NaN if any input is NaN.
 *
 *  Unlike Math.min() and Math.max(), the min() and max() functions
 *  do not differentiate between -0.0 and 0.0.
 *
 *  % more tiny.txt
 *  5
 *  3.0 1.0 2.0 5.0 4.0
 *
 *  % java StdStats < tiny.txt
 *         min   1.000
 *        mean   3.000
 *         max   5.000
 *     std dev   1.581
 *
 *  Should these funtions use varargs instead of array arguments?
 *
 *************************************************************************/

/**
 *  <i>Standard statistics</i>. This class provides methods for computing
 *  statistics such as min, max, mean, sample standard deviation, and
 *  sample variance.
 *  <p>
 *  For additional documentation, see
 *  <a href="http://introcs.cs.princeton.edu/22library">Section 2.2</a> of
 *  <i>Introduction to Programming in Java: An Interdisciplinary Approach</i>
 *  by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */

import java.util.Arrays;

public class StdStats
{
	
		protected int mInternalValue;
 
      /**
      * The following method will not be available because it is 
      * a constructor.
      */
    public StdStats(int value)
      {
         mInternalValue = value;
      }
      
    public static final String[][] HELP_STRINGS = {
        {
        	"GetPartition", 
        	"Returns partitionid from id	 -> GetPartition(id, PartitionSize)", 
        	"GetPartition(62811880683, 5000000)",
        	"1880683"
        },
        {
        	"RegexMatch", 
        	"Returns true if str matches the regex matchpattern	 -> RegexMatch(str, matchpattern)", 
        	"RegexMatch('a=b','.*=.*')",
        	"TRUE"
        },
        {	
        	"RegexReplaceAll", 
        	"Returns replacement string of str with 	 -> RegexMatch(str, matchpattern)", 
        	"RegexMatch('a=b','(.*)=(.*)', '%1==%2')",
        	"'a==b'"
        },
        {
        	"GetFilterExpression", 
        	"Returns filter expression in Java-Script from the Business Expression GetFilterExpression(name=value,name=value)", 
        	"GetFilterExpression('POC=JKT')",
        	"POC==JKT",
        	"GetFilterExpression('POC=JKT;ONNET=1,0,2')",
        	"POC=='JKT' && ( ONNET=='1' || ONNET=='0' || ONNET=='2' )",
        	"GetFilterExpression('POC=JKT;ONNET!=1')",
        	"POC=='JKT' && ONNET!='1'"
        },
        {
        	"StringExpression", 
        	"Returns result of string concatenation from the java-script Expression", 
        	"StringExpression(Expression, InputList)",
        	"Expression is java-script String Expression, InputList is the field definition in form as Variable=Value[;Variable=Value[;...]]",
        	"StringExpression('A + ' ' + B', 'A=Hello;B=World')",
        	"Hello World"
        },
        {
        	"Boolean", 
        	"Returns boolean from the java-script Expression", 
        	"BooleanExpression(Expression, InputList)",
        	"Expression is java-script Expression, InputList is the field definition in form as Variable=Value[;Variable=Value[;...]]",
        	"BooleanExpression('A > 10 && B=='Test'', 'A=11;B=Test')",
        	"TRUE"
        },
        { 
					"Compress", 
					"Takes the Input string and gives out a compressed String Output", 
					"Compress(\"TEST\")", 
					"??", 
					"Compress(\"TEST\") -- ERROR while Compression", 
					"-999"
				},
				{ 
					"Uncompress", 
					"Takes the Input string in compressed form and gives out a uncompressed String Output", 
					"Uncompress(\"??\")", 
					"TEST", 
					"Uncompress(\"??\") -- ERROR while Uncompression", 
					"-999"
				},
				{ 
					"EpochToDate", 
					"Return XML DateTime format from Epoch Timestamp", 
					"EpochToDate(1428776888627)", 
					"2015-04-12T01:28:08.626"
				},
				{ 
					"DateToEpoch", 
					"Return Epoch Timestamp from XML DateTime format", 
					"DateToEpoch('2015-04-12T01:28:08.626')", 
					"1428776888627"
				},
				{ 
					"DiffTime", 
					"Return time difference in milliseconds from two different time", 
					"DiffTime('2015-04-12T01:28:08.626','2015-04-12T01:28:08.826')", 
					"200"
				},
				{ 
					"ElapsedTime", 
					"Return time difference from given time compare to now", 
					"Elapsed('2015-04-12T01:28:08.826')", 
					"200"
				},
				{ 
					"EvalFilterExpression", 
					"Evaluate the filter expression based on given fields. Return TRUE  or FALSE", 
					"EvalFilterExpression('POC=JKT,ONNET!=1', 'POC=BD, ONNET=0')", 
					"FALSE"
				},				
				{ 
					"CleanUpXML", 
					"Remove namespace and xml prefix", 
					"CleanUpXML('xmlString',true)", 
					"Remove namespace and xml prefix from xmlString and add \\<?xml version='1.0'?>"
				},				
				{ 
					"RandomUUID", 
					"UUIDs is to enable distributed systems to uniquely identify information without significant central coordination. In this context the word unique should be taken to mean practically unique rather than guaranteed unique. Since the identifiers have a finite size, it is possible for two differing items to share the same identifier. This is a form of hash collision. The identifier size and generation process need to be selected so as to make this sufficiently improbable in practice. Anyone can create a UUID and use it to identify something with reasonable confidence that the same identifier will never be unintentionally created by anyone to identify something else. Information labeled with UUIDs can therefore be later combined into a single database without needing to resolve identifier (ID) conflicts.", 
					"RandomUUID()", 
					"Return human-readable display, de305d54-75b4-431b-adb2-eb6b9e546014"
				}				
	};

	private static double[] parseDoubleArray(String s) {

		if (s == null ||) { return new double[]; }

		String [] sa = s.split(";");
		double[] a = new double[sa.length];
		for (int i = 0; i<sa.length; i++) a[i] = Double.valueOf(sa[i]);

		return a;
	}
	
    /**
      * Returns the maximum value in the array a[], -infinity if no such value.
      */
    public static double max(String s) {
    	
    		return max(parseDoubleArray(s));
    }

    private static double max(double[] a) {
    	
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < a.length; i++) {
            if (Double.isNaN(a[i])) return Double.NaN;
            if (a[i] > max) max = a[i];
        }
        return max;
    }

   /**
     * Returns the maximum value in the array a[], Integer.MIN_VALUE if no such value.
     */
    private static int max(int[] a) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max) max = a[i];
        }
        return max;
    }
    
    /**
      * Returns the maximum value in the subarray a[lo..hi], -infinity if no such value.
      */
    private static double max(double[] a, int lo, int hi) {
        if (lo < 0 || hi >= a.length || lo > hi)
            throw new RuntimeException("Subarray indices out of bounds");
        double max = Double.NEGATIVE_INFINITY;
        for (int i = lo; i <= hi; i++) {
            if (Double.isNaN(a[i])) return Double.NaN;
            if (a[i] > max) max = a[i];
        }
        return max;
    }



   /**
     * Returns the minimum value in the array a[], +infinity if no such value.
     */
    public static double min(String s) {
				return min(parseDoubleArray(s));
    }

    private static double min(double[] a) {

        double min = Double.POSITIVE_INFINITY;
        for (int i = 0; i < a.length; i++) {
            if (Double.isNaN(a[i])) return Double.NaN;
            if (a[i] < min) min = a[i];
        }
        return min;
    }

   /**
      * Returns the minimum value in the subarray a[lo..hi], +infinity if no such value.
      */
    private static double min(double[] a, int lo, int hi) {
        if (lo < 0 || hi >= a.length || lo > hi)
            throw new RuntimeException("Subarray indices out of bounds");
        double min = Double.POSITIVE_INFINITY;
        for (int i = lo; i <= hi; i++) {
            if (Double.isNaN(a[i])) return Double.NaN;
            if (a[i] < min) min = a[i];
        }
        return min;
    }

   /**
     * Returns the minimum value in the array a[], Integer.MAX_VALUE if no such value.
     */
    private static int min(int[] a) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < a.length; i++) {
            if (a[i] < min) min = a[i];
        }
        return min;
    }




   /**
     * Returns the average value in the array a[], NaN if no such value.
     */
    public static double mean(String s) {
				return mean(parseDoubleArray(s));
    }

    private static double mean(double[] a) {

        if (a.length == 0) return Double.NaN;
        double sum = sum(a);
        return sum / a.length;
    }
    
      /**
     * Returns the average value in the subarray a[lo..hi], NaN if no such value.
     */
    private static double mean(double[] a, int lo, int hi) {
        int length = hi - lo + 1;
        if (lo < 0 || hi >= a.length || lo > hi)
            throw new RuntimeException("Subarray indices out of bounds");
        if (length == 0) return Double.NaN;
        double sum = sum(a, lo, hi);
        return sum / length;
    }

   /**
     * Returns the average value in the array a[], NaN if no such value.
     */
    private static double mean(int[] a) {
        if (a.length == 0) return Double.NaN;
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum = sum + a[i];
        }
        return sum / a.length;
    }



   /**
     * Returns the sample variance in the array a[], NaN if no such value.
     */
    public static double var(String s) {
				return var(parseDoubleArray(s));
    }

    private static double var(double[] a) {

        if (a.length == 0) return Double.NaN;
        double avg = mean(a);
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum += (a[i] - avg) * (a[i] - avg);
        }
        return sum / (a.length - 1);
    }

   /**
     * Returns the sample variance in the subarray a[lo..hi], NaN if no such value.
     */
    private static double var(double[] a, int lo, int hi) {
        int length = hi - lo + 1;
        if (lo < 0 || hi >= a.length || lo > hi)
            throw new RuntimeException("Subarray indices out of bounds");
        if (length == 0) return Double.NaN;
        double avg = mean(a, lo, hi);
        double sum = 0.0;
        for (int i = lo; i <= hi; i++) {
            sum += (a[i] - avg) * (a[i] - avg);
        }
        return sum / (length - 1);
    }

   /**
     * Returns the sample variance in the array a[], NaN if no such value.
     */
    private static double var(int[] a) {
        if (a.length == 0) return Double.NaN;
        double avg = mean(a);
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum += (a[i] - avg) * (a[i] - avg);
        }
        return sum / (a.length - 1);
    }

   /**
     * Returns the population variance in the array a[], NaN if no such value.
     */
    public static double varp(String s) {

				return varp(parseDoubleArray(s));
    }

    private static double varp(double[] a) {
        if (a.length == 0) return Double.NaN;
        double avg = mean(a);
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum += (a[i] - avg) * (a[i] - avg);
        }
        return sum / a.length;
    }

   /**
     * Returns the population variance in the subarray a[lo..hi], NaN if no such value.
     */
    private static double varp(double[] a, int lo, int hi) {
        int length = hi - lo + 1;
        if (lo < 0 || hi >= a.length || lo > hi)
            throw new RuntimeException("Subarray indices out of bounds");
        if (length == 0) return Double.NaN;
        double avg = mean(a, lo, hi);
        double sum = 0.0;
        for (int i = lo; i <= hi; i++) {
            sum += (a[i] - avg) * (a[i] - avg);
        }
        return sum / length;
    }


   /**
     * Returns the sample standard deviation in the array a[], NaN if no such value.
     */
     
    public static double stddev(String s) {
  			return stddev(parseDoubleArray(s));
    } 
     
    private static double stddev(double[] a) {
        return Math.sqrt(var(a));
    }

   /**
     * Returns the sample standard deviation in the subarray a[lo..hi], NaN if no such value.
     */
    private static double stddev(double[] a, int lo, int hi) {
        return Math.sqrt(var(a, lo, hi));
    }

   /**
     * Returns the sample standard deviation in the array a[], NaN if no such value.
     */
    private static double stddev(int[] a) {
        return Math.sqrt(var(a));
    }

   /**
     * Returns the population standard deviation in the array a[], NaN if no such value.
     */
    public static double stddevp(String s) {
        return stddevp(parseDoubleArray(s));
    }
    
     
    private static double stddevp(double[] a) {
        return Math.sqrt(varp(a));
    }

   /**
     * Returns the population standard deviation in the subarray a[lo..hi], NaN if no such value.
     */
    private static double stddevp(double[] a, int lo, int hi) {
        return Math.sqrt(varp(a, lo, hi));
    }

   /**
     * Returns the sum of all values in the array a[].
     */
		public static double sum(String s) {
				return sum(parseDoubleArray(s));
		}

    private static double sum(double[] a) {
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }

   /**
     * Returns the sum of all values in the subarray a[lo..hi].
     */
    private static double sum(double[] a, int lo, int hi) {
        if (lo < 0 || hi >= a.length || lo > hi)
            throw new RuntimeException("Subarray indices out of bounds");
        double sum = 0.0;
        for (int i = lo; i <= hi; i++) {
            sum += a[i];
        }
        return sum;
    }

   /**
     * Returns the sum of all values in the array a[].
     */
    private static int sum(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }
    
    
    public static double percentile(String s, double percentile) { 
    	
    		return percentile(parseDoubleArray(s), percentile);
    
  	}

    private static double percentile(double[] data, double percentile) { 
  	
    		Arrays.sort(data);
    		double index = percentile*(data.length-1);
    		int lower = (int)Math.floor(index);
    	
    		if(lower<0) { // should never happen, but be defensive
       		return data[0];
    		}
    	
    		if(lower>=data.length-1) { // only in 100 percentile case, but be defensive
       		return data[data.length-1];
    		}
    
    		double fraction = index-lower;
    		
    		// linear interpolation
    		double result=data[lower] + fraction*(data[lower+1]-data[lower]);
    		return result;
 		}		
  	


}
