/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw.sensor;

import java.io.IOException;

import com.tibco.sonar.plugins.bw.language.Process;
import com.tibco.sonar.plugins.bw.metric.BusinessWorksMetrics;
import com.tibco.sonar.plugins.bw.resource.ProcessFileResource;

import org.sonar.api.batch.SensorContext;
import org.sonar.api.component.ResourcePerspectives;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.Metric;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.Resource;
import org.sonar.api.scan.filesystem.ModuleFileSystem;

public class ProcessMetricSensor extends AbstractMetricSensor {

	public ProcessMetricSensor(
			ModuleFileSystem fileSystem,
			ResourcePerspectives resourcePerspectives) {
		super(fileSystem, resourcePerspectives, Process.KEY);
	}

	@SuppressWarnings("rawtypes")
	private Resource processFileResource;
	private int isBWproject = 1;

	@Override
	public void analyse(Project project, SensorContext sensorContext) {
		this.project = project;
		this.sensorContext = sensorContext;
		super.analyse(project, sensorContext);
		if(sensorContext.getMeasure(BusinessWorksMetrics.BWLANGUAGEFLAG) == null)
			sensorContext.saveMeasure(BusinessWorksMetrics.BWLANGUAGEFLAG, (double) isBWproject);
	}

	protected void analyseFile(java.io.File file) {
		processFileResource = sensorContext.getResource(ProcessFileResource.fromIOFile(file, project));
		com.tibco.utils.bw.model.Process process = new com.tibco.utils.bw.model.Process();
		try {
			process.setProcessXmlDocument(file).parse();		
			int groupsProcess = process.countAllGroups();
			int activitiesProcess = process.countAllActivities();
			int transitionsProcess = process.countAllTransitions();
			int processesProcess = 1;
			saveMeasure(BusinessWorksMetrics.PROCESSES, (double) processesProcess);
			saveMeasure(BusinessWorksMetrics.GROUPS, (double) groupsProcess);
			saveMeasure(BusinessWorksMetrics.ACTIVITIES, (double) activitiesProcess);
			saveMeasure(BusinessWorksMetrics.TRANSITIONS, (double) transitionsProcess);
		} catch (IOException e) {
			// Should not happen
		}
	}

	private void saveMeasure(Metric metric, double value) {
		sensorContext.saveMeasure(processFileResource, new Measure(metric, value));
	}


}
