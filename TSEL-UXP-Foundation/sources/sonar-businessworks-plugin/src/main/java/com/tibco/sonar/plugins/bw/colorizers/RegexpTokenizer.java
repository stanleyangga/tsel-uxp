package com.tibco.sonar.plugins.bw.colorizers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.sonar.channel.CodeReader;
import org.sonar.colorizer.HtmlCodeBuilder;

public class RegexpTokenizer extends AbstractNotThreadSafeTokenizer {

	private final String tagBefore;
	private final String tagAfter;
	private final Matcher matcher;
	private final StringBuilder tmpBuilder = new StringBuilder();

	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory
			.getLogger(RegexpTokenizer.class.getName());

	/**
	 * @param tagBefore
	 *            Html tag to add before the token
	 * @param tagAfter
	 *            Html tag to add after the token
	 * @param regexp
	 *            Regular expression which must be used to match token
	 */
	public RegexpTokenizer(String tagBefore, String tagAfter, String regexp) {
		this.tagBefore = tagBefore;
		this.tagAfter = tagAfter;
		this.matcher = Pattern.compile(regexp).matcher("");
	}

	@Override
	public boolean consume(CodeReader code, HtmlCodeBuilder codeBuilder) {
		getLogger().debug("   ***** consume *****   ");
		Boolean result;
		open(code, codeBuilder);
		if (code.popTo(matcher, tmpBuilder) > 0) {
			codeBuilder.appendWithoutTransforming(tagBefore);
			codeBuilder.append(tmpBuilder);
			codeBuilder.appendWithoutTransforming(tagAfter);
			tmpBuilder.delete(0, tmpBuilder.length());
			result = true;
		}else{
			result = false;
		}
		close(code, codeBuilder);		
		return result;
	}

	@Override
	public RegexpTokenizer clone() {
		return new RegexpTokenizer(tagBefore, tagAfter, matcher.pattern()
				.pattern());
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
}
