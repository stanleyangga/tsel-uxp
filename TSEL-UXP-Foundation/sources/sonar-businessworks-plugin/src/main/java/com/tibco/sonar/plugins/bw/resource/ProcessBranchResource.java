package com.tibco.sonar.plugins.bw.resource;

import org.sonar.api.resources.Language;
import org.sonar.api.resources.Resource;

@SuppressWarnings({ "rawtypes", "serial" })
public class ProcessBranchResource extends Resource implements
		ProcessBranch {

	private ProcessBranch processParentResource;

	public ProcessBranchResource(ProcessGroupResource processGroupResource) {
		this.processParentResource = processGroupResource;
	}

	public ProcessBranchResource(ProcessFileResource processFileResource) {
		this.processParentResource = processFileResource;
	}

	public String getName() {
		return processParentResource.getName();
	}

	public String getLongName() {
		return processParentResource.getLongName();
	}

	public String getDescription() {
		return processParentResource.getDescription();
	}

	public Language getLanguage() {
		return processParentResource.getLanguage();
	}

	public String getScope() {
		return processParentResource.getScope();
	}

	public String getQualifier() {
		return processParentResource.getQualifier();
	}

	public Resource getParent() {
		return processParentResource.getParent();
	}

	public boolean matchFilePattern(String antPattern) {
		return processParentResource.matchFilePattern(antPattern);
	}

	public boolean isProcessRoot() {
		return processParentResource.isProcessRoot();
	}

	public ProcessBranchResource asBranchResource() {
		return this;
	}

}
