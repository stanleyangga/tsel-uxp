package com.tibco.sonar.plugins.bw.resource;

import org.sonar.api.resources.Language;
import org.sonar.api.resources.Qualifiers;
import org.sonar.api.resources.Resource;
import org.sonar.api.resources.Scopes;
import org.sonar.api.utils.WildcardPattern;

import com.tibco.sonar.plugins.bw.language.Process;

public class ProcessActivityResource extends Resource<ProcessBranchResource> implements ProcessChildResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5645390152234821914L;

	public static final String SCOPE = Scopes.BLOCK_UNIT;

	private String activityName;
	private ProcessBranchResource parent;
	private String qualifier = Qualifiers.METHOD;

	public ProcessActivityResource(ProcessBranchResource parent, String activityName) {
		this.parent = parent;
		setKey(activityName);
	}
	
	@Override
	public String getName() {
		return activityName;
	}

	@Override
	public String getLongName() {
		return getKey();
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public Language getLanguage() {
		return Process.INSTANCE;
	}

	@Override
	public String getScope() {
		return SCOPE;
	}

	@Override
	public String getQualifier() {
		return qualifier;
	}

	@Override
	public ProcessBranchResource getParent() {
		return parent;
	}

	@Override
	public boolean matchFilePattern(String antPattern) {
		WildcardPattern matcher = WildcardPattern.create(antPattern, "/");
	    return matcher.match(getKey());
	}

	public boolean isProcessLeaf() {
		return true;
	}

}
