package com.tibco.sonar.plugins.bw.check.activity.http.request;

import org.sonar.check.Cardinality;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.check.RuleProperty;

import com.tibco.sonar.plugins.bw.check.AbstractProcessHardCodedCheck;

@Rule(key = HardCodedHostCheck.RULE_KEY, priority = Priority.MAJOR, cardinality = Cardinality.SINGLE)
public class HardCodedHostCheck extends AbstractProcessHardCodedCheck {

	public static final String RULE_KEY = "HTTPRequestHostHardCoded";
	
	protected static final String CONFIG_XPATH_DEFAULT = "//serverhost";
	protected static final String INPUTBINDING_XPATH_DEFAULT = "//RequestActivityInput/Host";
	protected static final String ACTIVITY_TYPE_DEFAULT = "com.tibco.plugin.http.client.HttpRequestActivity";
	protected static final String MESSAGE_DEFAULT = "Host hard coded in HTTP Request Reply activity";
	
	@RuleProperty(key = "configXPath", type = "TEXT", defaultValue=CONFIG_XPATH_DEFAULT)
	private String configXPath = CONFIG_XPATH_DEFAULT;
	
	@RuleProperty(key = "inputBindingXPath", type = "TEXT", defaultValue=INPUTBINDING_XPATH_DEFAULT)
	private String inputBindingXPath = INPUTBINDING_XPATH_DEFAULT;

	@RuleProperty(key = "activityType", defaultValue=ACTIVITY_TYPE_DEFAULT)
	private String activityType = ACTIVITY_TYPE_DEFAULT;

	@RuleProperty(key = "message", type="TEXT", defaultValue=MESSAGE_DEFAULT)
	private String message = MESSAGE_DEFAULT;
	
	public String getConfigXPath() {
		return configXPath;
	}

	public void setConfigXPath(String configXPath) {
		this.configXPath = configXPath;
	}

	public String getInputBindingXPath() {
		return inputBindingXPath;
	}

	public void setInputBindingXPath(String inputBindingXPath) {
		this.inputBindingXPath = inputBindingXPath;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
