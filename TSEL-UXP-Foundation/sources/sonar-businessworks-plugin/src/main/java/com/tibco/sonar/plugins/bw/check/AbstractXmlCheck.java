/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.check;

import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.source.XmlSource;

/**
 * Abtract superclass for checks.
 *
 * @author Matthijs Galesloot
 */
public abstract class AbstractXmlCheck extends AbstractCheck{

	protected abstract <S extends XmlSource> void validate(S xmlSource);

	@Override
	public <S extends Source> S validate(S source) {
		if(source instanceof XmlSource){
			validate((XmlSource)source);
		}
		return source;
	}
}
