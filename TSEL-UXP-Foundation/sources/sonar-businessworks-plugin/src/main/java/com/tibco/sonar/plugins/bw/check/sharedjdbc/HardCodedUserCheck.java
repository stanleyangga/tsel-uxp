/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw.check.sharedjdbc;

import org.sonar.check.BelongsToProfile;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.tibco.sonar.plugins.bw.check.AbstractXmlCheck;
import com.tibco.sonar.plugins.bw.profile.CommonRulesSonarWayProfile;
import com.tibco.sonar.plugins.bw.source.XmlSource;
import com.tibco.sonar.plugins.bw.violation.DefaultViolation;
import com.tibco.sonar.plugins.bw.violation.Violation;
import com.tibco.utils.bw.helper.XmlHelper;

@Rule(key = HardCodedUserCheck.RULE_KEY, priority = Priority.MAJOR)
@BelongsToProfile(title = CommonRulesSonarWayProfile.SONAR_WAY_PROFILE_NAME, priority = Priority.MAJOR)
public class HardCodedUserCheck extends AbstractXmlCheck {

	public static final String RULE_KEY = "SharedJdbcHardCodedUser";
	public static final String CONFIG_ELEMENT_NAME = "config";
	public static final String USER_ELEMENT_NAME = "user";
	public static final String USER_ELEMENT_DESC = "Shared JDBC connection resource user";

	@Override
	protected void validate(XmlSource xmlSource) {
		Document document = xmlSource.getDocument(false);
		try{
			Element config = XmlHelper.firstChildElement(document.getDocumentElement(), CONFIG_ELEMENT_NAME);
			if(config.hasChildNodes()){
				xmlSource.findAndValidateHardCodedChild(getRule(), config, USER_ELEMENT_NAME, USER_ELEMENT_DESC);
			}else{
				Violation violation = new DefaultViolation(getRule(), xmlSource.getLineForNode(config), "Shared JDBC connection resource configuration is empty");
				xmlSource.addViolation(violation);
			}
		}catch (Exception e) {
			Violation violation = new DefaultViolation(getRule(), 1, "No configuration found in shared JDBC connection resource");
			xmlSource.addViolation(violation);
		}
	}


}
