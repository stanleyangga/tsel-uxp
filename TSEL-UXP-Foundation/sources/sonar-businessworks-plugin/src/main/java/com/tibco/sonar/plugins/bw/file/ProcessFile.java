/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.file;


import com.tibco.sonar.plugins.bw.resource.ProcessFileResource;

import java.io.File;

/**
 * Checks and analyzes report measurements, issues and other findings in WebSourceCode.
 *
 * @author Matthijs Galesloot
 */
public class ProcessFile extends XmlFile{
	
  protected ProcessFileResource processFile;
  
  public ProcessFile(ProcessFileResource sonarFile, File file) {
	super(file);
	this.processFile = sonarFile;
  }
  
  public ProcessFileResource getFileResource(){
	  return this.processFile;
  }

  public void FileResource(ProcessFileResource processFileResource){
	  this.processFile = processFileResource;
  }

}
