/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw.rulerepository;

import org.sonar.api.rules.AnnotationRuleParser;
import org.sonar.api.rules.Rule;
import org.sonar.api.rules.RuleRepository;

import com.google.common.collect.ImmutableList;
import com.tibco.sonar.plugins.bw.language.Process;

import java.util.List;

/**
 * Replacement for org.sonar.plugins.squid.SquidRuleRepository
 */
public class ProcessRuleRepository extends RuleRepository {

	public static final String REPOSITORY_KEY = "process";
	public static final String REPOSITORY_NAME = "SonarQube";

	private final AnnotationRuleParser annotationRuleParser;

	public ProcessRuleRepository(AnnotationRuleParser annotationRuleParser) {
		super(REPOSITORY_KEY, Process.KEY);
		setName(REPOSITORY_NAME);
		this.annotationRuleParser = annotationRuleParser;
	}

	@Override
	public List<Rule> createRules() {
		return annotationRuleParser.parse(REPOSITORY_KEY, getChecks());
	}

	@SuppressWarnings("rawtypes")
	public static List<Class> getChecks() {
		return ImmutableList.<Class> of(
			// Common
			com.tibco.sonar.plugins.bw.check.process.NoDescriptionCheck.class,
			// Catch activities
			com.tibco.sonar.plugins.bw.check.activity.catcherror.CustomCatchCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.catcherror.CatchAllCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.catcherror.CatchBWExceptionCheck.class,
			// Generic Reusable Rule for Hard Coded Values
			com.tibco.sonar.plugins.bw.check.activity.CustomHardCodedCheck.class,
			// HTTP Request Activity
			com.tibco.sonar.plugins.bw.check.activity.http.request.HardCodedHostCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.http.request.HardCodedPortCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.http.request.HardCodedTimeoutCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.http.request.HardCodedUriCheck.class,
			// HTTP Receiver Activity
			com.tibco.sonar.plugins.bw.check.activity.jms.queue.receiver.HardCodedDestinationCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.jms.queue.receiver.HardCodedMaxSessionsCheck.class,
			// JMS Queue Request Reply Activity
			com.tibco.sonar.plugins.bw.check.activity.jms.queue.requestor.HardCodedDestinationCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.jms.queue.requestor.HardCodedTimeoutCheck.class,
			// JMS Queue Sender Activity
			com.tibco.sonar.plugins.bw.check.activity.jms.queue.sender.HardCodedDestinationCheck.class,
			// JMS Topic Publisher Activity
			com.tibco.sonar.plugins.bw.check.activity.jms.topic.publisher.HardCodedDestinationCheck.class,
			// JMS Topic Subscriber
			com.tibco.sonar.plugins.bw.check.activity.jms.topic.subscriber.HardCodedDestinationCheck.class,
			// SOAP Receiver Activity
			com.tibco.sonar.plugins.bw.check.activity.soap.receiver.HardCodedSoapActionCheck.class,
			// SOAP Request Reply Activity
			com.tibco.sonar.plugins.bw.check.activity.soap.request.HardCodedSoapActionCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.soap.request.HardCodedTimeoutCheck.class,
			com.tibco.sonar.plugins.bw.check.activity.soap.request.HardCodedUrlCheck.class
		);
	}

}
