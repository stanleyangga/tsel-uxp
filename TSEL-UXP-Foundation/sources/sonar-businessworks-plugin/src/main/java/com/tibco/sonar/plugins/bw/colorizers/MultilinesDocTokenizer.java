package com.tibco.sonar.plugins.bw.colorizers;

import org.slf4j.Logger;
import org.sonar.channel.CodeReader;
import org.sonar.channel.EndMatcher;
import org.sonar.colorizer.HtmlCodeBuilder;

import java.util.Arrays;

public class MultilinesDocTokenizer extends AbstractTokenizer {

	private static final String COMMENT_STARTED_ON_PREVIOUS_LINE = "COMMENT_STARTED_ON_PREVIOUS_LINE";
	private static final String COMMENT_TOKENIZER = "MULTILINE_COMMENT_TOKENIZER";
	private final char[] startToken;
	private final char[] endToken;
	private final String tagBefore;
	private final String tagAfter;
	
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory
			.getLogger(MultilinesDocTokenizer.class.getName());

	/**
	 * @deprecated endToken is hardcoded to star-slash, whatever the startToken
	 *             !
	 */
	@Deprecated
	public MultilinesDocTokenizer(String startToken, String tagBefore,
			String tagAfter) {
		this(startToken, "*/", tagBefore, tagAfter);
	}

	public MultilinesDocTokenizer(String startToken, String endToken,
			String tagBefore, String tagAfter) {
		this.tagBefore = tagBefore;
		this.tagAfter = tagAfter;
		this.startToken = startToken.toCharArray();
		this.endToken = endToken.toCharArray();
	}

	public boolean hasNextToken(CodeReader code, HtmlCodeBuilder codeBuilder) {
		return code.peek() != '\n'
				&& code.peek() != '\r'
				&& (isCommentStartedOnPreviousLine(codeBuilder) || (code.peek() == startToken[0] && Arrays
						.equals(code.peek(startToken.length), startToken)));
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean consume(CodeReader code, HtmlCodeBuilder codeBuilder) {
		getLogger().debug("   ***** consume *****   ");
		Boolean result;
		open(code, codeBuilder);
		if (hasNextToken(code, codeBuilder)) {
			codeBuilder.appendWithoutTransforming(tagBefore);
			code.popTo(new MultilineEndTokenMatcher(code, codeBuilder),
					codeBuilder);
			codeBuilder.appendWithoutTransforming(tagAfter);
			result = true;
		}else{
			result = false;
		}
		close(code, codeBuilder);		
		return result;
	}

	private class MultilineEndTokenMatcher implements EndMatcher {

		private final CodeReader code;
		private final HtmlCodeBuilder codeBuilder;
		private int commentSize = 0;

		public MultilineEndTokenMatcher(CodeReader code,
				HtmlCodeBuilder codeBuilder) {
			this.code = code;
			this.codeBuilder = codeBuilder;
		}

		public boolean match(int endFlag) {
			commentSize++;
			if (commentSize >= endToken.length + startToken.length
					|| (commentSize >= endToken.length && isCommentStartedOnPreviousLine(codeBuilder))) {
				boolean matches = true;
				for (int i = 1; i <= endToken.length; i++) {
					if (code.charAt(-i) != endToken[endToken.length - i]) {
						matches = false;
						break;
					}
				}
				if (matches) {
					setCommentStartedOnPreviousLine(codeBuilder, Boolean.FALSE);
					return true;
				}
			}

			if (endFlag == '\r' || endFlag == '\n') {
				setCommentStartedOnPreviousLine(codeBuilder, Boolean.TRUE);
				return true;
			}
			return false;
		}
	}

	private boolean isCommentStartedOnPreviousLine(HtmlCodeBuilder codeBuilder) {
		Boolean b = (Boolean) codeBuilder.getVariable(
				COMMENT_STARTED_ON_PREVIOUS_LINE, Boolean.FALSE);
		return (b == Boolean.TRUE)
				&& (this.equals(codeBuilder.getVariable(COMMENT_TOKENIZER)));
	}

	private void setCommentStartedOnPreviousLine(HtmlCodeBuilder codeBuilder,
			Boolean b) {
		codeBuilder.setVariable(COMMENT_STARTED_ON_PREVIOUS_LINE, b);
		codeBuilder.setVariable(COMMENT_TOKENIZER, b ? this : null);
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

}
