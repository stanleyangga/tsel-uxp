package com.tibco.sonar.plugins.bw.resource;

public interface ProcessChildResource {
	
	public boolean isProcessLeaf();

}
