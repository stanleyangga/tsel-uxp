package com.tibco.sonar.plugins.bw.source;

/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.sonar.api.resources.Resource;
import com.tibco.sonar.plugins.bw.file.XmlFile;
import com.tibco.sonar.plugins.bw.resource.ProcessActivityResource;
import com.tibco.sonar.plugins.bw.resource.ProcessBranchResource;
import com.tibco.utils.bw.common.SaxParser;
import com.tibco.utils.bw.model.Process;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Checks and analyzes report measurements, issues and other findings in
 * WebSourceCode.
 * 
 * @author Matthijs Galesloot
 */
public class ProcessSource extends XmlSource {

	private Process process;

	public ProcessSource(File file){
		super(file);
		this.process = new Process();
	}
	
	public ProcessSource(XmlFile xmlFile) {
		super(xmlFile);
		this.process = new Process();	
	}
	
	public ProcessSource(String code) {
		super(code);
		setCode(code);
		this.process = new Process();
		InputStream is = createInputStream();
		process.setProcessXmlDocument(new SaxParser().parseDocument(is, true));		
	}

	@Override
	public boolean parseSource(Charset charset) {
		boolean result = super.parseSource(charset);
		if(result){
			process.setProcessXmlDocument(getDocument(true)).parse();
		}
		return result;
	}
	
	public Process getProcessModel(){
		return process;
	}

	public ProcessActivityResource create(ProcessBranchResource parent) {
		return create(parent, process.getName());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Resource create(Resource parent, String key) {
		if (parent instanceof ProcessBranchResource) {
			return create((ProcessBranchResource) parent, key);
		}
		return null;
	}
	
	public ProcessActivityResource create(ProcessBranchResource parent, String key) {
		return new ProcessActivityResource(parent, key);
	}


}
