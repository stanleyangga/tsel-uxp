package com.tibco.sonar.plugins.bw.resource;

import org.sonar.api.resources.Language;
import org.sonar.api.resources.Resource;

public interface ProcessBranch {

	public String getName();

	public String getLongName();

	public String getDescription();

	public Language getLanguage();

	public String getScope();

	public String getQualifier();

	@SuppressWarnings("rawtypes")
	public Resource getParent();

	public boolean matchFilePattern(String antPattern);
	
	public boolean isProcessRoot();
	
	public ProcessBranchResource asBranchResource();

}
