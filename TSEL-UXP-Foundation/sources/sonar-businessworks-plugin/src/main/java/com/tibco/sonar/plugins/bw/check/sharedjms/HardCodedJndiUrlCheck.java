
package com.tibco.sonar.plugins.bw.check.sharedjms;

import org.sonar.check.BelongsToProfile;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.tibco.sonar.plugins.bw.check.AbstractXmlCheck;
import com.tibco.sonar.plugins.bw.profile.CommonRulesSonarWayProfile;
import com.tibco.sonar.plugins.bw.source.XmlSource;
import com.tibco.sonar.plugins.bw.violation.DefaultViolation;
import com.tibco.sonar.plugins.bw.violation.Violation;
import com.tibco.utils.bw.helper.XmlHelper;

@Rule(key = HardCodedJndiUrlCheck.RULE_KEY, priority = Priority.MAJOR)
@BelongsToProfile(title = CommonRulesSonarWayProfile.SONAR_WAY_PROFILE_NAME, priority = Priority.MAJOR)
public class HardCodedJndiUrlCheck extends AbstractXmlCheck {

	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory
			.getLogger(HardCodedJndiUrlCheck.class.getName());
	
	public static final String RULE_KEY = "SharedJmsHardCodedJndiUrl";
	public static final String CONFIG_ELEMENT_NAME = "config";
	public static final String NAMING_SECTION_ELEMENT_NAME = "NamingEnvironment";
	public static final String JNDI_FLAG_ELEMENT_NAME = "UseJNDI";
	
	public static final String JNDI_URL_ELEMENT_NAME = "NamingURL";
	public static final String JNDI_URL_ELEMENT_DESC = "Shared JMS connection resource JNDI URL";
	

	@Override
	protected void validate(XmlSource xmlSource) {
		Document document = xmlSource.getDocument(false);
		try{
			Element config = XmlHelper.firstChildElement(document.getDocumentElement(), CONFIG_ELEMENT_NAME);			
			if(config.hasChildNodes()){
				Element namingEnvironment = XmlHelper.firstChildElement(config, NAMING_SECTION_ELEMENT_NAME);
				if(config.hasChildNodes()){
					Element useJNDI = XmlHelper.firstChildElement(namingEnvironment, JNDI_FLAG_ELEMENT_NAME);
					if(useJNDI.getTextContent() != null && "true".equals(useJNDI.getTextContent())){
						xmlSource.findAndValidateHardCodedChild(getRule(), namingEnvironment, JNDI_URL_ELEMENT_NAME, JNDI_URL_ELEMENT_DESC);
					}					
				}else{
					Violation violation = new DefaultViolation(getRule(),xmlSource.getLineForNode(namingEnvironment),"Shared JMS connection resource naming environment is empty");
					xmlSource.addViolation(violation);
				}				
			}else{
				Violation violation = new DefaultViolation(getRule(),xmlSource.getLineForNode(config),"Shared JMS connection resource configuration is empty");
				xmlSource.addViolation(violation);
			}
		}catch (Exception e) {
			LOGGER.info("context", e);
			Violation violation = new DefaultViolation(getRule(),1,"No configuration found in shared JMS connection resource");
			xmlSource.addViolation(violation);
		}
	}
}
