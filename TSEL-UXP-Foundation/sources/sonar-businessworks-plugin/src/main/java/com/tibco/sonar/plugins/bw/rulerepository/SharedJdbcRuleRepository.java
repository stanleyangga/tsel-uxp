/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw.rulerepository;

import org.sonar.api.rules.AnnotationRuleParser;
import org.sonar.api.rules.Rule;
import org.sonar.api.rules.RuleRepository;

import com.google.common.collect.ImmutableList;
import com.tibco.sonar.plugins.bw.check.sharedjdbc.*;
import com.tibco.sonar.plugins.bw.language.SharedJdbc;

import java.util.List;

/**
 * Replacement for org.sonar.plugins.squid.SquidRuleRepository
 */
public class SharedJdbcRuleRepository extends RuleRepository {

	public static final String REPOSITORY_KEY = "sharedjdbc";
	public static final String REPOSITORY_NAME = "SonarQube";

	private final AnnotationRuleParser annotationRuleParser;

	public SharedJdbcRuleRepository(AnnotationRuleParser annotationRuleParser) {
		super(REPOSITORY_KEY, SharedJdbc.KEY);
		setName(REPOSITORY_NAME);
		this.annotationRuleParser = annotationRuleParser;
	}

	@Override
	public List<Rule> createRules() {
		return annotationRuleParser.parse(REPOSITORY_KEY, getChecks());
	}

	@SuppressWarnings("rawtypes")
	public static List<Class> getChecks() {
		return ImmutableList.<Class> of(
				HardCodedUrlCheck.class,
				HardCodedPasswordCheck.class,
				HardCodedUserCheck.class);
	}

}
