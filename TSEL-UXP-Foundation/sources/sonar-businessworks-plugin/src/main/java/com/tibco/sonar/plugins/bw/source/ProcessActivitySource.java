package com.tibco.sonar.plugins.bw.source;

/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.sonar.api.resources.Resource;

import com.tibco.sonar.plugins.bw.resource.ProcessActivityResource;
import com.tibco.sonar.plugins.bw.resource.ProcessBranchResource;
import com.tibco.utils.bw.model.Activity;

import java.nio.charset.Charset;

/**
 * Checks and analyzes report measurements, issues and other findings in
 * WebSourceCode.
 * 
 * @author Matthijs Galesloot
 */
public class ProcessActivitySource extends AbstractSource {

	private Activity activity;

	public ProcessActivitySource(Activity activity) {
		super();
		this.activity = activity;
	}

	@Override
	public boolean parseSource(Charset charset) {
		activity.parse();
		return true;
	}

	public ProcessActivityResource create(ProcessBranchResource parent) {
		return create(parent, activity.getName());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Resource create(Resource parent, String key) {
		if (parent instanceof ProcessBranchResource) {
			return create((ProcessBranchResource) parent, key);
		}
		return null;
	}
	
	public ProcessActivityResource create(ProcessBranchResource parent, String key) {
		return new ProcessActivityResource(parent, key);
	}

}
