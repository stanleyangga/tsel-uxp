package com.tibco.sonar.plugins.bw.colorizers;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;

import org.sonar.channel.CodeReader;
import org.sonar.colorizer.HtmlCodeBuilder;
import org.sonar.colorizer.Tokenizer;

import com.tibco.utils.bw.model.Process;
import com.tibco.webdesigner.html.Renderer;

public abstract class AbstractTokenizer extends Tokenizer {

	private static final String DOCUMENT_STARTED = "DOCUMENT_STARTED";
	private static final String DOCUMENT_ENDED = "DOCUMENT_ENDED";
	private static final String PROCESS = "PROCESS";
	
	protected abstract org.slf4j.Logger getLogger();
	
	public void open(CodeReader code, HtmlCodeBuilder codeBuilder){
		getLogger().debug("Open "+(char)code.peek()+" (length:"+code.length()+")");
		if (!isDocumentStarted(codeBuilder)) {
			getLogger().debug("   ***** STARTED *****   ");
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			int cursor = 0;
			int length = code.length();
			char character = code.charAt(cursor);
			getLogger().debug("Starting write in output Stream : length " + length);
			while (cursor < length && character != -1) {
				os.write(character);
				cursor++;
				character = code.charAt(cursor);
			}
			Process process = new Process();
			getLogger().debug("Full content");
			getLogger().debug(new String(os.toByteArray()));
			process.setProcessXmlDocument(new String(os.toByteArray())).parse();
			codeBuilder.setVariable(PROCESS, process);
			setDocumentStarted(codeBuilder, true);
		}
	}
	
	public void close(CodeReader code, HtmlCodeBuilder codeBuilder){
		getLogger().debug("Close "+(char)code.peek()+" (length:"+code.length()+")");
		if (code.length() <= 1 && !isDocumentEnded(codeBuilder)){
			try {
				getLogger().debug("   ***** ENDED *****   ");
				Renderer renderer = new Renderer();
				SimpleEntry<List<Exception>, String> jsProcessModel = renderer.getJavaScriptModel((Process)codeBuilder.getVariable(PROCESS));
				for(Exception e:jsProcessModel.getKey()){
					getLogger().error("Exception raised: "+e.getMessage());
				}
				codeBuilder.appendWithoutTransforming("\n");
				codeBuilder.appendWithoutTransforming(renderer.divSvgProcess(jsProcessModel.getValue()).replaceAll("[\n\r]", ""));
			} catch (Exception e) {
				getLogger().error("Error during HTML Rendering of Process");
				getLogger().error("context", e);
				
			}
			setDocumentEnded(codeBuilder, true);
		}
	}
	

	protected boolean isDocumentStarted(HtmlCodeBuilder codeBuilder) {
		return (Boolean) codeBuilder.getVariable(DOCUMENT_STARTED,
				Boolean.FALSE);
	}

	protected void setDocumentStarted(HtmlCodeBuilder codeBuilder, Boolean b) {
		codeBuilder.setVariable(DOCUMENT_STARTED, b);
	}
	
	protected boolean isDocumentEnded(HtmlCodeBuilder codeBuilder) {
		return (Boolean) codeBuilder.getVariable(DOCUMENT_ENDED,
				Boolean.FALSE);
	}
	
	protected void setDocumentEnded(HtmlCodeBuilder codeBuilder, Boolean b) {
		codeBuilder.setVariable(DOCUMENT_ENDED, b);
	}
	
}
