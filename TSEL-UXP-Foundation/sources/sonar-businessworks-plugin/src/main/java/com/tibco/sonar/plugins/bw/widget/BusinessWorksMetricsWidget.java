package com.tibco.sonar.plugins.bw.widget;

import org.sonar.api.web.*;

@UserRole(UserRole.USER)
@WidgetCategory({"BusinessWorks"})
@WidgetScope("GLOBAL")
public class BusinessWorksMetricsWidget extends AbstractRubyTemplate implements
		RubyRailsWidget {

	public String getId() {
		return "BusinessWork_Size";
	}

	public String getTitle() {
		return "BusinessWorks Size";
	}

	@Override
	protected String getTemplatePath() {
		return "/org/sonar/plugins/tibcobw/widget/BusinessWorksMetrics.html.erb";
	}

}
