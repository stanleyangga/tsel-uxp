package com.tibco.sonar.plugins.bw.colorizers;

import org.slf4j.Logger;
import org.sonar.channel.CodeReader;
import org.sonar.colorizer.HtmlCodeBuilder;

import java.util.Arrays;
import java.util.regex.Pattern;

public class CDataDocTokenizer extends AbstractTokenizer {

	private static final String CDATA_START = "<![CDATA[";
	private static final String CDATA_END = "]]>";

	private static final String CDATA_STARTED = "CDATA_STARTED";
	private static final String CDATA_TOKENIZER = "MULTILINE_CDATA_TOKENIZER";
	private final char[] startToken;
	private final char[] endToken;
	private final String tagBefore;
	private final String tagAfter;
	
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory
			.getLogger(CDataDocTokenizer.class.getName());

	public CDataDocTokenizer(String tagBefore, String tagAfter) {
		this.tagBefore = tagBefore;
		this.tagAfter = tagAfter;
		this.startToken = CDATA_START.toCharArray();
		this.endToken = CDATA_END.toCharArray();
	}

	public boolean isCDATABloc(CodeReader code, HtmlCodeBuilder codeBuilder) {
		return isCDATAStarted(codeBuilder)
				|| (code.peek() != '\n' && code.peek() != '\r' && (code.peek() == startToken[0] && Arrays
						.equals(code.peek(startToken.length), startToken)));
	}

	@Override
	public boolean consume(CodeReader code, HtmlCodeBuilder codeBuilder) {
		getLogger().debug("   ***** consume *****   ");
		Boolean result;
		open(code, codeBuilder);
		if (!isCDATAStarted(codeBuilder) && code.peek() == startToken[0]
				&& Arrays.equals(code.peek(startToken.length), startToken)) {
			codeBuilder.appendWithoutTransforming(tagBefore);
			// Consume CDATA start
			code.popTo(Pattern.compile(Pattern.quote(CDATA_START)).matcher(""),
					codeBuilder);
			codeBuilder.appendWithoutTransforming(tagAfter);
			setCDATAStarted(codeBuilder, true);
			result = true;
		}else{
			if (isCDATAStarted(codeBuilder)) {
				if (code.peek() == endToken[0]
						&& Arrays.equals(code.peek(endToken.length), endToken)) {
					codeBuilder.appendWithoutTransforming(tagBefore);
					// Consume CDATA end
					code.popTo(Pattern.compile(Pattern.quote(CDATA_END))
							.matcher(""), codeBuilder);
					codeBuilder.appendWithoutTransforming(tagAfter);
					setCDATAStarted(codeBuilder, false);
				} else {
					// Consume everything between CDATA
					code.pop(codeBuilder);
				}
				result = true;
			}else{
				result = false;
			}
		}
		close(code, codeBuilder);
		return result;
	}

	private boolean isCDATAStarted(HtmlCodeBuilder codeBuilder) {
		Boolean b = (Boolean) codeBuilder.getVariable(CDATA_STARTED,
				Boolean.FALSE);
		return (b == Boolean.TRUE)
				&& (this.equals(codeBuilder.getVariable(CDATA_TOKENIZER)));
	}

	private void setCDATAStarted(HtmlCodeBuilder codeBuilder, Boolean b) {
		codeBuilder.setVariable(CDATA_STARTED, b);
		codeBuilder.setVariable(CDATA_TOKENIZER, b ? this : null);
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

}
