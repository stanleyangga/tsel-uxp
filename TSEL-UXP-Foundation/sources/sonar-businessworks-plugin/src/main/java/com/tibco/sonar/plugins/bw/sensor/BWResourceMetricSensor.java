/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw.sensor;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import com.tibco.sonar.plugins.bw.language.AbstractBusinessWorksLanguage;
import com.tibco.sonar.plugins.bw.language.SharedHttp;
import com.tibco.sonar.plugins.bw.language.SharedJdbc;
import com.tibco.sonar.plugins.bw.language.SharedJms;
import com.tibco.sonar.plugins.bw.metric.BusinessWorksMetrics;

import org.sonar.api.batch.SensorContext;
import org.sonar.api.component.ResourcePerspectives;
import org.sonar.api.measures.Metric;
import org.sonar.api.resources.File;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.Resource;
import org.sonar.api.scan.filesystem.FileQuery;
import org.sonar.api.scan.filesystem.ModuleFileSystem;

public class BWResourceMetricSensor extends AbstractMetricSensor {

	private List<SimpleEntry<String,Metric>> resourceLanguageKeys = new ArrayList<SimpleEntry<String,Metric>>();
	
	@SuppressWarnings("rawtypes")
	private Resource bwResourceFile;
	private int isBWproject = 1;
	
	public BWResourceMetricSensor(
			ModuleFileSystem fileSystem,
			ResourcePerspectives resourcePerspectives) {
		super(fileSystem, resourcePerspectives, AbstractBusinessWorksLanguage.BW_KEY);
		resourceLanguageKeys.add(new SimpleEntry<String, Metric>(SharedHttp.KEY,BusinessWorksMetrics.BWRESOURCES_HTTP_CONNECTION));
		resourceLanguageKeys.add(new SimpleEntry<String, Metric>(SharedJms.KEY,BusinessWorksMetrics.BWRESOURCES_JMS_CONNECTION));
		resourceLanguageKeys.add(new SimpleEntry<String, Metric>(SharedJdbc.KEY,BusinessWorksMetrics.BWRESOURCES_JDBC_CONNECTION));
	}

	@Override
	public void analyse(Project project, SensorContext sensorContext) {
		this.project = project;
		this.sensorContext = sensorContext;
		for(SimpleEntry<String,Metric> entry:resourceLanguageKeys){
			for (java.io.File file : fileSystem.files(FileQuery.onSource()
					.onLanguage(entry.getKey()))) {
				bwResourceFile = sensorContext.getResource(File.fromIOFile(file, project));
				sensorContext.saveMeasure(bwResourceFile, entry.getValue(), (double) 1);
			}
		}
		if(sensorContext.getMeasure(BusinessWorksMetrics.BWLANGUAGEFLAG) == null){
			sensorContext.saveMeasure(BusinessWorksMetrics.BWLANGUAGEFLAG, (double) isBWproject);
		}
	}

	@Override
	protected void analyseFile(java.io.File file) {
		// Empty
	}
	
	public boolean shouldExecuteOnProject(Project project) {
		for(SimpleEntry<String,Metric> entry:resourceLanguageKeys){
			if(!fileSystem.files(FileQuery.onSource().onLanguage(entry.getKey()))
					.isEmpty()){
				return true;
			}
		}
		return false;
		
	}
}
