/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.sensor;

import com.tibco.sonar.plugins.bw.language.SharedJms;
import com.tibco.sonar.plugins.bw.rulerepository.SharedJmsRuleRepository;

import org.sonar.api.component.ResourcePerspectives;

import org.sonar.api.profiles.RulesProfile;
import org.sonar.api.resources.Project;
import org.sonar.api.scan.filesystem.FileQuery;
import org.sonar.api.scan.filesystem.ModuleFileSystem;

/**
 * XmlSensor provides analysis of xml files.
 * 
 * @author Matthijs Galesloot
 */
public class SharedJmsRuleSensor extends AbstractRuleSensor {

	public SharedJmsRuleSensor(RulesProfile profile, ModuleFileSystem fileSystem,
			ResourcePerspectives resourcePerspectives) {
		super(profile, fileSystem, resourcePerspectives,
				SharedJmsRuleRepository.REPOSITORY_KEY, SharedJmsRuleRepository
						.getChecks(), SharedJms.KEY);
	}

	/**
	 * This sensor only executes on projects with active XML rules.
	 */
	public boolean shouldExecuteOnProject(Project project) {
		return !fileSystem.files(FileQuery.onSource().onLanguage(SharedJms.KEY))
				.isEmpty();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
