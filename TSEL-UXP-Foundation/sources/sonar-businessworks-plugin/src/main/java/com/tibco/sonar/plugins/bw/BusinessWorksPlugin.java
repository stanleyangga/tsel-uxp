/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw;

import com.google.common.collect.ImmutableList;

import org.sonar.api.SonarPlugin;

import java.util.List;

public class BusinessWorksPlugin extends SonarPlugin {

	public static final String TIBCO_BUSINESSWORK_CATEGORY = "TIBCO BusinessWorks";
	public static final String TIBCO_BUSINESSWORK_RULEREPOSITORY = "TIBCO BusinessWorks";

	public static final String XML_FILE_SUFFIXES_KEY = "sonar.bw.xml.file.suffixes";
	public static final String GV_FILE_SUFFIXES_KEY = "sonar.bw.gv.file.suffixes";
	public static final String PROCESS_FILE_SUFFIXES_KEY = "sonar.bw.process.file.suffixes";
	public static final String SERVICEAGENT_FILE_SUFFIXES_KEY = "sonar.bw.serviceagent.file.suffixes";
	public static final String SHAREDHTTP_FILE_SUFFIXES_KEY = "sonar.bw.sharedhttp.file.suffixes";
	public static final String SHAREDJDBC_FILE_SUFFIXES_KEY = "sonar.bw.sharedjdbc.file.suffixes";
	public static final String SHAREDJMS_FILE_SUFFIXES_KEY = "sonar.bw.sharedjms.file.suffixes";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List getExtensions() {
		ImmutableList.Builder<Object> builder = ImmutableList.builder();
		builder.addAll(CommonExtensions.getExtensions());
		builder.addAll(BwResourcesExtensions.getExtensions());
		builder.addAll(GlobalVariableExtensions.getExtensions());
		builder.addAll(ProcessExtensions.getExtensions());
		builder.addAll(SharedHttpExtensions.getExtensions());
		builder.addAll(SharedJdbcExtensions.getExtensions());
		builder.addAll(SharedJmsExtensions.getExtensions());
		return builder.build();
	}

}
