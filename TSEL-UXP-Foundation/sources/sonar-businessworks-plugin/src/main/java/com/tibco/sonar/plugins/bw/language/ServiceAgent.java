/*
 * SonarQube BusinessWorks Plugin
 * Copyright (C) 2014 TIBCO Software
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * 
 */
package com.tibco.sonar.plugins.bw.language;

import com.tibco.sonar.plugins.bw.BusinessWorksPlugin;

import org.sonar.api.config.Settings;

/**
 * This class defines the BusinessWorks Service Agent language.
 * 
 * @author Gilles Seghaier
 */
public class ServiceAgent extends AbstractBusinessWorksLanguage {

	/** The process language key. */
	public static final String KEY = "serviceagent";

	/** The process language name */
	public static final String LANGUAGE_NAME = "BusinessWorks Service Agent";

	/**
	 * Key of the file suffix parameter
	 */
	public static final String FILE_SUFFIXES_KEY = BusinessWorksPlugin.SERVICEAGENT_FILE_SUFFIXES_KEY;

	/**
	 * All the valid process files suffixes.
	 */
	public static final String[] DEFAULT_FILE_SUFFIXES = { ".serviceagent" };

	
	public ServiceAgent(Settings settings) {		
		super(settings,KEY,LANGUAGE_NAME);
	}
	
	public String[] getFileSuffixes() {
		return getFileSuffixes(FILE_SUFFIXES_KEY, DEFAULT_FILE_SUFFIXES);
	}
}
