package com.tibco.sonar.plugins.bw.check.activity;

import org.sonar.check.Cardinality;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.check.RuleProperty;

import com.tibco.sonar.plugins.bw.check.AbstractProcessHardCodedCheck;

@Rule(key = CustomHardCodedCheck.RULE_KEY, priority = Priority.MAJOR, cardinality = Cardinality.MULTIPLE)
public class CustomHardCodedCheck extends AbstractProcessHardCodedCheck {

	public static final String RULE_KEY = "CustomHardCodedActivity";
	
	@RuleProperty(key = "configXPath", type = "TEXT")
	private String configXPath;
	
	@RuleProperty(key = "inputBindingXPath", type = "TEXT")
	private String inputBindingXPath;

	@RuleProperty(key = "activityType")
	private String activityType;

	@RuleProperty(key = "message")
	private String message;
	
	public String getConfigXPath() {
		return configXPath;
	}

	public void setConfigXPath(String configXPath) {
		this.configXPath = configXPath;
	}

	public String getInputBindingXPath() {
		return inputBindingXPath;
	}

	public void setInputBindingXPath(String inputBindingXPath) {
		this.inputBindingXPath = inputBindingXPath;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
