/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.sensor;

import com.tibco.sonar.plugins.bw.check.AbstractCheck;
import com.tibco.sonar.plugins.bw.language.Process;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;

import org.sonar.api.component.ResourcePerspectives;

import org.sonar.api.profiles.RulesProfile;
import org.sonar.api.resources.File;
import org.sonar.api.resources.Project;
import org.sonar.api.scan.filesystem.FileQuery;
import org.sonar.api.scan.filesystem.ModuleFileSystem;

/**
 * XmlSensor provides analysis of xml files.
 * 
 * @author Matthijs Galesloot
 */
public class ProcessRuleSensor extends AbstractRuleSensor {

	public ProcessRuleSensor(RulesProfile profile, ModuleFileSystem fileSystem,
			ResourcePerspectives resourcePerspectives) {
		super(profile, fileSystem, resourcePerspectives,
				ProcessRuleRepository.REPOSITORY_KEY, ProcessRuleRepository
						.getChecks(), Process.KEY);
	}
	
	@SuppressWarnings("unchecked")
	protected void analyseFile(java.io.File file) {
		File resource = File.fromIOFile(file, project);
		ProcessSource sourceCode = new ProcessSource(file);
		// Do not execute any XML rule when an XML file is corrupted
		// (SONARXML-13)
		if (sourceCode.parseSource(fileSystem.sourceCharset())) {
			for (AbstractCheck check : checks) {
				check.setRule(annotationCheckFactory.getActiveRule(check)
						.getRule());
				sourceCode = check.validate(sourceCode);
			}
			saveIssues(sourceCode, resource);
		}
	}

	/**
	 * This sensor only executes on projects with active XML rules.
	 */
	public boolean shouldExecuteOnProject(Project project) {
		return !fileSystem.files(FileQuery.onSource().onLanguage(Process.KEY))
				.isEmpty();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
