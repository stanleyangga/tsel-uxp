/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw;

import com.google.common.collect.ImmutableList;
import com.tibco.sonar.plugins.bw.language.SharedHttp;
import com.tibco.sonar.plugins.bw.rulerepository.SharedHttpRuleRepository;
import com.tibco.sonar.plugins.bw.profile.SharedHttpSonarWayProfile;
import com.tibco.sonar.plugins.bw.sensor.SharedHttpRuleSensor;

import org.apache.commons.lang.StringUtils;
import org.sonar.api.config.PropertyDefinition;
import org.sonar.api.resources.Qualifiers;

import java.util.List;

public final class SharedHttpExtensions {

	public static final String SUB_CATEGORY_NAME = "Shared HTTP resources";

	private SharedHttpExtensions() {
	}

	@SuppressWarnings("rawtypes")
	public static List getExtensions() {
		ImmutableList.Builder<Object> builder = ImmutableList.builder();
		builder.add(SharedHttp.class);
		builder.add(SharedHttpSonarWayProfile.class);	
		builder.add(SharedHttpRuleRepository.class);
		builder.add(SharedHttpRuleSensor.class);
		builder.add(PropertyDefinition
				.builder(SharedHttp.FILE_SUFFIXES_KEY)
				.defaultValue(StringUtils.join(SharedHttp.DEFAULT_FILE_SUFFIXES,","))
				.name("Shared HTTP resource file suffixes")
				.description(
						"Comma-separated list of suffixes for files to analyze.")
				.category(BusinessWorksPlugin.TIBCO_BUSINESSWORK_CATEGORY)
				.subCategory(SUB_CATEGORY_NAME)
				.onQualifiers(Qualifiers.PROJECT).build());
		return builder.build();
	}

}
