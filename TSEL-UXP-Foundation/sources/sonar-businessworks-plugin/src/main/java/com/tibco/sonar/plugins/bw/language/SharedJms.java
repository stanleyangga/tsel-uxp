/*
 * SonarQube BusinessWorks Plugin
 * Copyright (C) 2014 TIBCO Software
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * 
 */
package com.tibco.sonar.plugins.bw.language;

import org.sonar.api.config.Settings;

import com.tibco.sonar.plugins.bw.BusinessWorksPlugin;

/**
 * This class defines the BusinessWorks Shared JMS Connection language.
 * 
 * @author Gilles Seghaier
 */
public class SharedJms extends AbstractBusinessWorksLanguage {

	public static final SharedJms INSTANCE = new SharedJms();
	
	/** All the valid process files suffixes. */
	public static final String[] DEFAULT_FILE_SUFFIXES = { ".sharedjmscon" };

	/** The process language key. */
	public static final String KEY = "sharedjms";

	/** The process language name */
	public static final String LANGUAGE_NAME = "BusinessWorks Shared JMS Resources";

	/**
	 * Key of the file suffix parameter
	 */
	public static final String FILE_SUFFIXES_KEY = BusinessWorksPlugin.SHAREDJMS_FILE_SUFFIXES_KEY;
	
	public SharedJms() {		
		super(KEY,LANGUAGE_NAME);
	}
	
	public SharedJms(Settings settings) {		
		super(settings,KEY,LANGUAGE_NAME);
	}	
	
	public String[] getFileSuffixes() {
		return getFileSuffixes(FILE_SUFFIXES_KEY, DEFAULT_FILE_SUFFIXES);
	}
}
