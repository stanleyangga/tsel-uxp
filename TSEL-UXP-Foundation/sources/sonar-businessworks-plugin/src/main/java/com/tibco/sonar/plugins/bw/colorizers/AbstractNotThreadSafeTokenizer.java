package com.tibco.sonar.plugins.bw.colorizers;

import org.slf4j.Logger;
import org.sonar.channel.CodeReader;
import org.sonar.colorizer.HtmlCodeBuilder;
import org.sonar.colorizer.NotThreadSafeTokenizer;


public abstract class AbstractNotThreadSafeTokenizer extends NotThreadSafeTokenizer {

	private AbstractTokenizer tokenizer;
	
	protected abstract org.slf4j.Logger getLogger();
	protected AbstractNotThreadSafeTokenizer reflect;
	
	public AbstractNotThreadSafeTokenizer(){
		super();
		reflect = this;
		tokenizer = new AbstractTokenizer() {			
			@Override
			public boolean consume(CodeReader code, HtmlCodeBuilder output) {				
				return reflect.consume(code, output);				
			}		
			@Override
			protected Logger getLogger() {
				return reflect.getLogger();
			}
		};
	}
	
	public void open(CodeReader code, HtmlCodeBuilder codeBuilder){
		tokenizer.open(code, codeBuilder);
	}
	
	public void close(CodeReader code, HtmlCodeBuilder codeBuilder){
		tokenizer.close(code, codeBuilder);
	}	

	protected boolean isDocumentStarted(HtmlCodeBuilder codeBuilder) {
		return tokenizer.isDocumentStarted(codeBuilder);
	}

	protected void setDocumentStarted(HtmlCodeBuilder codeBuilder, Boolean b) {
		tokenizer.setDocumentStarted(codeBuilder, b);
	}
	
	protected boolean isDocumentEnded(HtmlCodeBuilder codeBuilder) {
		return tokenizer.isDocumentEnded(codeBuilder);
	}
	
	protected void setDocumentEnded(HtmlCodeBuilder codeBuilder, Boolean b) {
		tokenizer.setDocumentEnded(codeBuilder, b);
	}
}
