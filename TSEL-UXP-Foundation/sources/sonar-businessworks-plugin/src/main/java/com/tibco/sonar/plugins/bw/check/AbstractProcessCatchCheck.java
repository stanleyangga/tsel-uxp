/*
 * SonarQube Java
 * Copyright (C) 2012 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.tibco.sonar.plugins.bw.check;

import java.util.List;
import java.util.NoSuchElementException;

import org.w3c.dom.Element;

import com.tibco.sonar.plugins.bw.check.AbstractProcessCheck;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.violation.DefaultViolation;
import com.tibco.sonar.plugins.bw.violation.Violation;
import com.tibco.utils.bw.helper.XmlHelper;
import com.tibco.utils.bw.model.Activity;
import com.tibco.utils.bw.model.Process;

public abstract class AbstractProcessCatchCheck extends AbstractProcessCheck {
	
	public abstract String getCatchFaultElementValue();
	public abstract void setCatchFaultElementValue(String catchFaultElementValue);

	public abstract String getCatchFaultElementName();
	public abstract void setCatchFaultElementName(String catchFaultElementName);

	public abstract String getCatchAllElementValue();
	public abstract void setCatchAllElementValue(String catchAllElementValue);

	public abstract String getCatchAllElementName();
	public abstract void setCatchAllElementName(String catchAllElementName);

	public abstract String getCatchActivityType();
	public abstract void setCatchActivityType(String catchActivityType);

	public abstract String getNotFoundMessage();
	public abstract void setNotFoundMessage(String notFoundMessage);

	public abstract String getNoCatchMessage();
	public abstract void setNoCatchMessage(String noCatchMessage);
	
	@Override
	protected void validate(ProcessSource processSource) {
		// Get process
		Process process = processSource.getProcessModel();
		// Get all catch activities
		List<Activity> activitiesCatch = process
				.getActivitiesByType(getCatchActivityType());
		// If no catch activity found raise a violation
		if (activitiesCatch.size() < 1) {
			Violation violation = new DefaultViolation(getRule(), 1,
					getNoCatchMessage());
			processSource.addViolation(violation);
		// Else parse catch activities
		} else {
			boolean catchFound = false;
			boolean catchAllFound = false;
			// For each catch activity found
			for (Activity activity : activitiesCatch) {
				// try to retrieve catchAll element in configuration
				try{
					Element catchAllConfigElement = XmlHelper.firstChildElement(
							(Element) activity.getConfiguration(),getCatchAllElementName());
					// if catchAll found and value equal to activation value
					if (catchAllConfigElement.getTextContent().equals(getCatchAllElementValue())) {
						// then catch all found
						catchAllFound = true;
					}
				}catch(NoSuchElementException e){
					// else catch all not found
					catchAllFound = false;					
				}
				// if catch all found
				if(catchAllFound){
					// and no specifi fault element defined
					if(getCatchFaultElementValue() == null || getCatchFaultElementValue().isEmpty()){				
						// Then we found requiere catch (eg. catchAll)
						catchFound = catchAllFound;
						break;
					}
				// but if catch all not found
				}else{
					// if the catch activity is configured to catch a specific fault 
					if(getCatchFaultElementValue() != null && !getCatchFaultElementValue().isEmpty()){	
						// check if this specific fault is the rule's one 
						catchFound = findFaultCatch(activity);
						if(catchFound){
							// stop the loop if found
							break;
						}
					}					
				}
			}
			// if rule's catch not found raise a violation
			if (!catchFound) {
				Violation violation = new DefaultViolation(getRule(), 1,
						getNotFoundMessage());
				processSource.addViolation(violation);
			}
		}
	}
	
	private boolean findFaultCatch(Activity activity){
		// init catch not found
		boolean catchFound = false;
		// if specific fault element (based on name and value) is searched
		if(!getCatchFaultElementName().isEmpty() && !getCatchFaultElementValue().isEmpty()){
			// get fault element 
			Element catchFaultConfigElement = XmlHelper.firstChildElement(
					(Element) activity.getConfiguration(),getCatchFaultElementName());
			// if fault value is equal to what we are looking for
			if (getCatchFaultElementValue().equals(catchFaultConfigElement.getTextContent())) {
				// set catch found
				catchFound = true;
			}
		}
		return catchFound;
	}

	/** 
	 * Validate only process code source
	 * 
	 * @see com.tibco.sonar.plugins.bw.check.AbstractProcessCheck#validate(com.tibco.sonar.plugins.bw.source.Source)
	 */
	@Override
	public <S extends Source> S validate(S source) {
		if (source instanceof ProcessSource) {
			validate((ProcessSource) source);
		}
		return source;
	}
	
}
