/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.sensor;

import com.google.common.annotations.VisibleForTesting;
import com.tibco.sonar.plugins.bw.check.AbstractCheck;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.source.XmlSource;
import com.tibco.sonar.plugins.bw.violation.Violation;

import org.sonar.api.batch.SensorContext;
import org.sonar.api.checks.AnnotationCheckFactory;
import org.sonar.api.component.ResourcePerspectives;
import org.sonar.api.issue.Issuable;
import org.sonar.api.profiles.RulesProfile;
import org.sonar.api.resources.File;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.Resource;
import org.sonar.api.scan.filesystem.ModuleFileSystem;

import java.util.Collection;
import java.util.List;

/**
 * XmlSensor provides analysis of xml files.
 * 
 * @author Matthijs Galesloot
 */
public abstract class AbstractRuleSensor extends AbstractSensor {

	protected AnnotationCheckFactory annotationCheckFactory;
	protected Collection<AbstractCheck> checks;

	@SuppressWarnings("rawtypes")
	protected AbstractRuleSensor(RulesProfile profile,
			ModuleFileSystem fileSystem,
			ResourcePerspectives resourcePerspectives, String repositoryKey,
			List<Class> list, String languageKey) {
		super(fileSystem, resourcePerspectives, languageKey);
		this.annotationCheckFactory = AnnotationCheckFactory.create(profile,
				repositoryKey, list);
	}

	/**
	 * Analyze the XML files.
	 */
	@SuppressWarnings("unchecked")
	public void analyse(Project project, SensorContext sensorContext) {
		this.checks = annotationCheckFactory.getChecks();
		this.project = project;
		super.analyse(project, sensorContext);
	}

	@SuppressWarnings("unchecked")
	protected void analyseFile(java.io.File file) {
		File resource = File.fromIOFile(file, project);
		Source sourceCode = new XmlSource(file);
		// Do not execute any XML rule when an XML file is corrupted
		// (SONARXML-13)
		if (sourceCode.parseSource(fileSystem.sourceCharset())) {
			for (AbstractCheck check : checks) {
				check.setRule(annotationCheckFactory.getActiveRule(check)
						.getRule());
				sourceCode = check.validate(sourceCode);
			}
			saveIssues(sourceCode, resource);
		}
	}

	@SuppressWarnings("rawtypes")
	@VisibleForTesting
	protected void saveIssues(Source source, Resource resource) {
		for (Violation issue : source.getViolations()) {
			Issuable issuable = resourcePerspectives.as(Issuable.class,
					resource);
			issuable.addIssue(issuable.newIssueBuilder()
					.ruleKey(issue.getRule().ruleKey()).line(issue.getLine())
					.message(issue.getMessage()).build());
		}
	}
}
