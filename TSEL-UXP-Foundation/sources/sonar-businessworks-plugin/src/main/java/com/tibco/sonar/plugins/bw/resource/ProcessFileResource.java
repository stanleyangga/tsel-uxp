package com.tibco.sonar.plugins.bw.resource;

import java.util.List;

import org.sonar.api.resources.File;
import org.sonar.api.resources.Language;
import org.sonar.api.resources.Project;
import org.sonar.api.scan.filesystem.PathResolver;

import com.tibco.sonar.plugins.bw.language.Process;

public class ProcessFileResource extends File implements ProcessBranch{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9163056419611806888L;

	public ProcessFileResource(String key) {
		super(key);
		setLanguage(Process.INSTANCE);
	}

	public ProcessFileResource(String directory, String filename) {
		super(directory, filename);
		setLanguage(Process.INSTANCE);
	}
	
	public ProcessFileResource(java.io.File file) {
		super(file.getAbsolutePath());
		setLanguage(Process.INSTANCE);
	}

	public Language getLanguage() {
		return Process.INSTANCE;
	}
	
	public ProcessBranchResource asBranchResource(){
		return new ProcessBranchResource(this);
	}

	public boolean isProcessRoot() {
		return true;
	}

	/**
	 * Creates a File from its name and a project
	 */
	@SuppressWarnings("deprecation")
	public static ProcessFileResource fromIOFile(java.io.File file,
			Project project) {
		return fromIOFile(file, project.getFileSystem().getSourceDirs());
	}

	/**
	 * Creates a File from an io.file and a list of sources directories
	 */
	public static ProcessFileResource fromIOFile(java.io.File file,
			List<java.io.File> sourceDirs) {
		PathResolver.RelativePath relativePath = new PathResolver()
				.relativePath(sourceDirs, file);
		if (relativePath != null) {
			return new ProcessFileResource(relativePath.path());
		}
		return null;
	}


}
