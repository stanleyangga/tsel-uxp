package com.tibco.sonar.plugins.bw.resource;

import org.sonar.api.resources.Language;
import org.sonar.api.resources.Qualifiers;
import org.sonar.api.resources.Resource;
import org.sonar.api.resources.Scopes;
import org.sonar.api.utils.WildcardPattern;

import com.tibco.sonar.plugins.bw.language.Process;

public class ProcessGroupResource extends Resource<ProcessBranchResource> implements ProcessChildResource, ProcessBranch{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5645390152234821914L;

	public static final String SCOPE = Scopes.PROGRAM_UNIT;

	private ProcessBranchResource parent = null;
	private String qualifier = Qualifiers.CLASS;
	private boolean hasChildren = false;

	public ProcessGroupResource(ProcessBranchResource parent, String groupName) {
		this.parent = parent;
		setKey(groupName);
	}

	@Override
	public String getName() {
		return getKey();
	}

	@Override
	public String getLongName() {
		return parent.getLongName() + "//" + getKey();
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public Language getLanguage() {
		return Process.INSTANCE;
	}

	@Override
	public String getScope() {
		return SCOPE;
	}

	@Override
	public String getQualifier() {
		return qualifier;
	}

	@Override
	public ProcessBranchResource getParent() {
		return parent;
	}

	@Override
	public boolean matchFilePattern(String antPattern) {
		WildcardPattern matcher = WildcardPattern.create(antPattern, "/");
		return matcher.match(getKey());
	}
	
	public void foundChildren(boolean hasChildren){
		this.hasChildren = hasChildren;
	}

	public boolean isProcessLeaf() {
		return !hasChildren;
	}

	public boolean isProcessRoot() {
		return false;
	}
	
	public ProcessBranchResource asBranchResource(){
		return new ProcessBranchResource(this);
	}

}
