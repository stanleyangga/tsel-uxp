package com.tibco.sonar.plugins.bw.check.activity.soap.request;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

public class HardCodedSoapActionCheckTest extends AbstractCheckTester {

	@Test
	public void SoapActionConfigAndMappingHardCoded()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/SoapActionConfigAndMappingHardCoded.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedSoapActionCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void SoapActionConfigAndMappingOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/SoapActionConfigAndMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedSoapActionCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void SoapActionConfigHardCodedWithMappingOK() throws FileNotFoundException{
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/SoapActionConfigHardCodedWithMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedSoapActionCheck.class);
	
		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void SoapActionConfigHardCodedWithoutMapping()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/SoapActionConfigHardCodedWithoutMapping.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedSoapActionCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(25, sourceCode.getViolations().get(0).getLine());
	}

	@Test
	public void SoapActionMappingHardCodedConfigOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/SoapActionMappingHardCodedConfigOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedSoapActionCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}


}
