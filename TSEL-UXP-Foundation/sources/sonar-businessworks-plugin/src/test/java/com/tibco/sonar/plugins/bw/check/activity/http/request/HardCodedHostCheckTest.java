package com.tibco.sonar.plugins.bw.check.activity.http.request;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

public class HardCodedHostCheckTest extends AbstractCheckTester {
	
	@Test
	public void HostHardCodedWithMappingOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/HTTPRequestReplyActivity/HostHardCodedWithMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedHostCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void HostHardCodedWithoutMapping()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/HTTPRequestReplyActivity/HostHardCodedWithoutMapping.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedHostCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(23, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void HostMappingHardCoded()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/HTTPRequestReplyActivity/HostMappingHardCoded.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedHostCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(57, sourceCode.getViolations().get(0).getLine());
	}

}
