package com.tibco.sonar.plugins.bw.check.activity;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.utils.bw.model.Constants;

public class CustomProcessHardCodedCheckTest extends AbstractCheckTester {
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration1()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration1.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration2()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration2.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration3()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration3.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(34, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration4()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration4.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration5()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration5.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration6()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration6.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingNoConfiguration7()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMappingNoConfiguration7.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedMappingAndConfiguration()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedMapping&Configuration.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(36, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityHardCodedConfigurationNoMapping()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityHardCodedConfigurationNoMapping.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(25, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void ActivityOKConfigEmptyMapping()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityOKConfigEmptyMapping.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void ActivityOKMappingHardCodedConfig()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityOKMappingHardCodedConfig.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void ActivityOKMappingOKConfig()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/AbstractProcessHardCoded/ActivityOKMappingOKConfig.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomHardCodedCheck.class, "activityType",
				Constants.ACTIVITY_TYPES.JMS_QSENDMESSAGE.getName(),
				"configXPath", "//SessionAttributes/destination",
				"inputBindingXPath", "//*[local-name()='ActivityInput']/destinationQueue",
				"message", "Error");

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}

}
