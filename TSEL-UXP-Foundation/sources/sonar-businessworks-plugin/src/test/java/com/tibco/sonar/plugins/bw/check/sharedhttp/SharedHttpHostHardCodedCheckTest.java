package com.tibco.sonar.plugins.bw.check.sharedhttp;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.SharedHttpRuleRepository;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.source.XmlSource;

public class SharedHttpHostHardCodedCheckTest extends AbstractCheckTester{

	  @Test
	  public void HTTPConnectionHostHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/HTTP/KO/HTTPConnectionHostHardCoded.sharedhttp";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedHttpRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedHostCheck.class);
	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(4, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void HTTPConnectionPortHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/HTTP/KO/HTTPConnectionPortHardCoded.sharedhttp";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedHttpRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedHostCheck.class);
	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }
	  
	  @Test
	  public void HTTPConnection() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/HTTP/OK/HTTPConnection.sharedhttp";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedHttpRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedHostCheck.class);
	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }

}
