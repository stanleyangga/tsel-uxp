/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.check;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.sonar.api.profiles.RulesProfile;
import org.sonar.api.rules.ActiveRule;
import org.sonar.api.rules.ActiveRuleParam;
import org.sonar.api.rules.AnnotationRuleParser;
import org.sonar.api.rules.Rule;
import org.sonar.api.utils.SonarException;
import org.sonar.api.utils.ValidationMessages;

import com.tibco.sonar.plugins.bw.AbstractBusinessWorksPluginTester;
import com.tibco.sonar.plugins.bw.source.Source;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public abstract class AbstractCheckTester extends
		AbstractBusinessWorksPluginTester {

	private static final String INCORRECT_NUMBER_OF_VIOLATIONS = "Incorrect number of violations";

	private void configureDefaultParams(AbstractCheck check, Rule rule) {
		ValidationMessages validationMessages = ValidationMessages.create();
		RulesProfile rulesProfile = getProfileDefinition().createProfile(
				validationMessages);

		rulesProfile.activateRule(rule, null);
		ActiveRule activeRule = rulesProfile.getActiveRule(rule);

		assertNotNull("Could not find activeRule", activeRule);

		try {
			if (activeRule.getActiveRuleParams() != null) {
				for (ActiveRuleParam param : activeRule.getActiveRuleParams()) {
					Object value = PropertyUtils.getProperty(check, param
							.getRuleParam().getKey());
					if (value instanceof Integer) {
						value = Integer.parseInt(param.getValue());
					} else {
						value = param.getValue();
					}
					PropertyUtils.setProperty(check, param.getRuleParam()
							.getKey(), value);
				}
			}
		} catch (IllegalAccessException e) {
			throw new SonarException(e);
		} catch (InvocationTargetException e) {
			throw new SonarException(e);
		} catch (NoSuchMethodException e) {
			throw new SonarException(e);
		}
	}

	private Rule getRule(String repositoryKey, String ruleKey,
			Class<? extends AbstractCheck> checkClass) {

		AnnotationRuleParser parser = new AnnotationRuleParser();
		List<Rule> rules = parser.parse(repositoryKey,
				Arrays.asList(new Class[] { checkClass }));
		for (Rule rule : rules) {
			if (rule.getKey().equals(ruleKey)) {
				return rule;
			}
		}
		return null;
	}

	protected AbstractCheck instantiateCheck(String repositoryKey,
			Class<? extends AbstractCheck> checkClass, String... params) {
		try {
			AbstractCheck check = checkClass.newInstance();
			String ruleKey = checkClass.getAnnotation(
					org.sonar.check.Rule.class).key();

			Rule rule = getRule(repositoryKey, ruleKey, checkClass);
			assertNotNull("Could not find rule", rule);
			check.setRule(rule);
			configureDefaultParams(check, rule);

			for (int i = 0; i < params.length / 2; i++) {
				BeanUtils.setProperty(check, params[i * 2], params[i * 2 + 1]);
				assertNotNull(BeanUtils.getProperty(check, params[i * 2]));
			}
			return check;
		} catch (IllegalAccessException e) {
			throw new SonarException(e);
		} catch (InstantiationException e) {
			throw new SonarException(e);
		} catch (InvocationTargetException e) {
			throw new SonarException(e);
		} catch (NoSuchMethodException e) {
			throw new SonarException(e);
		}
	}

	protected <T extends Source> Source parseAndCheckSource(Reader reader,
			String repositoryKey, Class<T> sourceClass,
			Class<? extends AbstractCheck> checkClass, String... params) {
		return parseAndCheckSource(sourceClass, reader, repositoryKey, null,
				null, checkClass, params);
	}

	protected <T extends Source> T parseAndCheckSource(Class<T> sourceClass,
			Reader reader, String repositoryKey, java.io.File file,
			String code, Class<? extends AbstractCheck> checkClass,
			String... params) {

		AbstractCheck check = instantiateCheck(repositoryKey, checkClass,
				params);

		@SuppressWarnings("unchecked")
		T sourceCode = (T) getSource(sourceClass, file, code);

		if (sourceCode.parseSource(mockFileSystem().sourceCharset())) {
			check.validate(sourceCode);
		}

		return sourceCode;
	}

	protected <T extends Source> void checkSource(T sourceCode,
			String repositoryKey, Class<? extends AbstractCheck> checkClass,
			String... params) {

		AbstractCheck check = instantiateCheck(repositoryKey, checkClass,
				params);

		check.validate(sourceCode);
	}

	protected <T extends Source> void parseCheckAndAssert(String fragment,
			String repositoryKey, Class<T> sourceClass,
			Class<? extends AbstractCheck> clazz, int numViolations,
			String... params) {
		Reader reader = new StringReader(fragment);
		Source sourceCode = parseAndCheckSource(sourceClass, reader,
				repositoryKey, null, fragment, clazz, params);

		assertEquals(INCORRECT_NUMBER_OF_VIOLATIONS, numViolations, sourceCode
				.getViolations().size());
	}

}
