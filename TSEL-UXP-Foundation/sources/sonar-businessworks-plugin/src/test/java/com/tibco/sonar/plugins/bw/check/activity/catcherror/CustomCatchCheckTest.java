package com.tibco.sonar.plugins.bw.check.activity.catcherror;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

public class CustomCatchCheckTest extends AbstractCheckTester {
	
	@Test
	public void CatchNotAllKO()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchAll/CatchNotAllKO.process";
		FileReader reader = new FileReader(fileName);
		String message = "No catch all activity found";
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "",
				"catchFaultElementValue", "",
				"notFoundMessage", message,
				"noCatchMessage", "None catch activity exists");
		
		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(1, sourceCode.getViolations().get(0).getLine());
		assertEquals(message, sourceCode.getViolations().get(0).getMessage());
	}
	
	@Test
	public void CatchAllOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchAll/CatchAllOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "",
				"catchFaultElementValue", "",
				"notFoundMessage", "No catch all activity found",
				"noCatchMessage", "None catch activity exists");
		
		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	

	@Test
	public void CatchAllKO()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchAll/CatchAllKO.process";
		FileReader reader = new FileReader(fileName);
		String message = "None catch activity exists";
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "",
				"catchFaultElementValue", "",
				"notFoundMessage", "No catch all activity found",
				"noCatchMessage", message);
		
		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(1, sourceCode.getViolations().get(0).getLine());
		assertEquals(message, sourceCode.getViolations().get(0).getMessage());
	}
	
	/*
	 * BWException
	 */
	
	@Test
	public void CatchAllBWExceptionKO()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchBWException/CatchAllBWExceptionKO.process";
		FileReader reader = new FileReader(fileName);
		String message = "No catch all activity found";
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "fault",
				"catchFaultElementValue", "localname=bwException namespace=http://soa.coe.com/CommonTypes/Mediation/Framework/BWException",
				"notFoundMessage", message,
				"noCatchMessage", "None catch activity exists");
		
		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(1, sourceCode.getViolations().get(0).getLine());
		assertEquals(message, sourceCode.getViolations().get(0).getMessage());
	}
	
	@Test
	public void CatchBWExceptionKO()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchBWException/CatchBWExceptionKO.process";
		FileReader reader = new FileReader(fileName);
		String message = "None catch activity exists";
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "fault",
				"catchFaultElementValue", "localname=bwException namespace=http://soa.coe.com/CommonTypes/Mediation/Framework/BWException",
				"notFoundMessage", "No catch all activity found",
				"noCatchMessage", message);
		
		
		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(1, sourceCode.getViolations().get(0).getLine());
		assertEquals(message, sourceCode.getViolations().get(0).getMessage());
		
	}
	

	@Test
	public void CatchBWExceptionOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchBWException/CatchBWExceptionOK.process";
		FileReader reader = new FileReader(fileName);
		String message = "None catch activity exists";
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "fault",
				"catchFaultElementValue", "localname=bwException namespace=http://soa.coe.com/CommonTypes/Mediation/Framework/BWException",
				"notFoundMessage", "No catch all activity found",
				"noCatchMessage", message);
		
		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void CatchNotBWExceptionKO()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/CatchBWException/CatchNotBWExceptionKO.process";
		FileReader reader = new FileReader(fileName);
		String message = "None catch activity exists";
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				CustomCatchCheck.class, 
				"catchActivityType", "com.tibco.pe.core.CatchActivity",
				"catchAllElementName", "catchAll",
				"catchAllElementValue", "true",
				"catchFaultElementName", "fault",
				"catchFaultElementValue", "localname=bwException namespace=http://soa.coe.com/CommonTypes/Mediation/Framework/BWException",
				"notFoundMessage", message,
				"noCatchMessage", "No catch all activity found");
		
		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(1, sourceCode.getViolations().get(0).getLine());
		assertEquals(message, sourceCode.getViolations().get(0).getMessage());
	}
}
