package com.tibco.sonar.plugins.bw.check.activity.jms.queue.receiver;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

public class HardCodedMaxSessionsCheckTest extends AbstractCheckTester {
	
	@Test
	public void MaxSessionExistAndOKClient()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionExistAndOKClient.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void MaxSessionExistAndOKLocal()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionExistAndOKLocal.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void MaxSessionExistAndOKTransactional()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionExistAndOKTransactional.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}

	@Test
	public void MaxSessionHardCodedClient()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedClient.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(18, sourceCode.getViolations().get(0).getLine());
	}

	@Test
	public void MaxSessionHardCodedLocal()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedLocal.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(18, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void MaxSessionHardCodedTransactional()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedTransactional.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(18, sourceCode.getViolations().get(0).getLine());
	}

	
	@Test
	public void MaxSessionHardCodedOKEMSNoACK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedOKEMSNoACK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void MaxSessionHardCodedOKEMSExpliciteDup()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedOKEMSExpliciteDup.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void MaxSessionHardCodedOKEMSExplicite()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedOKEMSExplicite.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void MaxSessionHardCodedOKAuto()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/JMSQueueReceiverActivity/MaxSessionHardCodedOKAuto.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedMaxSessionsCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
}
