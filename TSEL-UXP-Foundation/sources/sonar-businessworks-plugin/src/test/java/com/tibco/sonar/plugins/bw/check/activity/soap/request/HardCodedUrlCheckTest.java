package com.tibco.sonar.plugins.bw.check.activity.soap.request;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

public class HardCodedUrlCheckTest extends AbstractCheckTester {
	
	@Test
	public void UrlConfigAndMappingHardCoded()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/UrlConfigAndMappingHardCoded.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedUrlCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void UrlConfigAndMappingOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/UrlConfigAndMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedUrlCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}

	@Test
	public void UrlConfigHardCodedWithMappingOK() throws FileNotFoundException{
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/UrlConfigHardCodedWithMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedUrlCheck.class);
	
		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
		
	@Test
	public void UrlConfigHardCodedWithoutMapping()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/UrlConfigHardCodedWithoutMapping.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedUrlCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(29, sourceCode.getViolations().get(0).getLine());
	}

	@Test
	public void UrlMappingHardCodedConfigOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/UrlMappingHardCodedConfigOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedUrlCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}

}
