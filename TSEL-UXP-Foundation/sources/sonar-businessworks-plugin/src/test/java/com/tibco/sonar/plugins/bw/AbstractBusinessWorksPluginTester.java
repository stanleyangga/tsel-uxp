/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw;

import java.io.IOException;
import java.nio.charset.Charset;

import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.sonar.api.profiles.AnnotationProfileParser;
import org.sonar.api.profiles.ProfileDefinition;
import org.sonar.api.profiles.RulesProfile;
import org.sonar.api.rules.Rule;
import org.sonar.api.rules.RuleFinder;
import org.sonar.api.scan.filesystem.ModuleFileSystem;
import org.sonar.api.utils.ValidationMessages;

import com.tibco.sonar.plugins.bw.profile.CommonRulesSonarWayProfile;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.source.SourceFactory;

import javax.xml.XMLConstants;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Matthijs Galesloot
 */
public class AbstractBusinessWorksPluginTester {

	public TemporaryFolder temporaryFolder;

	static {
		System.setProperty("javax.xml.validation.SchemaFactory:"
				+ XMLConstants.W3C_XML_SCHEMA_NS_URI,
				"org.apache.xerces.jaxp.validation.XMLSchemaFactory");
	}

	/**
	 * create standard rules profile
	 */
	protected RulesProfile createStandardRulesProfile() {
		ProfileDefinition profileDefinition = getProfileDefinition();
		ValidationMessages messages = ValidationMessages.create();
		RulesProfile profile = profileDefinition.createProfile(messages);
		assertEquals(0, messages.getErrors().size());
		assertEquals(0, messages.getWarnings().size());
		assertEquals(0, messages.getInfos().size());
		return profile;
	}

	protected CommonRulesSonarWayProfile getProfileDefinition() {
		RuleFinder ruleFinder = ruleFinder();
		return new CommonRulesSonarWayProfile(new AnnotationProfileParser(
				ruleFinder));
	}

	static RuleFinder ruleFinder() {
		return when(
				mock(RuleFinder.class).findByKey(Mockito.anyString(),
						Mockito.anyString())).thenAnswer(new Answer<Rule>() {
			public Rule answer(InvocationOnMock invocation) {
				Object[] arguments = invocation.getArguments();
				return Rule.create((String) arguments[0],
						(String) arguments[1], (String) arguments[1])
						.setDescription("Mocked description");
			}
		}).getMock();
	}

	protected ModuleFileSystem mockFileSystem() {
		temporaryFolder = new TemporaryFolder();
		try {
			temporaryFolder.create();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ModuleFileSystem fs = mock(ModuleFileSystem.class);
		when(fs.sourceCharset()).thenReturn(Charset.defaultCharset());
		when(fs.workingDir()).thenReturn(temporaryFolder.newFolder("temp"));
		return fs;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Source getSource(Class clazz,java.io.File file, String code) {
		if(file != null){
			return SourceFactory.create(clazz, file);
		}else if(code != null){
			return SourceFactory.create(clazz, code);
		}else{
			return null;
		}
	}
}
