package com.tibco.sonar.plugins.bw.check.sharedjdbc;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.SharedJdbcRuleRepository;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.source.XmlSource;

public class SharedJdbcHardCodedCheckTest extends AbstractCheckTester{

	  @Test
	  public void JDBCConnectionPasswordHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JDBC/KO/JDBCConnectionPasswordHardCoded.sharedjdbc";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJdbcRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedPasswordCheck.class);
	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(11, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JDBCConnectionURLHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JDBC/KO/JDBCConnectionURLHardCoded.sharedjdbc";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJdbcRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedPasswordCheck.class);
	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(13, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JDBCConnectionUserNameHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JDBC/KO/JDBCConnectionUserNameHardCoded.sharedjdbc";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJdbcRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedPasswordCheck.class);
	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(12, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JDBCConnection() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JDBC/OK/JDBCConnection.sharedjdbc";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJdbcRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJdbcRuleRepository.REPOSITORY_KEY, HardCodedPasswordCheck.class);
	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }

}
