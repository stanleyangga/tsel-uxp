package com.tibco.sonar.plugins.bw.check.sharedjms;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.SharedJmsRuleRepository;
import com.tibco.sonar.plugins.bw.source.Source;
import com.tibco.sonar.plugins.bw.source.XmlSource;

public class SharedJmsHardCodedCheckTest extends AbstractCheckTester{

	  @Test
	  public void JMSConnectionPasswordHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/KO/JMSConnectionPasswordHardCoded.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);
	    
	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(18, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JMSConnectionPasswordJNDIHardCodedURLOK() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/KO/JMSConnectionPasswordJNDIHardCoded-URLOK.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(14, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JMSConnectionURLHardCodedJNDIOK() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/KO/JMSConnectionURLHardCoded-JNDIOK.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(8, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JMSConnectionURLJNDIHardCodedURLOK() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/KO/JMSConnectionURLJNDIHardCoded-URLOK.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(9, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JMSConnectionUserNameHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/KO/JMSConnectionUserNameHardCoded.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(17, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JMSConnectionUserNameJNDIHardCodedURLOK() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/KO/JMSConnectionUserNameJNDIHardCoded-URLOK.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
	    assertEquals(13, sourceCode.getViolations().get(0).getLine());
	  }
	  
	  @Test
	  public void JMSConnection() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/OK/JMSConnection.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }
	  
	  @Test
	  public void JMSConnectionJNDIOKURLHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/OK/JMSConnectionJNDIOK-URLHardCoded.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }
	  
	  @Test
	  public void JMSConnectionURLOKJNDIPasswordHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/OK/JMS ConnectionURLOK-JNDIPasswordHardCoded.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }
	  
	  @Test
	  public void JMSConnectionURLOKJNDIURLHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/OK/JMSConnectionURLOK-JNDIURLHardCoded.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }
	  
	  @Test
	  public void JMSConnectionURLOKJNDIUserNameHardCoded() throws FileNotFoundException {
	    String fileName = "src/test/resources/bw/maven/bwproject/src/Resources/Connections/JMS/OK/JMSConnectionURLOK-JNDIUserNameHardCoded.sharedjmscon";
	    FileReader reader = new FileReader(fileName);
	    Source sourceCode = parseAndCheckSource(XmlSource.class, reader, SharedJmsRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, HardCodedPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedUserCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiPasswordCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUrlCheck.class);
	    checkSource(sourceCode, SharedJmsRuleRepository.REPOSITORY_KEY, HardCodedJndiUserCheck.class);

	    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
	  }

}
