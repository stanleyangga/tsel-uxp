package com.tibco.sonar.plugins.bw.check.activity.soap.request;

import static junit.framework.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

public class HardCodedTimeoutCheckTest extends AbstractCheckTester {
	

	@Test
	public void TimeoutMappingHardCodedConfigOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/TimeoutMappingHardCodedConfigOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedTimeoutCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
	
	@Test
	public void TimeoutConfigAndMappingOK()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/TimeoutConfigAndMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedTimeoutCheck.class);

		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	

	@Test
	public void TimeoutConfigHardCodedWithMappingOK() throws FileNotFoundException{
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/TimeoutConfigHardCodedWithMappingOK.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedTimeoutCheck.class);
	
		assertEquals("Incorrect number of violations", 0, sourceCode
				.getViolations().size());
	}
	
	@Test
	public void TimeoutConfigHardCodedWithoutMapping()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/TimeoutConfigHardCodedWithoutMapping.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedTimeoutCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(22, sourceCode.getViolations().get(0).getLine());
	}

	@Test
	public void TimeoutConfigAndMappingHardCoded()
			throws FileNotFoundException {
		String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/SOAPRequestReplyActivity/TimeoutConfigAndMappingHardCoded.process";
		FileReader reader = new FileReader(fileName);
		Source sourceCode = parseAndCheckSource(ProcessSource.class, reader,
				ProcessRuleRepository.REPOSITORY_KEY,
				new java.io.File(fileName), null,
				HardCodedTimeoutCheck.class);

		assertEquals("Incorrect number of violations", 1, sourceCode
				.getViolations().size());
		assertEquals(35, sourceCode.getViolations().get(0).getLine());
	}
}
