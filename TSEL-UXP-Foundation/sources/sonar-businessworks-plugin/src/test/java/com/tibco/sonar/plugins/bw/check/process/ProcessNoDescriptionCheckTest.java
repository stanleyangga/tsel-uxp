/*
 * SonarQube XML Plugin
 * Copyright (C) 2010 SonarSource
 * dev@sonar.codehaus.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tibco.sonar.plugins.bw.check.process;

import org.junit.Test;

import com.tibco.sonar.plugins.bw.check.AbstractCheckTester;
import com.tibco.sonar.plugins.bw.rulerepository.ProcessRuleRepository;
import com.tibco.sonar.plugins.bw.source.ProcessSource;
import com.tibco.sonar.plugins.bw.source.Source;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static junit.framework.Assert.assertEquals;
/**
 * @author Matthijs Galesloot
 */
public class ProcessNoDescriptionCheckTest extends AbstractCheckTester {

  @Test
  public void processWithoutDescription() throws FileNotFoundException {
    String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/ProcessNoDescriptionCheck/ProcessWithoutDescription.process";
    FileReader reader = new FileReader(fileName);
    Source sourceCode = parseAndCheckSource(ProcessSource.class, reader, ProcessRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, NoDescriptionCheck.class);

    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
    assertEquals(1, sourceCode.getViolations().get(0).getLine());
  }
  
  @Test
  public void processWithDeletedDescription() throws FileNotFoundException {
    String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/ProcessNoDescriptionCheck/ProcessWithDeletedDescription.process";
    FileReader reader = new FileReader(fileName);
    Source sourceCode = parseAndCheckSource(ProcessSource.class, reader, ProcessRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, NoDescriptionCheck.class);

    assertEquals("Incorrect number of violations", 1, sourceCode.getViolations().size());
    assertEquals(4, sourceCode.getViolations().get(0).getLine());
  }
  
  @Test
  public void processWithDescription() throws FileNotFoundException {
    String fileName = "src/test/resources/bw/maven/bwproject/src/Processes/Rules/ProcessNoDescriptionCheck/ProcessWithDescription.process";
    FileReader reader = new FileReader(fileName);
    Source sourceCode = parseAndCheckSource(ProcessSource.class, reader, ProcessRuleRepository.REPOSITORY_KEY, new java.io.File(fileName), null, NoDescriptionCheck.class);

    assertEquals("Incorrect number of violations", 0, sourceCode.getViolations().size());
  }

}
