package com.tibco.psg.dia.client;

import java.io.File;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.tibco.psg.dia.client.bean.ConfigBean;
import java.io.Serializable;

public class ConfigLoader implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -943541532567478283L;
	
	private NodeList nList;
    private Vector<ConfigBean> vecConfig;

    public ConfigLoader() {
       this.vecConfig = new Vector<ConfigBean>(); 
    }

    public ConfigLoader getConfiguration(){
        return this;    
    }
    
    public void addConfiguration(String destinationhost,String destinationport,String originhost,String originrealm,String vendorid,String productname,String firmwarerevision) throws Exception{
        
        ConfigBean cb = new ConfigBean();
        cb.setDestinationHost(destinationhost);
        cb.setDestinationPort(Integer.parseInt(destinationport));
        cb.setOriginHost(originhost);
        cb.setOriginRealm(originrealm);
        cb.setVendorId(Integer.parseInt(vendorid));
        cb.setProductName(productname);
        cb.setDestinationHost(destinationhost);
        cb.setFirmwareRevision(Integer.parseInt(firmwarerevision));
        cb.setConnId(Constant.SRC + originhost + "_"
                + Constant.DST + destinationhost + ":"
                + destinationport); // example: SRC=10.193.32.82_DST=192.168.170.130:3869

        vecConfig.add(cb);

    }

    public ConfigLoader(String filePath) {
        vecConfig = new Vector<ConfigBean>();

        try {
            File fXmlFile = new File(filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
//			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
//			System.out.println("--------------------------------");
            nList = doc.getElementsByTagName("diameter");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Load all connections configuration from config file into Hashtable<String, ConfigBean> hstblConfig
        loadDiaConnConfig();
    }

    private void loadDiaConnConfig() {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
//			    System.out.println("destinationhost : " + getTagValue("destinationhost", eElement));
//			    System.out.println("destinationport : " + getTagValue("destinationport", eElement));
//		        System.out.println("originhost : " + getTagValue("originhost", eElement));
//		        System.out.println("originrealm : " + getTagValue("originrealm", eElement));
//			    System.out.println("vendorid : " + getTagValue("vendorid", eElement));
//		        System.out.println("productname : " + getTagValue("productname", eElement));
//		        System.out.println("firmwarerevision : " + getTagValue("firmwarerevision", eElement));
//				System.out.println("connId : " + Constant.SRC + getTagValue("originhost", eElement) +"_"+ 
//												Constant.DST + getTagValue("destinationhost", eElement) +":"+
//				 								getTagValue("destinationport", eElement));
//		        System.out.println("--------------------------------");
                ConfigBean cb = new ConfigBean();
                cb.setDestinationHost(getTagValue("destinationhost", eElement));
                cb.setDestinationPort(Integer.parseInt(getTagValue("destinationport", eElement)));
                cb.setOriginHost(getTagValue("originhost", eElement));
                cb.setOriginRealm(getTagValue("originrealm", eElement));
                cb.setVendorId(Integer.parseInt(getTagValue("vendorid", eElement)));
                cb.setProductName(getTagValue("productname", eElement));
                cb.setDestinationHost(getTagValue("destinationhost", eElement));
                cb.setFirmwareRevision(Integer.parseInt(getTagValue("firmwarerevision", eElement)));
                cb.setConnId(Constant.SRC + getTagValue("originhost", eElement) + "_"
                        + Constant.DST + getTagValue("destinationhost", eElement) + ":"
                        + getTagValue("destinationport", eElement)); // example: SRC=10.193.32.82_DST=192.168.170.130:3869

                vecConfig.add(cb);
            }
        }
    }

    private String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
    }

    public Vector<ConfigBean> getConfigSet() {
        return vecConfig;
    }
//	public static void main(String args[]) throws Exception {
//		ConfigLoader obj = new ConfigLoader("/home/michael/Desktop/tib_ConfigLog/config/config.xml");
//	}
}
