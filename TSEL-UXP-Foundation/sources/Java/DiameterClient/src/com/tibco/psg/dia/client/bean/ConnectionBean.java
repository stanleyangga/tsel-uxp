package com.tibco.psg.dia.client.bean;

import java.io.Serializable;

import com.tibco.psg.dia.client.DiameterClient;

public class ConnectionBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private DiameterClient diaClient = null;
	private boolean diaClientAlive	= true; // To show DiameterClient connection status alive or dead, default true
	private String connId = "";
	
	public DiameterClient getDiaClient() {
		return diaClient;
	}
	public void setDiaClient(DiameterClient diaClient) {
		this.diaClient = diaClient;
	}
	public boolean isDiaClientAlive() {
		return diaClientAlive;
	}
	public void setDiaClientAlive(boolean diaClientAlive) {
		this.diaClientAlive = diaClientAlive;
	}
	public String getConnId() {
		return connId;
	}
	public void setConnId(String connId) {
		this.connId = connId;
	}
}
