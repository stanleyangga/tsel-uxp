package com.tibco.psg.dia.client;

import java.io.Serializable;
import org.apache.log4j.Logger;

import dk.i1.diameter.Message;
import dk.i1.diameter.Utils;
import dk.i1.diameter.node.Capability;
import dk.i1.diameter.node.InvalidSettingException;
import dk.i1.diameter.node.NodeSettings;
import dk.i1.diameter.node.Peer;
import dk.i1.diameter.node.SimpleSyncClient;

public class DiameterClient implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// Client node properties //
	private String 				hostId           = null;		// domain.example.com
	private String 				realm            = null;		// example.com
	private int    				vendorId		 = 0;			// 99999
	private String 				productName		 = null;		// my_test_client
	private int    				firmwareRevision = 0;  			// 0x01000000  -->  16777216
	
	public int  				applicationId	 = 0;			// 5
	
	// Server node properties //
	private String 				destHost 		 = null;		// 192.168.170.130
	private int    				destPort 		 = 0;			// 3868
	private NodeSettings     	nodeSettings 	 = null;
	private Peer             	peers[]      	 = null;
	private SimpleSyncClient 	ssc 		  	 = null;
	
//	private static Logger 		logger		 	 = Logger.getLogger("bw.logger");
	private static Logger 		logger		 	 = Logger.getLogger(DiameterClient.class);
	
	
	public DiameterClient(String hostId, String realm, int vendorId, String productName, int applicationId, 
						  String destHost, int destPort, int firmwareRevision) {
		
		this.hostId 	   = hostId;
		this.realm  	   = realm;
		this.vendorId 	   = vendorId;
		this.productName   = productName;
		this.applicationId = applicationId;
		this.destHost 	   = destHost;
		this.destPort 	   = destPort;
		this.firmwareRevision = firmwareRevision;
	}
	
	public void start(long timeOut, long intervalWatchDog) throws Exception {
		
		stop();
		
		// Client node properties //
		try {
			Capability capability = new Capability();
			capability.addAuthApp(applicationId);
			nodeSettings  = new NodeSettings(hostId, realm, vendorId, capability, 0, productName, firmwareRevision);
			nodeSettings.setWatchdogInterval(intervalWatchDog);	// DWR-DWA internal, mininum 6000 mili seconds
		} 
		catch (InvalidSettingException e) {
			logger.info("Fail to initialize node setting, invalid input define");
			throw e;
		}
		// Server node properties //
		peers = new Peer[]{new Peer(destHost, destPort)};
		// Start the stack //
		ssc = new SimpleSyncClient(nodeSettings, peers);
		ssc.start();
		ssc.waitForConnectionTimeout(timeOut); // Wait until at least one connection has been established or until the timeout (ms) expires.
	}
	
	public void stop() throws Exception {
		// Stop the stack //
		try {
			if(ssc != null) 
				ssc.stop();
		} 
		catch (Exception e) {
			logger.info("Fail to stop connection, connection may already closed");
			throw e;
		}
	}
	
	public void stop(long timeOut) throws Exception {
		// Stop the stack //
		try {
			if(ssc != null) 
				ssc.stop(timeOut);
		} 
		catch (Exception e) {
			logger.info("Fail to stop connection, connection may already closed");
			throw e;
		}
	}
	
	public String makeNewSessionId(String sessionId) {
		// Generate a session id //
		// example: domain.example.com;1334733357;988;<sessionId>
		return ssc.node().makeNewSessionId(sessionId);
	}
	
	public Message sendRequest(Message rqMsg) {
		// Set Diameter AVP message Host & Realm //
		//  { Origin-Host }
		//  { Origin-Realm }
		ssc.node().addOurHostAndRealm(rqMsg);

		// Set M-bit according RFC3588 //
		Utils.setMandatory_RFC3588(rqMsg);
		
		// Send it //
		Message rsMsg = ssc.sendRequest(rqMsg);
		logger.info("Send Diameter Request (CCR)");
		
		return rsMsg;
	}
        
        public Message sendRequest(Message rqMsg,long timeOut) {
		// Set Diameter AVP message Host & Realm //
		//  { Origin-Host }
		//  { Origin-Realm }
		ssc.node().addOurHostAndRealm(rqMsg);

		// Set M-bit according RFC3588 //
		Utils.setMandatory_RFC3588(rqMsg);
		
		// Send it //
		Message rsMsg = ssc.sendRequest(rqMsg,timeOut);
		logger.info("Send Diameter Request (CCR)");
		
		return rsMsg;
	}
}
