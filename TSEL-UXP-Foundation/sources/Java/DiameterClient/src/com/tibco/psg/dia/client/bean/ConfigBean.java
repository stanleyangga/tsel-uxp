package com.tibco.psg.dia.client.bean;

public class ConfigBean {
	
	private String connId = "";
	private String destinationHost = "";
	private int    destinationPort = 0;
	private String originHost = "";
	private String originRealm = "";
	private int    vendorId = 0;
	private String productName = "";
	private int    firmwareRevision = 0;
	
	public String getConnId() {
		return connId;
	}
	public void setConnId(String connId) {
		this.connId = connId;
	}
	public String getDestinationHost() {
		return destinationHost;
	}
	public void setDestinationHost(String destinationHost) {
		this.destinationHost = destinationHost;
	}
	public int getDestinationPort() {
		return destinationPort;
	}
	public void setDestinationPort(int destinationPort) {
		this.destinationPort = destinationPort;
	}
	public String getOriginHost() {
		return originHost;
	}
	public void setOriginHost(String originHost) {
		this.originHost = originHost;
	}
	public String getOriginRealm() {
		return originRealm;
	}
	public void setOriginRealm(String originRealm) {
		this.originRealm = originRealm;
	}
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getFirmwareRevision() {
		return firmwareRevision;
	}
	public void setFirmwareRevision(int firmwareRevision) {
		this.firmwareRevision = firmwareRevision;
	}
}
