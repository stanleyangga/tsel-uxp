package com.tibco.psg.dia.client.bean;

import java.io.Serializable;

public class LogBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long   timeStampIN = 0l;
	private long   timeStampOUT = 0l;
	private String connId = "";	
	private String commandCode = "";
	private String sessionID = "";
	private String resultCode = "";
	private String message = "";
	private String stackTrace = "";;
	
	public String getConnId() {
		return connId;
	}
	public void setConnId(String connId) {
		this.connId = connId;
	}
	public long getTimeStampIN() {
		return timeStampIN;
	}
	public void setTimeStampIN(long timeStampIN) {
		this.timeStampIN = timeStampIN;
	}
	public long getTimeStampOUT() {
		return timeStampOUT;
	}
	public void setTimeStampOUT(long timeStampOUT) {
		this.timeStampOUT = timeStampOUT;
	}
	public String getCommandCode() {
		return commandCode;
	}
	public void setCommandCode(String commandCode) {
		this.commandCode = commandCode;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStackTrace() {
		return stackTrace;
	}
	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}
}
