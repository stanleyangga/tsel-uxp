package com.tibco.psg.dia.client.log;

import java.io.Serializable;
import java.util.Vector;
import org.apache.log4j.Logger;

import com.tibco.psg.dia.client.bean.LogBean;

public class ThreadLogManager implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Vector<LogBean> vecInitConnLog = null;
	private Vector<LogBean> vecCERALog = null;
	private Vector<LogBean> vecDWRALog = null;
	private Vector<LogBean> vecDPRALog = null;
//	private static Logger logger = Logger.getLogger("bw.logger");
	private static Logger logger = Logger.getLogger(ThreadLogManager.class);
	
	private static ThreadLogManager instance = null;	
	
	protected ThreadLogManager() {
		this.vecInitConnLog = new  Vector<LogBean>();
		this.vecCERALog = new  Vector<LogBean>();
		this.vecDWRALog = new  Vector<LogBean>();
		this.vecDPRALog = new  Vector<LogBean>();
	}
	
	public static ThreadLogManager getInstance() {
		if(instance==null) {
			logger.info("Initialize new ThreadLogManager instance");
			instance = new ThreadLogManager();
			return instance;
		}
		logger.info("Get existing ThreadLogManager instance");
		return instance;
	}

	/* setInitConnLog
	 * To add in init-connection LogBean to vector if not exist, else replace
	 */
	public void setInitConnLog(LogBean logBean) {
		if(vecInitConnLog.size()==0) {
			vecInitConnLog.add(logBean);
		} else {
			boolean isExist = false;
			for(int i=0; i<vecInitConnLog.size(); i++) {
				if(vecInitConnLog.get(i).getConnId().equalsIgnoreCase(logBean.getConnId())) {
					vecInitConnLog.set(i, logBean);
					isExist = true;
					break;
				} 
			}
			if(isExist==false)
				vecInitConnLog.add(logBean);
		}
	}
	
	/* setCERALog
	 * To add in CER-CEA LogBean to vector if not exist, else replace
	 */
	public void setCERALog(LogBean logBean) {
		if(vecCERALog.size()==0) {
			vecCERALog.add(logBean);
		} else {
			boolean isExist = false;
			for(int i=0; i<vecCERALog.size(); i++) {
				if(vecCERALog.get(i).getConnId().equalsIgnoreCase(logBean.getConnId())) {
					vecCERALog.set(i, logBean);
					isExist = true;
					break;
				} 
			}
			if(isExist==false)
				vecCERALog.add(logBean);
		}
	}

	/* setDWRALog
	 * To add in DWR-DWA LogBean to vector if not exist, else replace
	 */
	public void setDWRALog(LogBean logBean) {
		if(vecDWRALog.size()==0) {
			vecDWRALog.add(logBean);
		} else {
			boolean isExist = false;
			for(int i=0; i<vecDWRALog.size(); i++) {
				if(vecDWRALog.get(i).getConnId().equalsIgnoreCase(logBean.getConnId())) {
					vecDWRALog.set(i, logBean);
					isExist = true;
					break;
				} 
			}
			if(isExist==false)
				vecDWRALog.add(logBean);
		}
	}
	
	/* setDPRALog
	 * To add in DPR-DPA LogBean to vector if not exist, else replace
	 */
	public void setDPRALog(LogBean logBean) {
		if(vecDPRALog.size()==0) {
			vecDPRALog.add(logBean);
		} else {
			boolean isExist = false;
			for(int i=0; i<vecDPRALog.size(); i++) {
				if(vecDPRALog.get(i).getConnId().equalsIgnoreCase(logBean.getConnId())) {
					vecDPRALog.set(i, logBean);
					isExist = true;
					break;
				} 
			}
			if(isExist==false)
				vecDPRALog.add(logBean);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/* getConnInitLog
	 * To get init-connection LogBean from vector if exist, else return null
	 */
	public LogBean getConnInitLog(String connId) {
		for(LogBean logBean : vecInitConnLog) {
			if(logBean.getConnId().equalsIgnoreCase(connId))
				return logBean;
		}
		return null;
	}
	
	/* getCERALog
	 * To get CER-CEA LogBean from vector if exist, else return null
	 */
	public LogBean getCERALog(String connId) {
		for(LogBean logBean : vecCERALog) {
			if(logBean.getConnId().equalsIgnoreCase(connId))
				return logBean;
		}
		return null;
	}
	
	/* getDWRALog
	 * To get DWR-DWA LogBean from vector if exist, else return null
	 */
	public LogBean getDWRALog(String connId) {
		for(LogBean logBean : vecDWRALog) {
			if(logBean.getConnId().equalsIgnoreCase(connId))
				return logBean;
		}
		return null;
	}
	
	/* getDWRALog
	 * To get DWR-DWA LogBean from vector if exist, else return null
	 */
	public LogBean getDPRALog(String connId) {
		for(LogBean logBean : vecDPRALog) {
			if(logBean.getConnId().equalsIgnoreCase(connId))
				return logBean;
		}
		return null;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/* getCCRALogs
	 * 
	 */
	public Vector<LogBean> getConnInitLogs() {
		return vecInitConnLog;
	}
	
	/* getCERALogs
	 * 
	 */
	public Vector<LogBean> getCERALogs() {
		return vecCERALog;
	}
	
	/* getDWRALogs
	 * 
	 */
	public Vector<LogBean> getDWRALogs() {
		return vecDWRALog;
	}
	
	/* getDPRALogs
	 * 
	 */
	public Vector<LogBean> getDPRALogs() {
		return vecDPRALog;
	}
}
