package com.tibco.psg.dia.client;

import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.tibco.psg.dia.client.bean.ConfigBean;
import com.tibco.psg.dia.client.bean.ConnectionBean;
import com.tibco.psg.dia.client.bean.LogBean;
import com.tibco.psg.dia.client.log.ThreadLogManager;
import com.tibco.psg.dia.client.DiameterClient;

public class DiameterClientManager {
	
	private ConfigLoader configLoader = null;
	private Vector<ConnectionBean> 	vecConnsPool = null;
	private long initConnTimeOut = 0l;
	private long stopConnTimeOut = 0l;
	private long watchDogInterval = 0l;
	public static ThreadLogManager logManager = null;
//	private static Logger logger = Logger.getLogger("bw.logger");
	private static Logger logger = Logger.getLogger(DiameterClientManager.class);
	private static int currentConnIndex = -1;
	private static int connsPoolSize = 0;
	
	// To load connection configurations (1 ~ * connections)
	public DiameterClientManager(String configFilePath, String configLogFilePath, 
								 long initConnTimeOut, long stopConnTimeOut, long intervalWatchDog) {
		
		this.configLoader = new ConfigLoader(configFilePath);
		this.initConnTimeOut = initConnTimeOut;
		this.stopConnTimeOut = stopConnTimeOut;
		this.watchDogInterval = intervalWatchDog;
		logManager = ThreadLogManager.getInstance();
		PropertyConfigurator.configure(configLogFilePath);
		currentConnIndex = -1;
	}
	
        public DiameterClientManager(String configLogFilePath, 
								 long initConnTimeOut, long stopConnTimeOut, long intervalWatchDog) {

		this.initConnTimeOut = initConnTimeOut;
		this.stopConnTimeOut = stopConnTimeOut;
		this.watchDogInterval = intervalWatchDog;
		logManager = ThreadLogManager.getInstance();
		PropertyConfigurator.configure(configLogFilePath);
		currentConnIndex = -1;
	}
	// To initlialize connections on start up
	// initDiaConns
	// Warning !! initDiaConns() only use it once on start up purpose, does not do stop any connections existing before initialize, 
	public void initDiaConns(ConfigLoader configLoader) throws Exception {
                this.configLoader=configLoader;
		tryTerminateExistPool();
		// Loop the configBean, to create DiaClient connections, add to connBean, add to Vector pool
		vecConnsPool = new Vector<ConnectionBean>();
		Vector<ConfigBean> vecConfig = configLoader.getConfigSet();

		for(ConfigBean configBean : vecConfig) {

			logger.debug("Initialize connection " +configBean.getConnId());
			// Try to initialize a TCP/Diameter connection instance, add into a bean with connection status flag
			ConnectionBean connBean = new ConnectionBean();
			connBean.setConnId(configBean.getConnId());
			connBean.setDiaClient(new DiameterClient(configBean.getOriginHost(),			// hostId 
													 configBean.getOriginRealm(), 			// realm 
													 configBean.getVendorId(),				// vendorId 
													 configBean.getProductName(), 			// productName 
													 4, 									// AuthApplicationId (4 for CCR-CCA) 
													 configBean.getDestinationHost(), 		// destHost
													 configBean.getDestinationPort(), 		// destPort
													 configBean.getFirmwareRevision()));  	// firmwareRevision
													
			// Try catch to set bean with fail flag when initialize connection failed
			tryInitiate(connBean);
			// Add connection to pool
			vecConnsPool.add(connBean);
		}
	}
        
        public void initDiaConns() throws Exception {
		tryTerminateExistPool();
		// Loop the configBean, to create DiaClient connections, add to connBean, add to Vector pool
		vecConnsPool = new Vector<ConnectionBean>();
		Vector<ConfigBean> vecConfig = configLoader.getConfigSet();
		for(ConfigBean configBean : vecConfig) {
			logger.debug("Initialize connection " +configBean.getConnId());
			// Try to initialize a TCP/Diameter connection instance, add into a bean with connection status flag
			ConnectionBean connBean = new ConnectionBean();
			connBean.setConnId(configBean.getConnId());
			connBean.setDiaClient(new DiameterClient(configBean.getOriginHost(),			// hostId 
													 configBean.getOriginRealm(), 			// realm 
													 configBean.getVendorId(),				// vendorId 
													 configBean.getProductName(), 			// productName 
													 4, 									// AuthApplicationId (4 for CCR-CCA) 
													 configBean.getDestinationHost(), 		// destHost
													 configBean.getDestinationPort(), 		// destPort
													 configBean.getFirmwareRevision()));  	// firmwareRevision
													
			// Try catch to set bean with fail flag when initialize connection failed
			tryInitiate(connBean);
			// Add connection to pool
			vecConnsPool.add(connBean);
		}
	}
	
	// To attempt to reInitialize connections that mark as "dead"
	// reInitDiaConns
	public void reInitDiaConns() throws Exception {
		for(ConnectionBean connBean : vecConnsPool) {
			if(!connBean.isDiaClientAlive()) {
								
				Vector<ConfigBean> vecConfig = configLoader.getConfigSet();
				for(ConfigBean configBean : vecConfig) {
					if(connBean.getConnId().equalsIgnoreCase(configBean.getConnId())) {
						logger.debug("Attempt to Re-initialize connection " + configBean.getConnId());
						// Try to ensure the existing connection instance is stopped, before initialize a new one
						tryTerminate(connBean); 
						// Try to initialize a TCP/Diameter connection instance, add into a bean with connection status flag
						connBean.setDiaClient(new DiameterClient(configBean.getOriginHost(),			// hostId 
								 								 configBean.getOriginRealm(), 			// realm 
								 								 configBean.getVendorId(),				// vendorId 
								 								 configBean.getProductName(), 			// productName 
								 								 4, 									// AuthApplicationId (4 for CCR-CCA) 
								 								 configBean.getDestinationHost(), 		// destHost
								 								 configBean.getDestinationPort(),		// destPort
																 configBean.getFirmwareRevision()));  	// firmwareRevision
						// Try catch to set bean with fail flag when initialize connection failed
						tryInitiate(connBean);
					}
				}
				
			}
		}
	}
	
	// Use only for during initialize of connection pool initDiaConns(), in any existing pool present,
	// stop all connections for existing pool before initialize another pool
	private void tryTerminateExistPool() {
		if (vecConnsPool!=null && vecConnsPool.size() > 0) {
			logger.debug("Found existing pool instance, try to terminate it before initialize another pool");
			for(ConnectionBean connBean : vecConnsPool) {
				tryTerminate(connBean);
			}
			vecConnsPool=null;
		}
	}
	
	private void tryInitiate(ConnectionBean connBean) {
		// Try catch to set bean with fail flag when initialize connection failed
		try {
			connBean.getDiaClient().start(initConnTimeOut, watchDogInterval);
			connBean.setDiaClientAlive(true);
			logger.debug("Connection " + connBean.getConnId() +" initiate success");
		} catch (Exception e) {
			try{ connBean.getDiaClient().stop(); } catch(Exception e2){ /* possible connection already closed */ }
			connBean.setDiaClientAlive(false);
			logger.error("Throws exception, possible tcp connection error or wait CEA for response timeout", e);
		}
	}
	private void tryTerminate(ConnectionBean connBean) {
		// Try to ensure the connection instance is stopped
		try{ connBean.getDiaClient().stop(); } catch(Exception e2){ /* possible connection already closed */ }
	}
	
	// To stop all all connections gracefully
	// stopDiaConns
	public void stopDiaConns() throws Exception {
		synchronized (this.vecConnsPool) {
			for(ConnectionBean connBean : vecConnsPool) {
				try {
					logger.debug("Attempt to stop connection " + connBean.getConnId());
					connBean.getDiaClient().stop(stopConnTimeOut);
					logger.debug("Stop connection " + connBean.getConnId() + " success");
				} catch(Exception e) {}
			}
		}
	}
	
	// To mark a connection as "dead" from the pool
	// getDiaConn
	public void markDiaConnFail(String connId) throws Exception {
		for(ConnectionBean connBean : vecConnsPool) {
			if(connBean.getConnId().equalsIgnoreCase(connId)) {
				logger.debug("Mark alive connection as dead " + connBean.getConnId());
				try{ connBean.getDiaClient().stop(); } catch(Exception e){ /* possible connection already closed */ }
				connBean.setDiaClientAlive(false);
			}
		}
	}
	
	// To get the whole connections pool
	// getDiaConnPool
	public Vector<ConnectionBean> getDiaConnPool() {
		logger.debug("Get connections pool");
		return vecConnsPool;
	}
	
	// To get a mark as "alive" connection from the pool
	// getDiaConn
//	public ConnectionBean getDiaConn() {
//		for(ConnectionBean connBean : vecConnsPool) {
//			if(connBean.isDiaClientAlive()) {
//				logger.debug("Get an alive connection " + connBean.getConnId());
//				return connBean;
//			}
//		}
//		logger.debug("Cannot get any alive connection");
//		return null;
//	}
	public ConnectionBean getDiaConn() {
		if(vecConnsPool!=null && vecConnsPool.size() > 0) {
			
			boolean loop = true;
			boolean loop2Round = false;	
			connsPoolSize = vecConnsPool.size();
			
			if(currentConnIndex== -1 || (currentConnIndex + 1) >= connsPoolSize) {
				currentConnIndex = 0;
			} else {
				loop2Round = true;
				currentConnIndex++;
			}
			
			while(loop) {				
				if(vecConnsPool.get(currentConnIndex).isDiaClientAlive()) {
					logger.debug("Get an alive connection (round robin) " + vecConnsPool.get(currentConnIndex).getConnId());
					loop = false;
					return vecConnsPool.get(currentConnIndex);
				}
				
				if(((currentConnIndex + 1) >= connsPoolSize) && (loop2Round==true)) {
					loop2Round=false;
					currentConnIndex = -1;
				}
				if(((currentConnIndex + 1) >= connsPoolSize) && (loop2Round==false)) {
					loop = false;
					break;
				}
				currentConnIndex++;
			}
		}
		logger.debug("Cannot get any alive connection");
		return null;
	}
	
	public Vector<LogBean> getConnInitLogs() {
		// Get connection logBeans for return, with message of success or fail initialize connection		
		logger.debug("Get Init Logs " + logManager.getConnInitLogs().size());
		return logManager.getConnInitLogs();
	}
	public Vector<LogBean> getCERALogs() {
		// Get CER-CEA logBeans for return, with message of success or fail CER-CEA
		logger.debug("Get CCRA Logs " + logManager.getCERALogs().size());
		return logManager.getCERALogs();
	}
	public Vector<LogBean> getDWRALogs() {
		// Get DWR-DWA logBeans for return, with message of success or fail DWR-DWA
		logger.debug("Get DWRA Logs " + logManager.getDWRALogs().size());
		return logManager.getDWRALogs();
	}
	public Vector<LogBean> getDPRALogs() {
		// Get DPR-DPA logBeans for return, with message of success or fail DPR-DPA
		logger.debug("Get DPRA Logs " + logManager.getDPRALogs().size());
		return logManager.getDPRALogs();
	}
}
