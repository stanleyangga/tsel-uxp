package com.tibco.psg.ldap.client;

public class RegexTest {
	public static void main(String[] args) {
		String regex = "^(\\d{2}-\\d{2}-\\d{4}(?:T\\d{2}:\\d{2}:\\d{2})?)$";
		String test = "17-09-2015T00:00:00";
		System.out.println(test.matches(regex));
		System.out.println("==============");
		
		String regex2 = "^(\\w+):(\\d{0,2})(?::(\\d{2}-\\d{2}-\\d{4}(?:T\\d{2}:\\d{2}:\\d{2})?))?(?:,(\\d{2}-\\d{2}-\\d{4}(?:T\\d{2}:\\d{2}:\\d{2})?))?$";
		String test2 = "Roaming_Asia_5::18-09-2015T13:40:46,23-09-2015T13:40:46";
		System.out.println(test2.replaceFirst(regex2, "$1"));
		System.out.println(test2.replaceFirst(regex2, "$2"));
		System.out.println(test2.replaceFirst(regex2, "$3"));
		System.out.println(test2.replaceFirst(regex2, "$4"));
		System.out.println("==============");
	}
}