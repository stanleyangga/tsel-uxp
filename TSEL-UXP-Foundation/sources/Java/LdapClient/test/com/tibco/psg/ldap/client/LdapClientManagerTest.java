package com.tibco.psg.ldap.client;

public class LdapClientManagerTest {
	public static void main(String[] args) throws Exception {
		String configFilePath = "C:/tibco/uxp/git2/TSEL-UXP-Adapter-PCRF/config/sapc-config-DEV.xml";
		String logFilePath = "C:/tibco/uxp/git2/TSEL-UXP-Adapter-PCRF/config/log4j.properties";
		
		LdapClientManager manager = new LdapClientManager();
		manager.init(configFilePath, "true", "10", "5", "10000", "30000", logFilePath);
		manager.initConnections();
		String msisdn = "628111889239";
		manager.search(manager.getConnection(msisdn), 
				"EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=jambala", 
				"objectClass=*", new String[] { "EPC-GroupIds", "EPC-Data"}, 2);
		System.out.println("==============");
		
		manager.search(manager.getConnection(msisdn), 
				"EPC-LimitName=EPC-LimitUsage,EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=jambala", 
				"objectClass=*", new String[] {"EPC-Data"}, 2);
		System.out.println("==============");
		
		manager.search(manager.getConnection(msisdn), 
				"EPC-AccumulatedName="+msisdn+",EPC-SubscribersAccumulatedName=EPC-SubscribersAccumulated,applicationName=EPC-EpcNode,nodeName=jambala", 
				"objectClass=*", new String[] { "EPC-AccumulatedData"}, 2);
		System.out.println("==============");
		
	}
}