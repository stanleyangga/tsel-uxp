package com.tibco.psg.ldap.bean;

import java.io.Serializable;

import com.tibco.psg.ldap.util.CommonUtil;

public class SearchResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3775679072671786492L;
	private String name;
	private Attribute[] attributes;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Attribute[] getAttributes() {
		return attributes;
	}
	public void setAttributes(Attribute[] attributes) {
		this.attributes = attributes;
	}
	
	@Override
	public String toString() {
		return "{\"SearchResult\":{"
				+ "\"Name\":\"" + name + "\","
				+ "\"Attributes\":" + CommonUtil.toStringJSON(attributes)
				+ "}}";
	}
}
