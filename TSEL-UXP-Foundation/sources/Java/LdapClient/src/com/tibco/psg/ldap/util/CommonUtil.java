package com.tibco.psg.ldap.util;

public class CommonUtil {
	
	public static String toStringJSON(Object[] objects) {
		StringBuilder sb = new StringBuilder("[");
		
		if(objects != null && objects.length > 0) {
			int i = 0;
			for (Object object : objects) {
				if(object instanceof String)
					sb.append("\"" + object.toString() + "\"");
				else
					sb.append(object.toString());
				
				if(++i < objects.length)
					sb.append(",");
			}
		}
		
		return sb.append("]").toString();
	}
}
