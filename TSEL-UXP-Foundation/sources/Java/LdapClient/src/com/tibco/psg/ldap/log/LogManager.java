package com.tibco.psg.ldap.log;

import java.io.Serializable;
import java.util.Vector;

import com.tibco.psg.ldap.bean.Log;

public class LogManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3294844851166224869L;
	private Vector<Log> logs;
	private static LogManager instance = null;

	protected LogManager() {
		this.logs = new Vector<Log>();
	}

	public static LogManager getInstance() {
		if (instance == null) {
			instance = new LogManager();
			return instance;
		}

		return instance;
	}
	
	public void appendLog(Log log) {
		this.logs.add(log);
	}
	
	public void appendLog(Log log, String code, long timestampOut, String message, String stackTrace) {
		log.setCode(code);
		log.setTimestampOut(timestampOut);
		log.setMessage(message);
		log.setStackTrace(stackTrace);
		this.logs.add(log);
	}

	public String[] getLogString() {
		Vector<String> logStrings = new Vector<String>();
		for(Log log : logs) {
			logStrings.add(log.toString());
		}
		
		return logStrings.toArray(new String[logStrings.size()]);
	}

	public String[] captureLogString() {
		Vector<String> logStrings = new Vector<String>();
		for(Log log : logs) {
			logStrings.add(log.toString());
		}
		
		logs = new Vector<Log>();
		
		return logStrings.toArray(new String[logStrings.size()]);
	}

	public Vector<Log> getLogs() {
		return logs;
	}

	public void setLogs(Vector<Log> logs) {
		this.logs = logs;
	}
	
}
