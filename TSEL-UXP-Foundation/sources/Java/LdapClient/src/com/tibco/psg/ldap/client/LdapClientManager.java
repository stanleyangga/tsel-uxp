package com.tibco.psg.ldap.client;

import java.io.File;
import java.io.Serializable;
import java.util.Vector;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchResult;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.tibco.psg.ldap.bean.Configuration;
import com.tibco.psg.ldap.bean.Connection;
import com.tibco.psg.ldap.util.CommonUtil;

public class LdapClientManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2972495878662632353L;
	
	private Vector<Connection> connections;
	private static Logger logger = Logger.getLogger("bw.logger");
	
	public LdapClientManager() {
		
	}
	
	public void init(String configFilePath, String useConnectionPool, 
			String poolMaxSize, String poolPrefSize, String connectTimeout, String readTimeout, 
			String logFilePath) throws Exception {
		if(!logFilePath.equals(""))
			PropertyConfigurator.configure(logFilePath);
		System.setProperty("com.sun.jndi.ldap.connect.pool", useConnectionPool);
		System.setProperty("com.sun.jndi.ldap.connect.pool.maxsize", poolMaxSize);
		System.setProperty("com.sun.jndi.ldap.connect.pool.prefsize", poolPrefSize);
		System.setProperty("com.sun.jndi.ldap.connect.timeout", connectTimeout);
		System.setProperty("com.sun.jndi.ldap.read.timeout", readTimeout);
		
		try {
			Configuration[] configurations = loadConfigurationXML(configFilePath);
			
			init(configurations, useConnectionPool, poolMaxSize, poolPrefSize, 
					connectTimeout, readTimeout, logFilePath);
		} catch(Exception e) {
			String message = "File doesn't exists or content XML is invalid";
			logger.error(message);
			throw new Exception(message);
		}
	}
	
	public void init(Configuration[] configurations, String useConnectionPool, 
			String poolMaxSize, String poolPrefSize, String connectTimeout, String readTimeout, 
			String logFilePath) throws Exception {
		if(!logFilePath.equals(""))
			PropertyConfigurator.configure(logFilePath);
		System.setProperty("com.sun.jndi.ldap.connect.pool", useConnectionPool);
		System.setProperty("com.sun.jndi.ldap.connect.pool.maxsize", poolMaxSize);
		System.setProperty("com.sun.jndi.ldap.connect.pool.prefsize", poolPrefSize);
		System.setProperty("com.sun.jndi.ldap.connect.timeout", connectTimeout);
		System.setProperty("com.sun.jndi.ldap.read.timeout", readTimeout);
		
		connections = new Vector<Connection>();
		
		for (Configuration configuration : configurations) {
			Connection connection = new Connection();

			connection.setLdapClient(new LdapClient(configuration.getProviderUrl(), 
					configuration.getUsername(), configuration.getPassword()));
			connection.setAlive(false);
			connection.setNumberRegex("^\\d*(?:" + configuration.getNumberSuffix().replaceAll("\\s*,\\s*", "|").replaceAll("\\*", "") + ")$");
			connection.setNode("[" + configuration.getName() + "]" + configuration.getProviderUrl());
			
			connections.add(connection);
		}
	}
	
	public void add(Connection connection, String baseObject, com.tibco.psg.ldap.bean.Attribute[] p_attributes) throws Exception {
		connection.getLdapClient().add(baseObject, createAttributes(p_attributes));
	}
	
	public void delete(Connection connection, String baseObject) throws Exception {
		connection.getLdapClient().delete(baseObject);
	}
	
	public void addAttributes(Connection connection, String baseObject, com.tibco.psg.ldap.bean.Attribute[] p_attributes) throws Exception {
		connection.getLdapClient().modifyAttributes(baseObject, createAttributes(p_attributes), DirContext.ADD_ATTRIBUTE);
	}
	
	public void removeAttributes(Connection connection, String baseObject, com.tibco.psg.ldap.bean.Attribute[] p_attributes) throws Exception {
		connection.getLdapClient().modifyAttributes(baseObject, createAttributes(p_attributes), DirContext.REMOVE_ATTRIBUTE);
	}
	
	public void replaceAttributes(Connection connection, String baseObject, com.tibco.psg.ldap.bean.Attribute[] p_attributes) throws Exception {
		connection.getLdapClient().modifyAttributes(baseObject, createAttributes(p_attributes), DirContext.REPLACE_ATTRIBUTE);
	}
	
	private Attributes createAttributes(com.tibco.psg.ldap.bean.Attribute[] p_attributes) {
		BasicAttributes attributes = new BasicAttributes();
		for (com.tibco.psg.ldap.bean.Attribute p_attribute : p_attributes) {
			BasicAttribute attribute = new BasicAttribute(p_attribute.getId());
			for (String value : p_attribute.getValues()) {
				attribute.add(value);
			}
			attributes.put(attribute);
		}
		return attributes;
	}
	
	public com.tibco.psg.ldap.bean.SearchResult[] search(Connection connection, String baseObject, String filter, String[] attributeIds, int scope) throws Exception {
		NamingEnumeration<SearchResult> searchResultNE = connection.getLdapClient().search(baseObject, filter, attributeIds, scope);
		if (searchResultNE != null) {
			Vector<com.tibco.psg.ldap.bean.SearchResult> p_searchResults = new Vector<com.tibco.psg.ldap.bean.SearchResult>();
			while (searchResultNE.hasMoreElements()) {
				SearchResult searchResult = searchResultNE.next();
				@SuppressWarnings("unchecked")
				NamingEnumeration<Attribute> attributeNE = (NamingEnumeration<Attribute>) searchResult.getAttributes().getAll();
				
				Vector<com.tibco.psg.ldap.bean.Attribute> p_attributes = new Vector<com.tibco.psg.ldap.bean.Attribute>();
				while(attributeNE.hasMoreElements()) {
					Attribute attribute = attributeNE.next();
					@SuppressWarnings("unchecked")
					NamingEnumeration<Object> objectNE = (NamingEnumeration<Object>) attribute.getAll();
					
					Vector<String> p_values = new Vector<String>(); 
					while(objectNE.hasMoreElements()) {
						p_values.add(objectNE.next().toString());
					}
					
					com.tibco.psg.ldap.bean.Attribute p_attribute = new com.tibco.psg.ldap.bean.Attribute();
					p_attribute.setId(attribute.getID());
					p_attribute.setValues(p_values.toArray(new String[p_values.size()]));
					p_attributes.add(p_attribute);
				}
				
				com.tibco.psg.ldap.bean.SearchResult p_searchResult = new com.tibco.psg.ldap.bean.SearchResult();
				p_searchResult.setName(searchResult.getNameInNamespace());
				p_searchResult.setAttributes(p_attributes.toArray(new com.tibco.psg.ldap.bean.Attribute[p_attributes.size()]));
				p_searchResults.add(p_searchResult);
			}
			
		    try {
		    	searchResultNE.close();
		    } catch (NamingException localNamingException) {}
			
			com.tibco.psg.ldap.bean.SearchResult[] results = p_searchResults.toArray(new com.tibco.psg.ldap.bean.SearchResult[p_searchResults.size()]);
			logger.info("{\"SearchResults\":" + CommonUtil.toStringJSON(results) + "}");
			return results;
		}
		
		return null;
	}
	
	public Connection getConnection(String msisdn) throws Exception {
		if(msisdn != null)
			for (Connection connection : connections)
				if(msisdn.matches(connection.getNumberRegex()))
					return connection;

		String message = "No matching connection config for MSISDN (" + msisdn + ")";
		logger.error(message);
		throw new Exception(message);
	}
	
	public String[] getFailedConnections() {
		Vector<String> failedConnections = null;
		
		for (Connection connection : connections)
			if(!connection.isAlive()) {
				if(failedConnections == null) 
					failedConnections = new Vector<String>();
				failedConnections.add(connection.getNode());
			}
		
		return failedConnections.toArray(new String[failedConnections.size()]);
	}
	
	public void markAsFailed(Connection connection) {
		for (Connection cb : connections)
			if(cb.getNumberRegex().equals(connection.getNumberRegex()))
				cb.setAlive(false);
	}

	public boolean initConnections() throws Exception {
		boolean successful = true;
		for (Connection connection : connections)
			successful &= initConnection(connection);
		return successful;
	}

	public boolean ReinitConnections() throws Exception {
		boolean successful = true;
		for (Connection connection : connections)
			if(!connection.isAlive())
				successful &= initConnection(connection);
		return successful;
	}
	
	private boolean initConnection(Connection connection) throws Exception {
		logger.info("Attempt init connection: " + connection.getLdapClient().toString());
		
		try {
			connection.getLdapClient().init();
			connection.setAlive(true);
			
			logger.info("Init LDAP connection (" + connection.getNode() + ") has completed");
		} catch (Exception e) {
			logger.error("Init LDAP connection (" + connection.getNode() + ") has failed: " + e.getMessage() + ".\n");
			connection.setAlive(false);
		}
		
		return connection.isAlive();
	}

	public boolean terminateConnections() throws Exception {
		for (Connection connection : connections)
			connection.getLdapClient().close();
		return true;
	}

	private Configuration[] loadConfigurationXML(String configFilePath) throws Exception {
		File file = new File(configFilePath);
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(file);
		doc.getDocumentElement().normalize();
		NodeList sapc_config = doc.getElementsByTagName("sapc-config");

		NodeList node = ((Element) sapc_config.item(0)).getElementsByTagName("node");

		Vector<Configuration> configurations = new Vector<Configuration>();
		
		for (int i = 0; i < node.getLength(); i++) {
			Element element = (Element) node.item(i);
			Configuration configuration = new Configuration();
			configuration.setProviderUrl(getTagValue("provider-url", element));
			configuration.setUsername(getTagValue("username", element));
			configuration.setPassword(getTagValue("password", element));
			configuration.setNumberSuffix(getTagValue("number-suffix", element));
			configuration.setName(getTagValue("name", element));
			
			configurations.add(configuration);
		}
		
		return configurations.toArray(new Configuration[configurations.size()]);
	}

	private String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodeList.item(0);
		return node.getNodeValue();
	}
}