package com.tibco.psg.ldap.bean;

import java.io.Serializable;

public class Log implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2611189791673068934L;
	private String code;
	private long timestampIn;
	private long timestampOut;
	private String action;
	private String actor;
	private String message;
	private String stackTrace;
	
	public Log() {
	}
	
	@SuppressWarnings("rawtypes")
	public Log(Class clazz, String actor) {
		this.timestampIn = System.currentTimeMillis();
		this.action = clazz.getName();
		this.actor = actor;
	}
	
	public long getTimeStampIn() {
		return timestampIn;
	}
	public void setTimeStampIn(long timeStampIn) {
		this.timestampIn = timeStampIn;
	}
	public long getTimestampOut() {
		return timestampOut;
	}
	public void setTimestampOut(long timestampOut) {
		this.timestampOut = timestampOut;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActor() {
		return actor;
	}
	public void setActor(String actor) {
		this.actor = actor;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStackTrace() {
		return stackTrace;
	}
	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}
	
	@Override
	public String toString() {
		return "[" + code + "] timestampIn=" + timestampIn + " & timestampOut=" + timestampOut
				+ " & action=" + action + " & actor=" + actor + " & message=" + message
				+ " & stackTrace=" + stackTrace;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
