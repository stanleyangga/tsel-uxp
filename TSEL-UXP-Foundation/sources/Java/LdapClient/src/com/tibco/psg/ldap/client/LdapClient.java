package com.tibco.psg.ldap.client;

import java.util.Hashtable;
import java.util.Set;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

public class LdapClient {
	private Hashtable<String, String> properties;
	private DirContext dirContext;
	private static Logger logger = Logger.getLogger("bw.logger");
	
	public LdapClient() {
		properties = new Hashtable<String, String>();
	}
	
	public LdapClient(String providerUrl, String principal, String credentials) {
		
		properties = new Hashtable<String, String>();
		
		properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		properties.put(Context.PROVIDER_URL, providerUrl);
		properties.put(Context.SECURITY_AUTHENTICATION, "simple");
		properties.put(Context.SECURITY_PRINCIPAL, principal);
		properties.put(Context.SECURITY_CREDENTIALS, credentials);
		
		properties.put("com.sun.jndi.ldap.connect.pool", System.getProperty("com.sun.jndi.ldap.connect.pool"));
		properties.put("com.sun.jndi.ldap.connect.pool.maxsize", System.getProperty("com.sun.jndi.ldap.connect.pool.maxsize"));
		properties.put("com.sun.jndi.ldap.connect.pool.prefsize", System.getProperty("com.sun.jndi.ldap.connect.pool.prefsize"));
		properties.put("com.sun.jndi.ldap.connect.timeout", System.getProperty("com.sun.jndi.ldap.connect.timeout"));
		properties.put("com.sun.jndi.ldap.read.timeout", System.getProperty("com.sun.jndi.ldap.read.timeout"));
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		Set<String> keys = properties.keySet();
		for (String key : keys) {
			sb.append("\n" + key + " : " + properties.get(key));
		}
		return sb.toString();
	}
	
	public void init() throws Exception {
		dirContext = new InitialDirContext(properties);
	}
	
	public void close() {
		if(dirContext != null)
			try {
				dirContext.close();
			} catch (NamingException e) {
				// possibly connection is closed already
			}
		
		dirContext = null;
	}
	
	public NamingEnumeration<SearchResult> search(String baseObject, String filter, String[] attributes, int scope) throws Exception {
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(scope);
		
		try {
			return dirContext.search(baseObject, filter, attributes, searchControls);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	public void add(String baseObject, Attributes attributes) throws Exception {
		Context resultContext = null;
		
		try {
			resultContext = dirContext.createSubcontext(baseObject, attributes);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			if(resultContext != null) {
				try {
					resultContext.close();
				} catch(Exception e2) {
					// possibly connection is closed already
				}
			}
		}
	}
	
	public void delete(String baseObject) throws Exception {
		try {
			dirContext.destroySubcontext(baseObject);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	public void modifyAttributes(String baseObject, Attributes attributes, int operation) throws Exception {
		try {
			ModificationItem[] items = new ModificationItem[attributes.size()];
			@SuppressWarnings("unchecked")
			NamingEnumeration<Attribute> attributeEnum = (NamingEnumeration<Attribute>) attributes.getAll();
			
			int i = 0;
			
			while(attributeEnum.hasMoreElements()) {
				Attribute attribute = attributeEnum.nextElement();
				items[i] = new ModificationItem(operation, attribute);
				
				i++;
			}
			
			dirContext.modifyAttributes(baseObject, items);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		close();
	}
}
