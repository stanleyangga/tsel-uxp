package com.tibco.psg.ldap.bean;

import java.io.Serializable;

public class Configuration implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2469273060872519920L;
	private String name;
	private String providerUrl;
	private String username;
	private String password;
	private String numberSuffix;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProviderUrl() {
		return providerUrl;
	}
	public void setProviderUrl(String providerUrl) {
		this.providerUrl = providerUrl;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNumberSuffix() {
		return numberSuffix;
	}
	public void setNumberSuffix(String numberSuffix) {
		this.numberSuffix = numberSuffix;
	}
}
