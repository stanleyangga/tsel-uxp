package com.tibco.psg.ldap.bean;

import java.io.Serializable;

import com.tibco.psg.ldap.util.CommonUtil;

public class Attribute implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7089684395379747408L;
	private String id;
	private String[] values;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String[] getValues() {
		return values;
	}
	public void setValues(String[] values) {
		this.values = values;
	}
	
	@Override
	public String toString() {
		return "{\"Attribute\":{"
				+ "\"Id\":\"" + id + "\","
				+ "\"Values\":" + CommonUtil.toStringJSON(values)
				+ "}}";
	}
}
