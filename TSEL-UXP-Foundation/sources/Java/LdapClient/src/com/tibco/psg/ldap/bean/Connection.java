package com.tibco.psg.ldap.bean;

import java.io.Serializable;

import com.tibco.psg.ldap.client.LdapClient;

public class Connection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7418555023588202368L;
	private LdapClient ldapClient;
	private boolean alive;
	private String node;
	private String numberRegex;
	
	public LdapClient getLdapClient() {
		return ldapClient;
	}
	public void setLdapClient(LdapClient ldapClient) {
		this.ldapClient = ldapClient;
	}
	public boolean isAlive() {
		return alive;
	}
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	public String getNumberRegex() {
		return numberRegex;
	}
	public void setNumberRegex(String numberRegex) {
		this.numberRegex = numberRegex;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
}
