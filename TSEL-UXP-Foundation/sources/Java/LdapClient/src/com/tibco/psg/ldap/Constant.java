package com.tibco.psg.ldap;

public class Constant {
	static final int PART_SOCCD = 0;
	static final int PART_PRIORITY = 1;
	static final int PART_SUBSCRIPTION_DATE = 2;
	static final int OPERATOR_SPECIFIC_INFO_SOCCD = 0;
	static final int OPERATOR_SPECIFIC_INFO_EXPIRY_DATE = 1;
}
