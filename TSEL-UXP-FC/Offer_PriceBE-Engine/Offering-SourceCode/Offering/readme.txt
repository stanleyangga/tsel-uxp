Steps to configure/build notes

First time only:

Create the following queues:

tibco.afs.extensions.product.model.load
tibco.afs.extension.validation.request
tibco.afs.extension.eligibility.request

and topics:
tibco.afs.extension.validation.response
tibco.afs.extension.eligibility.response



1. Bridge queue tibco.aff.ocv.events.products.publish  to tibco.afs.extensions.product.model.load

triggering a standard product catalogue load will now also publish to the Extensions engine.




Segment catalog publish generates a message which is very like the event OfferConfigurationAndValidation.Events.ProductManagement.Events.Segment.SetSegmentsRequest but has no namespace set. Probably a bug. Also generates 2 messages when publishing segments - one with 25 segments, second with 8 - probably a config?

Created a mirror xsd to this one in the sandpit, with no namespaces.

Event /AFSExtension/ProductManagement/Events/SegmentCatalogueEvent.event arrives on destination /AFSExtension/ProductManagement/Destinations/SegmentLoadChannel.channel/SetSegmentsDestination

First processing by RF SegmentModelLoadRuleFunction.
Basically just generates a segmentHolder if no segmentholder with extId "segmentHolder" can be retrieved. otherwise just generates a temp segmentholder with the segment info from the event and exits.
Problems here using the functions aff.common.functions.utilities.lock & unlock - got extId collisions even while using them, went away when using the basic c_lock and c_unlock. 

Next we hit Rule AddAdditionalSegmentsToSegmentHolder
This one adds the segments in the tempSegmentHolder to the SegmentHolder if they don't already exist in there. No intention currently of supporting a removal of a segment - it's in memory only, so restart the machine.