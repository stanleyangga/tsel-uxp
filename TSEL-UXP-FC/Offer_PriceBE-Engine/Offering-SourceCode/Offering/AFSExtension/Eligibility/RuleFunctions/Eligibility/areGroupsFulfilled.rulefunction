/**
 * @description 
 */
boolean rulefunction AFSExtension.Eligibility.RuleFunctions.Eligibility.areGroupsFulfilled {
	attribute {
		validity = ACTION;
	}
	scope {
		AFSExtension.SharedResources.Ontology.Concepts.Eligibility.EligibleProduct	eligibleproduct;
		boolean	ignoreLinking;
	}
	body {
		/* 
		TIBCO SOFTWARE PVT LTD
		Created By    :- DGuschakowski
		Created Date  :- 2012-08-16
		
		Check for the given eligibleproduct if ComprisedOf and RequiredFor group/record maximum constraints of potential parents in the basket or inventory are violated. 
		Therefore it does the evaluation for the basket and inventory first and just checks whether an addition of the eligible product would create a violation.
		It returns false on the first violation
		*/
		
		LogString(LogLevel.DEBUG2,"Evaluation : "+eligibleproduct.ProductID+ " for parent's group restrictions");
		AFSExtension.SharedResources.Ontology.Concepts.Eligibility.EligibilityRequest er=eligibleproduct@parent;
		if(er.validateComprisedOf && !eligibleproduct.customComprisedOfGroupCheck){
		    for(int i=0;i<eligibleproduct.ComprisingProductIDs@length;i=i+1){
		        AFSExtension.SharedResources.Ontology.Concepts.Common.Product p=Instance.getByIdByUri(eligibleproduct.ComprisingProductIDs[i],AFSExtension.SharedResources.Ontology.Concepts.Common.Uris.Common_TrolleyItem);
		        if(p==null)
		            p=Instance.getByIdByUri(eligibleproduct.ComprisingProductIDs[i],AFSExtension.SharedResources.Ontology.Concepts.Common.Uris.Common_InventoryItem);
		        AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.Product pm=AFSExtension.SharedResources.RuleFunctions.ProductHandling.getModelByProductInstance(p);
		        if(p.customComprisedOfGroupCheck)
		            continue;
		        AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.GroupRecord cor=Instance.getByIdByUri(eligibleproduct.ComprisingRecordIDs[i],AFSExtension.SharedResources.Ontology.Concepts.Common.Uris.Model_ComprisedOfGroupRecord);
		        AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.Group co=cor@parent;
		        AFSExtension.SharedResources.Ontology.Concepts.Common.ProductGroup pg=AFSExtension.SharedResources.RuleFunctions.ProductComprisedOf.isComprisedOfGroupFulfilled(p,er,co,false,true,ignoreLinking);
		        if(!AFSExtension.Eligibility.RuleFunctions.Eligibility.isGroupFulfilled(p,co,cor,eligibleproduct,pg))
		            return false;
		    }
		}
		if(er.validateProductRequiredFor){
		    for(int i=0;i<eligibleproduct.RequiredForProductIDs@length;i=i+1){
		        AFSExtension.SharedResources.Ontology.Concepts.Common.Product p=Instance.getByIdByUri(eligibleproduct.RequiredForProductIDs[i],AFSExtension.SharedResources.Ontology.Concepts.Common.Uris.Common_TrolleyItem);
		        if(p==null)
		            p=Instance.getByIdByUri(eligibleproduct.RequiredForProductIDs[i],AFSExtension.SharedResources.Ontology.Concepts.Common.Uris.Common_InventoryItem);
		        AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.Product pm=AFSExtension.SharedResources.RuleFunctions.ProductHandling.getModelByProductInstance(p);
		        if(p.customProductRequiredForCheck)
		            continue;
		        AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.GroupRecord cor=Instance.getByIdByUri(eligibleproduct.RequiredForRecordIDs[i],AFSExtension.SharedResources.Ontology.Concepts.Common.Uris.Model_ProductRequiredForGroupRecord);
		        AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.Group co=cor@parent;
		        AFSExtension.SharedResources.Ontology.Concepts.Common.ProductGroup pg=AFSExtension.SharedResources.RuleFunctions.ProductRequiredFor.isRequiredForGroupFulfilled(p,er,co,false,true,ignoreLinking);
		        if(!AFSExtension.Eligibility.RuleFunctions.Eligibility.isGroupFulfilled(p,co,cor,eligibleproduct,pg))
		            return false;
		    }
		}
		LogString(LogLevel.DEBUG2,"Evaluation : "+eligibleproduct.ProductID+ " is valid for parent's group restrictions");
		return true;
	}
}