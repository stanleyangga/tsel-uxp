/**
 * @description 
 */
AFSExtension.SharedResources.Ontology.Concepts.Eligibility.EligibleProduct rulefunction AFSExtension.Eligibility.RuleFunctions.Eligibility.processProduct {
	attribute {
		validity = ACTION;
	}
	scope {
		AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.Product	p;
		AFSExtension.SharedResources.Ontology.Concepts.Eligibility.EligibilityRequest	eligibilityrequest;
		AFSExtension.Eligibility.Ontology.Events.EligibilityRequest	eligibilityrequestevent;
	}
	body {
		/* 
		TIBCO SOFTWARE PVT LTD
		Created By    :- DGuschakowski
		Created Date  :- 2012-08-16
		
		this is the key function to evaluate the eligibility of a TopLevelProduct:
		it runs through several checks in the following order:
		    - validity of the product(RecordStatus is active, RequestTimestamp against StartDate and EndDate of the product) -> if NOT the product is invisible
		    - product is compatible with all categories -> if NOT the product is invisible
		    - product is in promotion list -> if NOT the product is invisible
		    - product is in input field list -> if NOT the product is invisible
		    - product is compatible with any of the segments of each segment type, but compatible with all types in total -> if NOT the product is ineligible
		   -- here the actual product will be created and udfs such as input field udfs will be attached, further checks will be done already using the udfs 
		    - product is a valid bundle offering in context of the returnBundleOfferings feature -> if NOT the product is invisible
		    ***** HERE IS A POLICY RULE EXECUTION POINT: SCOPE: Eligibility EXECUTION POINT: ProductEligibilityCheck *****
		    - product is incompatible with any of the basket or inventory items, possible migrations are considered and make the product valid again -> if NOT the product is ineligible
		    - product is single use is checked against all basket or inventory items -> if NOT the product is ineligible
		if ReturnIneligibleProducts is set in the request, ineligible products will be shown as such plus an indicating message why they are not eligible
		note that the chain of the checks stops immediately after the product is ineligible, the first error of the chain will be displayed then
		*/
		
		    AFSExtension.SharedResources.Ontology.Concepts.Eligibility.EligibleProduct currp=null;
		    if(!AFSExtension.SharedResources.RuleFunctions.Entities.entityIsValid(p,eligibilityrequest.RequestTimeStamp)||
		        !AFSExtension.Eligibility.RuleFunctions.CategoryFilter.productIsInCategoryFilter(eligibilityrequest,p)||
		        !AFSExtension.Eligibility.RuleFunctions.PromotionFilter.productIsInPromotionFilter(p,eligibilityrequest)||
		        !AFSExtension.Eligibility.RuleFunctions.UDFPopulation.productIsInInputFieldFilter(p,eligibilityrequest)||
		        !AFSExtension.Eligibility.RuleFunctions.ProductTypeFilters.productIsInProductSubTypeFilter(eligibilityrequest,p.SubType)){
		        return null;
		    }
		    boolean valid=true;
		    if(eligibilityrequest.validateSegmentCompatibility)
		        valid=AFSExtension.SharedResources.RuleFunctions.SegmentEligibility.entityIsCompatibleWithSegmentTypeSet(p,eligibilityrequest);
		    if(valid){
		        currp=AFSExtension.Eligibility.RuleFunctions.ProductHandling.createEligibleProduct(p,eligibilityrequest,true,false,1);
		        if(!AFSExtension.Eligibility.RuleFunctions.Eligibility.isProductAValidOffering(p,eligibilityrequest,currp)){
		            AFSExtension.Eligibility.RuleFunctions.ProductHandling.removeProduct(currp,eligibilityrequest,true);
		            return null;
		        }
		        if(PolicyRules.RuleFunctions.isToggleActive(AFSExtension.SharedResources.Ontology.Concepts.Common.Eligibility_ExecutionPointToggles,AFSExtension.SharedResources.Ontology.Concepts.Common.ExecutionPoints.ProductEligibilityCheck))
		            AFSExtension.Eligibility.RuleFunctions.PolicyRules.executePolicyRulesPerEligibleProduct(           
		                AFSExtension.SharedResources.Ontology.Concepts.Common.Scope.Eligibility,
		                AFSExtension.SharedResources.Ontology.Concepts.Common.ExecutionPoints.ProductEligibilityCheck,
		                p,
		                currp,
		                eligibilityrequest,
		                eligibilityrequestevent);
		                
		
		        if(eligibilityrequest.validateProductCompatibility && !currp.customProductIntegrityCheck && currp.productEligible)
		            currp.productEligible=AFSExtension.SharedResources.RuleFunctions.ProductCompatibility.isProductCompatibleWithProductSet(p,eligibilityrequest,currp,true,true,false,true);
		        if(eligibilityrequest.validateProductCompatibility && currp.productEligible && p.singleUse && !currp.customSingleUseCheck)
		            currp.productEligible=AFSExtension.SharedResources.RuleFunctions.ProductCompatibility.isSingleUseProductValidWithProductSet(eligibilityrequest,currp,p,true);      
		        if(currp.productEligible){
		            AFSExtension.SharedResources.RuleFunctions.ProductComprisedOf.processParents(currp,p,eligibilityrequest);
		            AFSExtension.SharedResources.RuleFunctions.ProductComprisedOf.processChildren(currp,p,eligibilityrequest);
		            AFSExtension.SharedResources.RuleFunctions.ProductRequiredFor.processRequiredByProducts(currp,p,eligibilityrequest);
		        }
		    }
		    else if(eligibilityrequest.provideIneligibleProducts){
		        currp=AFSExtension.Eligibility.RuleFunctions.ProductHandling.createEligibleProduct(p,eligibilityrequest,false,false,1);
		        AFSExtension.SharedResources.RuleFunctions.Messages.createMessage(currp,"AFF-OCV-VAL-0300",null,null,"SegmentEligibility","Error");
		        LogString(LogLevel.DEBUG1,"Product: "+p.ID+ " is not Compatible with segments");
		    }
		if(currp!=null && !currp.productEligible && !eligibilityrequest.provideIneligibleProducts){
		    AFSExtension.Eligibility.RuleFunctions.ProductHandling.removeProduct(currp,eligibilityrequest,true);
		    return null;
		}
		return currp;
	}
}