/**
 * @description 
 */
void rulefunction AFSExtension.Validation.RuleFunctions.SanityCheck.productIsValidForDatesAndStatus {
	attribute {
		validity = ACTION;
	}
	scope {
		AFSExtension.SharedResources.Ontology.Concepts.ExtendedModel.Product	productmodel;
		AFSExtension.SharedResources.Ontology.Concepts.Common.RequestLifeCycle	requestlifecycle;
		AFSExtension.SharedResources.Ontology.Concepts.Common.Product	product;
	}
	body {
		/* 
		TIBCO SOFTWARE PVT LTD
		Created By    :- DGuschakowski
		Created Date  :- 2012-08-20
		
		This function checks if the given product is valid on the date the request is submitted (submittedDate) 
		and if present if it is valid when the order or basket item is required: 
		    - requestlifecycle.RequestTimeStamp or if not present DateTime.now() will be taken for the submitted(Date)
		    - requestlifecycle.RequiredByDate or trolleyitem.RequiredByDate: the one which is earlier creates requiredStart
		    - requestlifecycle.RequiredByDate or trolleyitem.RequiredByDate: the one which is later creates requiredEnd
		The products record status will be evaluated (=ACTIVE) and startDate and endDate will be evaluated against the submitted(Date), requiredStart and requiredEnd parameters.
		If any of those checks fail it will do the following things:
		    - creation of a validation message with indication which rule was violated
		    - product is invalid
		    - offer is invalid 
		*/
		
		DateTime startDate = productmodel.StartDate;
		DateTime endDate = productmodel.EndDate;
		DateTime defaultStart = DateTime.parseString("2000-01-01T00:00:00","yyyy-MM-dd'T'00:00:00");
		DateTime defaultEnd = DateTime.parseString("2100-01-01T00:00:00","yyyy-MM-dd'T'00:00:00");
		
		boolean checkEndDate=true;
		boolean checkStartDate=true;
		
		if(endDate == null || endDate == defaultEnd)
		    checkEndDate=false;
		if(startDate == null || startDate == defaultStart)
		    checkStartDate=false;
		
		
		String requiredStartDateSource;
		String requiredEndDateSource;
		long start = -1;
		long end = -1;
		if(startDate!=null)
		    start=DateTime.getTimeInMillis(startDate);
		if(endDate!=null)
		    end = DateTime.getTimeInMillis(endDate);
		long submitted=-1;
		long requiredstart=-1;
		long requiredend=-1;
		if(requestlifecycle.RequestTimeStamp != null){
		        submitted = DateTime.getTimeInMillis(requestlifecycle.RequestTimeStamp);
		}
		
		if(product instanceof AFSExtension.SharedResources.Ontology.Concepts.Common.TrolleyItem){
		AFSExtension.SharedResources.Ontology.Concepts.Common.TrolleyItem trolleyitem=product;
		//set the requiredByDate to the earlier of the orderline requiredByDate or the order requiredByDate if both exist.
		    if(trolleyitem.RequiredByDate != null && requestlifecycle.RequiredByDate != null){
		        if(requestlifecycle.RequiredByDate < trolleyitem.RequiredByDate){
		            requiredstart = DateTime.getTimeInMillis(requestlifecycle.RequiredByDate);
		            requiredStartDateSource = "Order";
		            requiredend = DateTime.getTimeInMillis(trolleyitem.RequiredByDate);
		            requiredEndDateSource = "Order Line";
		        }else{
		            requiredend = DateTime.getTimeInMillis(requestlifecycle.RequiredByDate);
		            requiredEndDateSource = "Order";
		            requiredstart = DateTime.getTimeInMillis(trolleyitem.RequiredByDate);
		            requiredStartDateSource = "Order Line";
		        }
		    }else if(requestlifecycle.RequiredByDate != null){
		            requiredstart = DateTime.getTimeInMillis(requestlifecycle.RequiredByDate);
		            requiredStartDateSource = "Order";
		            requiredend = DateTime.getTimeInMillis(requestlifecycle.RequiredByDate);
		            requiredEndDateSource = "Order";
		    }else if(trolleyitem.RequiredByDate != null){
		            requiredstart = DateTime.getTimeInMillis(trolleyitem.RequiredByDate);
		            requiredStartDateSource = "Order Line";
		            requiredend = DateTime.getTimeInMillis(trolleyitem.RequiredByDate);
		            requiredEndDateSource = "Order Line";
		    }
		}
		    //now do the tests.
		
		if(!productmodel.isActive){
		    requestlifecycle.offerIsValid = false;
		    product.productValid=false;
		    AFSExtension.SharedResources.RuleFunctions.Messages.createMessage(product,"AFF-OCV-VAL-0511",null,null,null,"Error");
		}else if(checkStartDate && start > submitted && submitted >0){
		    requestlifecycle.offerIsValid = false;
		    product.productValid=false;
		    AFSExtension.SharedResources.RuleFunctions.Messages.createMessage(product,"AFF-OCV-VAL-0507",null,null,null,"Error");
		}else if(checkEndDate && end < submitted && submitted >0){
		    requestlifecycle.offerIsValid = false;
		    product.productValid=false;
		    AFSExtension.SharedResources.RuleFunctions.Messages.createMessage(product,"AFF-OCV-VAL-0508",null,null,null,"Error");
		}else if(checkStartDate && start > requiredstart && requiredstart > 0){
		    requestlifecycle.offerIsValid = false;
		    product.productValid=false;
		    String text=System.getGlobalVariableAsString("AFF/OfferConfigurationValidation/Constants/Errors/OfferValidation/AFF-OCV-VAL-0509", "Date before ");
		    text=text+requiredStartDateSource;
		    AFSExtension.SharedResources.RuleFunctions.Messages.createMessage(product,"AFF-OCV-VAL-0509",null,text,null,"Error");
		}else if(checkStartDate && end < requiredend  && requiredend > 0){
		    requestlifecycle.offerIsValid = false;
		    product.productValid=false;
		    String text=System.getGlobalVariableAsString("AFF/OfferConfigurationValidation/Constants/Errors/OfferValidation/AFF-OCV-VAL-0510", "Date after ");
		    text=text+requiredEndDateSource;
		    AFSExtension.SharedResources.RuleFunctions.Messages.createMessage(product,"AFF-OCV-VAL-0510",null,text,null,"Error");     
		}
	}
}