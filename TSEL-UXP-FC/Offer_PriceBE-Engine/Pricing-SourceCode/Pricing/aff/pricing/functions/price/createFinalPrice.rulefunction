/**
 * @description 
 */
aff.pricing.concepts.calculation.FinalPrice rulefunction aff.pricing.functions.price.createFinalPrice {
	attribute {
		validity = ACTION;
	}
	scope {
		aff.pricing.concepts.prices.Price	price;
		long	lineID;
		String	PriceType;
		aff.pricing.concepts.calculation.PriceCalculationLine	pricecalculationline;
	}
	body {
		/* 
		TIBCO SOFTWARE PVT LTD
		Created By    :- DGuschakowski
		Created Date  :- 11-10-2012
		
		This function creates a new FinalPrice instance based on the data provided in the price (model) object, note that FinalPrice inherits from the Price concept, 
		so this can be used to create a copy of an existing FinalPrice (see copyPrices function).
		
		Also note that all Characteristics are attached to the FinalPrice, these will be part of the response resultset.
		
		*/
		
		String service = "price";
		String operation="createFinalPrice";
		String component=getComponent();
		String correlationID= price.Identifier;
		
		//commonServices.functions.logging.log(LogLevel.DEBUG1,correlationID,service,operation,component,"Creating Price: "+price.Identifier+" type: "+PriceType);
		//System.debugOut("Inside the createFinalPrice::value of:PriceTypes.Migration::"+PriceTypes.Migration);
		
		aff.pricing.concepts.calculation.FinalPrice fp=Instance.newInstance(PriceType,null);
		fp.Identifier=price.Identifier;
		
		if (price.Type==PriceTypes.Migration)
		{
		    fp.Amount=calculateMigrationPriceAmount(price,pricecalculationline);
		}
		else 
		{
		    fp.Amount=price.Amount;
		}
		
		fp.Type=price.Type;
		//System.debugOut("Inside the createFinalPrice::fp.Amount=price.Amount :::"+fp.Amount+"::type::"+fp.Type);
		
		fp.ChargeFrequency=price.ChargeFrequency;
		fp.ChargeFrequencyUOM=price.ChargeFrequencyUOM;
		fp.Currency=price.Currency;
		fp.Per=price.Per;
		fp.PerUOM=price.PerUOM;
		fp.MinBoundary=price.MinBoundary;
		fp.MaxBoundary=price.MaxBoundary;
		fp.Priority=price.Priority;
		fp.isValid=true;
		fp.Quantity=1;
		//fp.Quantity=price.Quantity;
		fp.LineID=lineID;
		fp.isOverride=price.isOverride;
		fp.discountsDetermined=false;
		fp.MinAmount=price.MinAmount;
		fp.MinAmountSet=price.MinAmountSet;
		
		for(int i=0;i<price.Characteristic@length;i=i+1){
		      aff.pricing.concepts.common.Characteristic fpc=  Instance.newInstance("/aff/pricing/concepts/common/Characteristic",null);
		      fpc.Name=price.Characteristic[i].Name;
		      fpc.Value=price.Characteristic[i].Value; 
		      Instance.PropertyArray.appendContainedConcept(fp.Characteristic,fpc,System.currentTimeMillis());
		}
		
		if(price instanceof aff.pricing.concepts.calculation.FinalPrice)
		{
		    aff.pricing.concepts.calculation.FinalPrice fp2=price;
		    fp.OriginalFinalPriceID=price@id;
		    fp.InitialAmount=fp2.InitialAmount;
		    fp.AttachedToProduct=fp2.AttachedToProduct;
		}else
		{
		        fp.InitialAmount=fp.Amount;
		}
		return fp;
	}
}