/**
 * @description 
 */
double rulefunction aff.pricing.functions.restrictions.entityIsApplicableForProducts {
	attribute {
		validity = ACTION;
	}
	scope {
		aff.pricing.concepts.calculation.PriceCalculationHolder	pricecalculationholder;
		aff.pricing.concepts.common.PricingModel	entity;
		double	quantity;
		String	type;
		aff.pricing.concepts.calculation.ProductHelper	producthelper;
		aff.pricing.concepts.calculation.PriceCalculationLine	pricecalculationline;
	}
	body {
		/* 
		TIBCO SOFTWARE PVT LTD
		Created By    :- DGuschakowski
		Created Date  :- 09-10-2012
		
		This function evaluates if the provided entity is applicable for its configured required products, therefore it evaluates for each RequiresProduct: 
		    - if the relationship was already fulfilled by another entity, this is done due to the fact that multiple prices and discount can share the same RequiresProductGroup, 
		      once fulfilled then it fulfills the restrictions for all sharing entities and their configured appliesToQuantity parameters, 
		      a fulfilled RequiresProductGroup can mean that PriceA can be applied once and PriceB can be applied 4 times depending on their configured appliesToQuantity
		    - if there is more "open" quantity for the price or discount due to previous evaluations then the provided quantity is fulfilled, if it's less not not there at all then the RequiresProductGroup will be
		      evaluated again for the remaining quantity, e.g. if a price needs to be applied 7 times and the prefullfiled quantity is 3 then the remaining 4 pieces are evaluated, if that results in 0 then it returns 3 if
		      it is fulfilled it returns with 7. 
		      - note that if the includeInventory flag is set to true it will first look for existing instance of the same Product in the inventory and substract that quantity from the
		      fulfilled quantity, e.g. if a price needs to be applied to 7 instances of ProductA and the RequiresProductGroup has appliesToQuantity=10 configured and includeInventory set to true 
		      and if there are 4 instances in the inventory then the RequiresProductGroup needs to be fulfilled twice as it fulfills the appliesToQuantity=10 - the 4 instances in the inventory so it can only apply the 
		      price to only 6 instances of the product, in order to apply it to the 7th it needs to evaluate the group again, if it's fulfilled again then it returns 7 if not it returns only the 6
		    - the evaluation of product groups is done using the RequiresProducts function
		    - if the RequiresProduct function is called it uses the ProductCounterHolder, this holder stores all products that were needed to fulfill the group. If the group is fulfilled with a quantity > 0 then
		      the needed products are bound to the price, this is done by reducing the quantity which can be used for other RequiresProductGroup entities
		    - the reduction is done by the mapQuantityUsageToProductHelpers
		    - the prefulfilled quantity which is used earlier in the process is set by the resetFulfilledGroupQuantities, so if there is another price or discount 
		      which shares the same RequiresProductGroup it will work with the updated prefulfilled quantity
		
		
		The return value is the quantity that is applicable.
		
		*/
		
		String service = "restrictions";
		String operation="entityIsApplicableForProducts";
		String component=getComponent();
		String correlationID= pricecalculationline.LineNumber;
		aff.pricing.concepts.calculation.ProductCounterHolder pch=Instance.newInstance("/aff/pricing/concepts/calculation/ProductCounterHolder",null);
		pch.type=type;
		pch.currentQuantity=quantity;
		pch.itemsInInventory=-1;
		//commonServices.functions.logging.log(LogLevel.DEBUG1,correlationID,service,operation,component,"Testing ProductCompatibility: "+entity.Identifier);
		double invItems=-1;
		for(int i=0;i<entity.RequiresProduct@length;i=i+1)
		{
		     String linkID=null;
		    aff.pricing.concepts.common.RequiresProductGroup rpg=aff.pricing.functions.price.getRequiresProductGroup(entity.RequiresProduct[i].ProductGroupID);
		    //commonServices.functions.logging.log(LogLevel.DEBUG1,correlationID,service,operation,component,"Executing ProductCompatibility: "+entity.Identifier+" for group: "+rpg.GroupID);
		    if(rpg.withinLink)
		        linkID=aff.pricing.functions.grouping.getLinkDefs(rpg,pricecalculationline); 
		    aff.pricing.concepts.calculation.FulfilledGroup fg = aff.pricing.functions.restrictions.getFulfilledGroup(pricecalculationholder,rpg.GroupID,false,type,linkID);
		    aff.pricing.concepts.calculation.FulfilledEntity fe=null;
		    boolean calculateInventory=true;
		    if(fg!=null){
		        fe = aff.pricing.functions.restrictions.getFulfilledEntity(fg,entity.RequiresProduct[i],true,producthelper.ProductID);
		        if(fe.inventoryApplied || !entity.RequiresProduct[i].includeInventoryItems){
		            pch.itemsInInventory=0;
		            calculateInventory=false;
		        }
		    }
		    if( calculateInventory) {          
		        if(invItems==-1){
		            invItems=aff.pricing.functions.restrictions.getInventoryItems(pricecalculationholder,producthelper,rpg);
		            pch.itemsInInventory=invItems;
		        }
		    }            
		
		    double preFulfilledQuantity=aff.pricing.functions.restrictions.getFulfilledQuantity(fe,quantity);
		    
		    commonServices.functions.logging.log(LogLevel.DEBUG2,correlationID,service,operation,component,"Group: "+rpg.GroupID+" / Already Fulfilled Quantity: "+preFulfilledQuantity+" / Needed Quantity: "+pch.currentQuantity +" / ReducedByInventoryItems: "+pch.itemsInInventory);
		
		     if(preFulfilledQuantity>=pch.currentQuantity && fe!=null && fe.inventoryApplied){
		        commonServices.functions.logging.log(LogLevel.DEBUG2,correlationID,service,operation,component,"Quantity of Entity: "+entity.Identifier+" was already fully fulfilled");
		        fe.TakenQuantity=fe.TakenQuantity+pch.currentQuantity;
		    }
		     else{
		        commonServices.functions.logging.log(LogLevel.DEBUG2,correlationID,service,operation,component,"Quantity of Entity: "+entity.Identifier+" not fully fulfilled yet");
		        aff.pricing.concepts.calculation.ProductCounter pc=Instance.newInstance("/aff/pricing/concepts/calculation/ProductCounter",null);
		        Instance.PropertyArray.appendContainedConcept(pch.entities,pc,System.currentTimeMillis());
		        pc.totalcount=0;
		        pc.min=rpg.GroupMin;
		        pc.max=rpg.GroupMax;
		        pc.groupid=rpg.GroupID;
		        pc.openQuantity=preFulfilledQuantity;
		        aff.pricing.functions.restrictions.requiresProducts(pricecalculationholder,rpg,pc,entity.RequiresProduct[i].appliesToQuantity,entity.RequiresProduct[i].appliesToMaxQuantity,entity.RequiresProduct[i].includeInventoryItems,producthelper,pricecalculationline);
		        if(pch.currentQuantity==0)
		            break;
		    }
		}
		//commonServices.functions.logging.log(LogLevel.DEBUG1,correlationID,service,operation,component,"ProductCompatibility: "+entity.Identifier+" for quantity: "+pch.currentQuantity);
		if(pch.currentQuantity>0)
		{
		    for(int f=0;f<pch.entities@length;f=f+1)
		    {
		        ProductCounter pc=pch.entities[f];
		        String linkID=null;
		        aff.pricing.concepts.common.RequiresProductGroup rpg=aff.pricing.functions.price.getRequiresProductGroup(pc.groupid);
		        aff.pricing.concepts.common.ProductGroupContainer rpc= aff.pricing.functions.restrictions.getRequiredProductGroupContainer(entity,pc.groupid);
		        if(rpg.withinLink)
		            linkID=aff.pricing.functions.grouping.getLinkDefs(rpg,pricecalculationline); 
		        aff.pricing.concepts.calculation.FulfilledGroup fg=aff.pricing.functions.restrictions.getFulfilledGroup(pricecalculationholder,rpg.GroupID,true,type,linkID);
		        fg.groupFulfilled=fg.groupFulfilled+pc.groupsApplied;
		        aff.pricing.functions.restrictions.resetFulfilledGroupQuantities(fg);
		        aff.pricing.concepts.calculation.FulfilledEntity fe = aff.pricing.functions.restrictions.getFulfilledEntity(fg,rpc,true,producthelper.ProductID);
		        fe.TakenQuantity=fe.TakenQuantity+pch.currentQuantity;
		        if(pch.itemsInInventory>-1 && !fe.inventoryApplied){
		            fe.TakenQuantity=fe.TakenQuantity+pch.itemsInInventory;
		            fe.inventoryApplied=true;
		        }
		        //commonServices.functions.logging.log(LogLevel.DEBUG1,correlationID,service,operation,component,"Mapping Quantity for Entity: "+entity.Identifier);
		    }
		    mapQuantityUsageToProductHelpers(pch);
		}
		double qty= pch.currentQuantity;
		aff.pricing.functions.common.logConcept(LogLevel.DEBUG3,correlationID,service,operation,component,pch);
		Instance.deleteInstance(pch);
		//commonServices.functions.logging.log(LogLevel.DEBUG1,correlationID,service,operation,component,"Quantity of Entity: "+entity.Identifier+" is valid for RequiresProduct: "+qty);
		return qty;
	}
}