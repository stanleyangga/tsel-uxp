/**
 * @description 
 */
aff.pricing.concepts.calculation.ApplicableDiscount rulefunction aff.pricing.functions.discount.createDiscountStructure {
	attribute {
		validity = ACTION;
	}
	scope {
		aff.pricing.concepts.discounts.Discount	discount;
		aff.pricing.concepts.calculation.PriceCalculationLine	pricecalculationline;
		aff.pricing.concepts.calculation.ProductHelper	producthelper;
		double	quantity;
		String	DiscountType;
		aff.pricing.concepts.calculation.FinalPrice	finalprice;
		aff.pricing.concepts.calculation.ApplicableDiscount	parent;
	}
	body {
		/* 
		TIBCO SOFTWARE PVT LTD
		Created By    :- DGuschakowski
		Created Date  :- 09-10-2012
		
		This function creates a concrete discount instance based on the provided discount model (createFinalDiscount). 
		Before creating it the function determines whether a discount is applicable or not for Segments, Characteristics and Products.
		
		*/
		
		String service = "discount";
		String operation="createDiscountStructure";
		String component=getComponent();
		String correlationID= pricecalculationline.LineNumber;
		
		aff.pricing.concepts.calculation.ApplicableDiscount ad=null;
		int prodresult=quantity;
		
		if(discount.virtual){
		        if(!aff.pricing.functions.calculation.containsVirtualProduct(pricecalculationline@parent,discount.Identifier,pricecalculationline.SubscriberID,discount.subscriberOnly))
		           return null;
		}else{
		    if(!aff.pricing.functions.mvalidity.instanceIsValid(discount,pricecalculationline@parent.messageTimestamp))
		    return null;
		    
		    int segresult=aff.pricing.functions.restrictions.entityIsApplicableForSegments(pricecalculationline@parent,discount);
		
		    aff.pricing.concepts.calculation.PriceCalculationLine rpcl;
		    if(producthelper.presentInOrderLineID>-1)
		        rpcl=Instance.getByIdByUri(producthelper.presentInOrderLineID,aff.pricing.functions.entityUri.priceCalculationLineUri());
		    else
		        rpcl=pricecalculationline;
		
		    int charresult=aff.pricing.functions.restrictions.entityIsApplicableForCharacteristics(rpcl,discount);
		
		    if(segresult>=0 && charresult>=0){
		        if(discount.RequiresProduct@length>0)
		            prodresult=aff.pricing.functions.restrictions.entityIsApplicableForProducts(pricecalculationline@parent,discount,quantity,"discount",producthelper,pricecalculationline);
		    }else 
		        return null;
		}
		if(prodresult>0)
		{
		        if(!discount.virtual){
		            ad=aff.pricing.functions.discount.createFinalDiscount(discount,DiscountType);
		            ad.Quantity=prodresult;
		            if(finalprice!=null)
		                finalprice.discountedQuantity=finalprice.discountedQuantity+ad.Quantity;
		            if(parent!=null)
		                Instance.PropertyArray.appendContainedConcept(parent.Childs,ad,0);
		            
		        }else
		            ad=parent;
		        aff.pricing.concepts.discounts.Discount cd;
		        aff.pricing.concepts.calculation.ApplicableDiscount fd;
		        for(int i=0;i<discount.ComprisedOf@length;i=i+1){
		               cd=aff.pricing.functions.common.loadByExtID(aff.pricing.functions.extID.getDiscountExtID(discount.ComprisedOf[i]),aff.pricing.functions.entityUri.discountUri(),false);
		               fd=aff.pricing.functions.discount.createDiscountStructure(cd,pricecalculationline,producthelper,prodresult,aff.pricing.functions.entityUri.childDiscountHelperUri(),null,ad);              
		        }
		        return ad;
		}else
		    return null;
	}
}