<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:ns0 = "http://schemas.telkomsel.com/UXP/Configuration/v1"
    xmlns:ns1 = "http://schemas.telkomsel.com/UXP/Component/v1"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="ns0:ServiceProperties">
            <xsl:element name="{concat(@ComponentName,@Operation)}">
                <xsl:for-each select="ns0:Item">
            <xsl:element name="{@Key}">
                <xsl:for-each select="ns1:Property">
                    <xsl:element name="{@Name}">
                        <xsl:value-of select="@Value"/>
                    </xsl:element>
                </xsl:for-each>
                <xsl:for-each select="ns0:Item">
                    <xsl:element name="{@Key}">
                        <xsl:for-each select="ns1:Property">
                            <xsl:element name="{@Name}">
                                <xsl:value-of select="@Value"/>
                            </xsl:element>
                        </xsl:for-each>
                        <xsl:for-each select="ns0:Item">
                            <xsl:element name="{@Key}">
                                <xsl:for-each select="ns1:Property">
                                    <xsl:element name="{@Name}">
                                        <xsl:value-of select="@Value"/>
                                    </xsl:element>
                                </xsl:for-each>
                                <xsl:for-each select="ns0:Item">
                                    <xsl:element name="{@Key}">
                                        <xsl:for-each select="ns1:Property">
                                            <xsl:element name="{@Name}">
                                                <xsl:value-of select="@Value"/>
                                            </xsl:element>
                                        </xsl:for-each>
                                        <xsl:for-each select="ns0:Item">
                                            <xsl:element name="{@Key}">
                                                <xsl:for-each select="ns1:Property">
                                                    <xsl:element name="{@Name}">
                                                        <xsl:value-of select="@Value"/>
                                                    </xsl:element>
                                                </xsl:for-each>
                                                <xsl:for-each select="ns0:Item">
                                                    <xsl:element name="{@Key}">
                                                        <xsl:for-each select="ns1:Property">
                                                            <xsl:element name="{@Name}">
                                                                <xsl:value-of select="@Value"/>
                                                            </xsl:element>
                                                        </xsl:for-each>
                                                        <xsl:for-each select="ns0:Item">
                                                            <xsl:element name="{@Key}">
                                                                <xsl:for-each select="ns1:Property">
                                                                    <xsl:element name="{@Name}">
                                                                        <xsl:value-of select="@Value"/>
                                                                    </xsl:element>
                                                                </xsl:for-each>
                                                                <xsl:for-each select="ns0:Item">
                                                                    <xsl:element name="{@Key}">
                                                                        <xsl:for-each select="ns1:Property">
                                                                            <xsl:element name="{@Name}">
                                                                                <xsl:value-of select="@Value"/>
                                                                            </xsl:element>
                                                                        </xsl:for-each>
                                                                        
                                                                    </xsl:element>
                                                                </xsl:for-each>
                                                            </xsl:element>
                                                        </xsl:for-each>        
                                                    </xsl:element>
                                                </xsl:for-each>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:for-each>
                            </xsl:element>
                        </xsl:for-each>        
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
        </xsl:for-each>
                <xsl:for-each select="ns1:Property">
                    <xsl:element name="{@Name}">
                        <xsl:value-of select="@Value"/>
                    </xsl:element>
                </xsl:for-each>
                
            </xsl:element>
    </xsl:template>
</xsl:stylesheet>