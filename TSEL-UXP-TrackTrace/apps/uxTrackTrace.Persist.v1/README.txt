$Id: README.txt 57 2015-06-17 08:28:23Z ibutarbu $

=======
PERSIST
=======

Purpose:
  To persist a tracktrace record into a database
  

Notes:
  Currently only OnStartup and OnShutdown operation is persisted

