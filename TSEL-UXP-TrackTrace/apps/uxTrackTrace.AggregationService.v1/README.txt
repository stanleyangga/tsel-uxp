$Id: README.txt 40 2015-06-16 05:27:15Z ibutarbu $

=============
SPECIFICATION
=============

Last Updated: 10 June 2015

@InstanceName = "UXP"

##### 1 Component SLA #####

Objective:
~~~~~~~~
Ability to quickly define each of UXP Component's SLA in real-time in terms of:
a. Throughput (Transaction per seconds) -> By having total transactions per 5 minutes windows size, we can calculate the throughput in spotfire using specific formula
b. ResponseTime -> Can be measured by max/min/avg/p90 aggregation function.
c. Success Rate -> (Total Success + Total BusinessError) / (Total Success + Total BusinessError + Total SystemError)

Specification:
~~~~~~~~~~~
@CacheType = "SLA"
WindowSize = 5 mins

ID = <ComponentOperationName>
IDType = "ComponentOperation"

To define an ID is by using following mapping
ID = TrackTrace/Component/@Name + "." + TrackTrace/Component/Operation

Notes: Applicable to all components

Properties:
Status (TrackTrace/Metrics/Status)

If Status = "SystemError" || "BusinessError" then
ExceptionCode (TrackTrace/Metrics/ExceptionCode)
ExceptionActor (TrackTrace/Metrics/ExceptionActor)

Measurement:
ProcessingTime (TrackTrace/Metrics/ProcessingTime)


#### 2 Component Technical Summary ####

Objective:
~~~~~~~~
Ability to quickly track and trace component's journey and troubleshoot the culprit during an exception.

Specification:
~~~~~~~~~~
@CacheType = "TechnicalSummary"
WindowSize=60 mins

ID = <ComponentOperationName>
IDType = "ComponentOperation"

To define an ID is by using following mapping
ID = TrackTrace/Component/@Name + "." + TrackTrace/Component/Operation

Notes: Applicable to all components

Properties:
Host (TrackTrace/Component/Host)
Consumer (TrackTrace/Consumer/@Name + "." + TrackTrace/Consumer/Operation)
Instance (TrackTrace/Component/Instance)
Status (TrackTrace/Metrics/Status)

If Status = "SystemError" || "BusinessError" then
ExceptionCode (TrackTrace/Metrics/ExceptionCode)
ExceptionReason (TrackTrace/Metrics/ExceptionReason)
ExceptionActor (TrackTrace/Metrics/ExceptionActor)
ExceptionActivity (TrackTrace/Metrics/ExceptionActivity)

Measurement:
ProcessingTime (TrackTrace/Metrics/ProcessingTime)


#3 UXP Business Summary

Objective:
~~~~~~~~
Ability to quickly track and trace component's journey and troubleshoot the culprit during an exception.

Specification:
~~~~~~~~~~
@CacheType = "BusinessSummary"
@WindowSize = 60 mins

ID = <ComponentName>
IDType = "CompositeOperation"

Only applicable for following CompositeOperation for Stage 1A.

Offer.v1.GetBusinessOffers
Offer.v1.GetEligibleOffers
Fulfilment.v1.PurchaseOffer

Expected Properties:
Channel (TrackTrace/@UMB)
Consumer (TrackTrace/Consumer/@Name + "." + TrackTrace/Consumer/Operation)
Status (TrackTrace/Metrics/Status)

Invoke Subscriber.v1.GetSubscriberProfiles (Profile=Location,Brand,CustType,PricePlan) then map to respective property below
SubLocation
SubBrand
SubCustType
SubPricePlan

POName
POSubClass
BPName
PFName
BonusName
BonusSubClass
BonusValidity

If Status = "SystemError" || "BusinessError" then
ExceptionCode (TrackTrace/Metrics/ExceptionCode)
ExceptionReason (TrackTrace/Metrics/ExceptionReason)

Expected Measurement:
ProcessingTime (TrackTrace/Metrics/ProcessingTime)
Amount (Only applicable for Fulfilment.v1.PurchaseOffer)