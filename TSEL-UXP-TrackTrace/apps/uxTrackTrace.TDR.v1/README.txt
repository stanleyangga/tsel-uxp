$Id: README.txt 91 2015-06-19 07:00:16Z ibutarbu $

===
TDR
===

Purpose:
  Generate hourly TDR files from every transaction occurred within UXP systems.

Specification:
  Filename: UXPTDR.yyyyMMddd-HH.txt
  Format:   PIPE separated values - format specified in /SharedResources/Schemas/Services/TDR_v2.xsd

Notes:
  TDR_v1.xsd is conformed with OLD specification
  TDR_v2.xsd is conformed with CURRENT format of REFLEX TDR.
  TDR_v3.xsd is v2 with additional columns
