UPDATE
  C2P_ACTIVEOFFER
SET
  IS_SUBSCRIPTION = 'false',
  EXPIRED_ON = ?
WHERE 
  IDENTIFIER = ? AND 
  CATEGORY = COALESCE(?, CATEGORY) AND
  TRX_ID != ?