DELETE FROM C2P_ACTIVEOFFER 
WHERE
  IDENTIFIER = ? AND
  PRODUCT_FAMILY = COALESCE(?, PRODUCT_FAMILY) AND
  PRODUCT_ID = COALESCE(?, PRODUCT_ID) AND
  BUSINESS_PRODUCT = COALESCE(?, BUSINESS_PRODUCT)