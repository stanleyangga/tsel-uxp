UPDATE
  C2P_ACTIVEOFFER
SET
  EXPIRED_ON = ?
WHERE 
  IDENTIFIER = ? AND 
  CATEGORY = COALESCE(?, CATEGORY) AND
  TRX_ID != ?